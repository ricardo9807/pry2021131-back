var mongoose = require('mongoose');
var Schema   = mongoose.Schema;

//Importamos modelos hijo
var documentSchema = require('./childrens/documentModel.js');	

var newsSchema = new Schema({
	'target' : String, // -> Valores: RIDER, CLIENT
	'attachment' : documentSchema,
	'published' : Boolean,
	'content': String,
	'title': String,
	'size': String, //STANDART, LARGE
	'active' : Boolean,
	'creationDate' : Date,
});

module.exports = mongoose.model('news', newsSchema);
