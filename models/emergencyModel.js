var mongoose = require('mongoose');
var Schema   = mongoose.Schema;


//Importamos modelos hijo
var fetusSchema = new mongoose.Schema({ 	
	'fcf': Number, 
	'situationFetus': Number, 
	'presentationFetus': Number,         
	'observation': String,        
	'positionFetus': Number,    
});

//esquema principal
var emergencySchema = new Schema({
	 'dateAttention': Date,   
	 'clinic': String,
	 'initialObservation': String,
	 'lastControlDate' : Date,


	 'gestationalAge' : Number,
	 'diastolicPressure': Number,
	 'systolicPressure': Number,
	 'temperature': Number,
	 'pulse': Number,
	 'breathingFrequency': Number,

	 'systemDiagnosis': String,
	 'specialistName': String,
	 'colegiatureNumber': String,
	 'finalDiagnosis': String,
	 'recommendations': String,
	 'interPregnant': Number,
	 'terminatePregnancy': Number,
	 'pretermBirth': Number,

	 'fetusesDetail': [fetusSchema],

	 'idHCPB' : {
		type: Schema.Types.ObjectId,
		ref: 'hcpbs'
	 },
	 'idPatient' : {
		type: Schema.Types.ObjectId,
		ref: 'users'
	 },
	 'active': Boolean,
});


module.exports = mongoose.model('emergency', emergencySchema);
