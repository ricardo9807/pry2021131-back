var mongoose = require('mongoose');
var Schema   = mongoose.Schema;
var bcrypt = require('bcrypt');

//esquema hijo de detalle de cliente
var bussinessHourSchema = new mongoose.Schema({ 
	start: Object,
	end: Object, 
});

//esquema principal
var configurationSchema = new Schema({
	'distanceRadius' : Number,
	'flatAmount' : Number,
	'useFlatAmount': Boolean,
	'sortBy' : String,
	'servicePercentage' : Number,
	'usePercentage':Boolean,
	'bussinessHour': bussinessHourSchema,
	'timerInterval': Number, //Intervalo de envío de localización para riders (Usado por App rider)
	'checkServiceInterval': Number, //Intervalo de tiempo para actualización de estado de servicio (Usado por App cliente)
	'paymentMethods': [],
	'feePerKm':Number,
	'pollutionFactor': Number,
	'email': String,
	'password': String,
	'initialBonus' : Number,
	'adminPhone' : String,
	'clientCallSupport' : Boolean,
	'contactSupportNumber' : String,
	'contactAdminNumber' : String,
});

module.exports = mongoose.model('configuration', configurationSchema);
