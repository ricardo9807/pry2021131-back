var mongoose = require('mongoose');
var Schema   = mongoose.Schema;

//Importamos modelos hijo
var documentSchema = require('./childrens/documentModel.js');
var statusSchema = require('./childrens/statusModel.js');

var creditsSchema = new Schema({
	'idRider' : {
	 	type: Schema.Types.ObjectId,
	 	ref: 'user'
	},
	'riderName': String,
	'riderDocumentNumber': String,
	'riderPhone': String,
	'statusHistory' : [statusSchema],
	'currentStatus' : statusSchema, 
	'document' : documentSchema,
	'amount' : Number,
	'creationDate' : Date,
	'active' : Boolean,
	/**valores test */
	'nombre': String,
	'numero':Number,
	'listaVal':String,
	'createdBy': String
});

module.exports = mongoose.model('credits', creditsSchema);
