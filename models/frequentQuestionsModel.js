var mongoose = require('mongoose');
var Schema   = mongoose.Schema;



var frequentQuestionsSchema = new Schema({
	
	'creationDate' : Date,
	'active' : Boolean,
	'target' : String,
	'question': String,
	'answer': String,
	'published' : Boolean,
});

module.exports = mongoose.model('frequentQuestions', frequentQuestionsSchema);
