var mongoose = require('mongoose');
var Schema   = mongoose.Schema;
var bcrypt = require('bcrypt');


//Importamos modelos hijo

//esquema hijo de detalle de cliente
var specialistDetailSchema = new mongoose.Schema({ 
	colegiatureNumber: String,
	isAdmin: Boolean,
	specialtiesArray: Array, 
});
//esquema hijo de detalle de rider
var patientDetailSchema = new mongoose.Schema({ 
	affiliationDate: Date,
	hcpbActive: Boolean,
	idHCPB :  String,
	status: String
});

//esquema principal
var userSchema = new Schema({
	'name' : String,
	'lastName' : String,
	'surName' : String,
	'fullname' : String,
	'email' : String,
	'password' : String,
	'phone' : String,
	'documentNumber' : String,
	'documentType': String,
	'active' : Boolean,
	'creationDate' : Date,
	'patientDetail': patientDetailSchema,
	'specialistDetail': specialistDetailSchema,
	'address': String,
	'birthdate': Date,
	'idDistrict': Number,
	'isSpecialist': Boolean,
	'isPatient': Boolean,
	'password' : String,
	'status': String,
	
});


// hash the password
userSchema.methods.generateHash = function (password) {
	return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};

// checking if password is valid
userSchema.methods.validPassword = function (password) {
	return bcrypt.compareSync(password, this.password);
};

module.exports = mongoose.model('user', userSchema);
