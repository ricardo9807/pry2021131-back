var mongoose = require('mongoose');
var Schema   = mongoose.Schema;


//esquema principal
var monitoringSchema = new Schema({	   
	 'idHCPB' : {
		type: Schema.Types.ObjectId,
		ref: 'hcpbs'
	 },
	 'idPatient' : {
		type: Schema.Types.ObjectId,
		ref: 'users'
	 },
	 'temperature': Number,
	 'dateAttention': Date, 
	 'active': Boolean,
	 'gestationalAge' : Number,
	 'pulse': Number,
	 'bloodPressure': Number, 
	 'patientComment': String,
	 'crampPelvicArea' : Boolean,
	 'fatigue' : Boolean,
	 'abdominalPain': Boolean,
	 'swelling': Boolean, 
	 'phone': String,
	 'documentNumber':String,
	 'fullName': String,
});


module.exports = mongoose.model('monitoring', monitoringSchema);
