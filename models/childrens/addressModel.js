var mongoose = require('mongoose');
var Schema   = mongoose.Schema;

//Importamos modelos hijo
var pointSchema = require('./pointModel.js');

//esquema hijo de detalle de cliente
var addressSchema = new mongoose.Schema({ 
	addressLine1: String, //Dirección general
	interior: String, //Interior de la dirección
	isPrimary: Boolean, //Dirección principal
	location: pointSchema
});
module.exports = addressSchema; 