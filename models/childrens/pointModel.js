var mongoose = require('mongoose');
var Schema   = mongoose.Schema;

//esquema para Puntos GPS
const pointSchema = new mongoose.Schema({
	type: {
	  type: String,
	  enum: ['Point'],
	  //cambio10/11
	  required: false
	},
	coordinates: {
	  type: [Number],
	  required: true
	}
  });
  
module.exports = pointSchema;