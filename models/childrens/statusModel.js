var mongoose = require('mongoose');
var Schema   = mongoose.Schema;

//esquema para cambios de estado
var statusSchema = new mongoose.Schema({ 
	creationDate: Date,
	code: String,
	description: String
});
module.exports = statusSchema;