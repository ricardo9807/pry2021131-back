var mongoose = require('mongoose');
var Schema   = mongoose.Schema;

//esquema hijo de detalle de cliente
var documentSchema = new mongoose.Schema({ 
	name: String, //nombre del documento
	url: String, //url
	type: String 
	/**
	 * Valores:
	 * 'CREDIT_PAYMENT'
	 * ''//Pendiente por definir para reclu
	*/
});
module.exports = documentSchema;