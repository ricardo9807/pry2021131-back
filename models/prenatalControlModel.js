var mongoose = require('mongoose');
var Schema   = mongoose.Schema;


//Importamos modelos hijo
var fetusSchema = new mongoose.Schema({ 	
	'fcf': Number, 
	'situationFetus': Number, 
	'presentationFetus': Number,         
	'observation': String,        
	'positionFetus': Number,    
});

//esquema principal
var prenatalControlSchema = new Schema({
	 'dateAttention': Date,   
	 'actualWeight': Number,     
	 'weightGain': Number,
	 'pulse': Number,
	 'imc': Number,
	 'temperature': Number,
	 'systolicPressure': Number,
	 'diastolicPressure': Number,
	 'uterineHeight': Number,
	 'breathingFrequency': Number,
	 'systemDiagnosis': String,
	 'finalDiagnosis': String,
	 'nextControlDate': Date,
	 'numberControl': Number,
	 'recommendations': String,
	 'nameSpecialist' : String,
	 'fetusesDetail': [fetusSchema],
	 'idHCPB' : {
		type: Schema.Types.ObjectId,
		ref: 'hcpbs'
	 },
	 'idSpecialist' : {
		type: Schema.Types.ObjectId,
		ref: 'users'
	 },
	 'active': Boolean,
	 'gestationalAge': Number,
});


module.exports = mongoose.model('prenatalControl', prenatalControlSchema);
