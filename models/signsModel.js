var mongoose = require('mongoose');
var Schema   = mongoose.Schema;

var signsSchema = new Schema({
	'pulse': Number,                        
	'bloodPressure': Number,
	'temperature': Number,
  });

module.exports = mongoose.model('data_signs', signsSchema);
