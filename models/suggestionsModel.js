var mongoose = require('mongoose');
var Schema   = mongoose.Schema;

var suggestionsSchema = new Schema({
	'idClient' : {
		type: Schema.Types.ObjectId,
		ref: 'user'
	},
	'status' : String, //NEW, FILED
	'content': String,
	'active' : Boolean,
	'creationDate' : Date,
});

module.exports = mongoose.model('suggestions', suggestionsSchema);
