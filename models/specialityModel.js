var mongoose = require('mongoose');
var Schema   = mongoose.Schema;

//Importamos modelos hijo

var newsSchema = new Schema({
	'idClinic': String,
	'name': String,
	'description': String, //STANDART, LARGE
	'active' : Boolean,
	'creationDate' : Date,
});

module.exports = mongoose.model('speciality', newsSchema);
