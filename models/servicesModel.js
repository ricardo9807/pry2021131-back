var mongoose = require('mongoose');
var Schema   = mongoose.Schema;

//Importamos modelos hijo
var addressSchema = require('./childrens/addressModel.js');
var statusSchema = require('./childrens/statusModel.js');
var pointSchema = require('./childrens/pointModel.js');

var servicesSchema = new Schema({
	'idClient' : {
	 	type: Schema.Types.ObjectId,
	 	ref: 'user'
	},
	'clientName' : String,
	'clientPhone' : String,
	'clientUrl' : String,
	'idRider' : {
	 	type: Schema.Types.ObjectId,
	 	ref: 'user'
	},
	'riderName' : String,
	'riderPhone' : String,
	'riderRating' : Number,
	'riderUrl' : String,
	'active' : Boolean,
	'path' : [pointSchema], //Arreglo de puntos GPS
	'fee' : Number,
	'notes' : String,
	'totalDistance' : Number,
	'totalCash' : Number, //Si el usuario pagara con efectivo, este campo almacena el monto con el que paga
	'packageDescription' : String,
	'messageForRider' : String,
	'paymentMethod' : String, //Forma de pago
	'ratingMessageForRider' : String, //Mensaje que deja el cliente para el rider al momento de calificar
	'ratingMessageForClient' : String, //Mensaje que deja el rider para el cliente al momento de calificar
	'ratingForRider' : Number, //Calificación que se le asigna al rider en el servicio 
	'ratingForClient' : Number, //Calificación que se le asigna al cliente en el servicio
	//Campos no generados:
	'fromAddress' : addressSchema,
	'toAddress' : addressSchema,
	'currentStatus' : statusSchema, 
	/**
	 * OUT_OF_TIME:FUERA DE HORA, 
	 * PENDDING_CONFIRMATION:POR CONFIRMAR, 
	 * NEW:NUEVO, 
	 * PICKINGUP:RECOGIENDO, 
	 * AWAITING_PACKAGE:ESPERANDO PAQUETE, 
	 * DELIVERING:PAQUETE EN CAMINO, 
	 * ARRIVED:PAQUETE LLEGÓ, 
	 * CLOSED:ENVÍO TERMINADO
	 * CANCELLED_BY_CLIENT: SERVICIO CANCELADO POR EL CLIENTE
	*/
	'statusHistory' : [statusSchema],
	'creationDate' : Date,
	'deliveryDate' : Date,
	//Cuando un rider rechaza un servicio, se guardaran los ids de los riders que rechazaron
	'ridersWhoRejected' : [],
});

module.exports = mongoose.model('services', servicesSchema);
