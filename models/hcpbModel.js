var mongoose = require('mongoose');
var Schema   = mongoose.Schema;
var bcrypt = require('bcrypt');


//Importamos modelos hijo

//esquema del historial médico
var  familyHistorySchema = new mongoose.Schema({ 
	'documentNumber' :String,     
	'fullName' : String, 
	'relation' : String,
	'observation' :  String,        
	'antecedent' :  String,
	'motherPatient' : Boolean,    
	'fatherPatient' : Boolean,     
});

var medicalHistorySchema = new mongoose.Schema({ 
	'codeDisease' :String,     
	'description' : String, 
	'observation' : String,
	'diagnosisDate' :  Date,    
});

//esquema de la cartilla de vacunación
var vaccinationRecordSchema = new mongoose.Schema({ 
	'rubella'              : Boolean,
	'hepatitisB'          : Boolean,
	'papilloma'            : Boolean,
	'yellowFever'         : Boolean,
	'ah1n1'                : Boolean,
	'influenza'            : Boolean,  
});


//esquema de los embarazos previos
var previousPregnancySchema = new mongoose.Schema({ 
	
	'pregnancyType': Number, 
	'pregnancyTermination': Number, 
	'dateOfBirth': Date,         
	'observation': String,        
	'neonateStatus': Number,       
	'placeOfBirth': String,    
	
});

//deatlle de fin de embarazo
var terminatedPregnancySchema = new mongoose.Schema({ 
	'gestationalAge'  : Number,  
	'pregnancyType': Number, 
	'pregnancyTermination': Number, 
	'dateOfBirth': Date,         
	'observation': String,        
	'neonateStatus': Number,       
	'placeOfBirth': String,   
	'pretermBirth': String 
	
});


//esquema principal
var hcpbSchema = new Schema({
	
	'status': String,              
	'creationDate': Date,     
	'active': Boolean,    
	'initialDiagnostic': String,   
	'finalDiagnostic' : String,    
	'ageMenarche'     : Number, 
	'ageFirstSexualExperience'  : Number,
	'lastContraceptiveMethod' : String,
	'lastDateMenstruation' : Date,
	'pregnantHeight'  : Number,    
	'pregnantWeight'   : Number,       
	'gestationalAge'  : Number,    
	'probableDateBirth'  : Date,
	'vaccinationRecord' : vaccinationRecordSchema,
	'previousPregnancy' : [previousPregnancySchema],
	'medicalHistory' : [medicalHistorySchema],
	'familyHistory' : [familyHistorySchema],
	'terminatedPregnancy' : [terminatedPregnancySchema],
	'idPregnant' : {
		type: Schema.Types.ObjectId,
		ref: 'user'
	 },
	'idSpecialist' : {
		type: Schema.Types.ObjectId,
		ref: 'user'
	},
	'currentControl': Number,
	'currentGestationalAge': Number, 
	'nextControlDate': Date,
	'previousPretermPregnancy': Number,
	'multiplePregnancy': Number,
	'shortUterineNeck': Number,
	'highStressLevel': Number,

});


module.exports = mongoose.model('hcpb', hcpbSchema);
