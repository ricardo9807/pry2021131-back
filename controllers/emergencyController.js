var emergencyModel = require('../models/emergencyModel.js');
var signsModel = require('../models/signsModel.js');
var hcpbModel = require('../models/hcpbModel.js');
var monitoringModel = require('../models/monitoringModel.js');
var userModel = require('../models/userModel.js');
const nodemailer = require("nodemailer");
var mongoose = require('mongoose');
var Schema   = mongoose.Schema;
const moment = require('moment');

/**
 * customerController.js
 *
 * @description :: Server-side logic for managing customers.
 */
module.exports = {


    list: function (req, res) {
       //Estamos usando un pipeline de agregación
       var idHcpb = req.params.idHcpb;
       console.log('idHcpb', idHcpb)
       emergencyModel.aggregate([
           //Primera fase: extraer data que queremos
           { '$match': { 'active': true, 'idHCPB':  mongoose.Types.ObjectId(idHcpb)}},
           { "$addFields": { "custom_field": "$obj.obj_field1" } },
       ]).then((emergencys) => {
           return res.json(emergencys);
       }).catch((error) => {
           console.log('getemergencys error: ', error)
           return res.status(400).json({
               message: 'Error consultando las emergecnias registradas.',
               error: error
           });
       })
    },

    create: function (req, res) {
        //Validamos campos requeridos
        if( 
            req.body.dateAttention && req.body.clinic && req.body.initialObservation && req.body.lastControlDate

            && req.body.gestationalAge && req.body.diastolicPressure 
            && req.body.systolicPressure &&  req.body.temperature 
            && req.body.pulse &&  req.body.breathingFrequency             
            
            && req.body.systemDiagnosis  && req.body.specialistName  &&  req.body.colegiatureNumber 
            && req.body.finalDiagnosis  &&  req.body.interPregnant != null
            && req.body.terminatePregnancy != null && req.body.pretermBirth !=null

            && req.body.fetusesArray  && req.body.idHCPB  &&  req.body.idPatient 
        ){
            var fetuses= []
            if(req.body.fetusesArray.length > 0){
                for (var i = 0; i < req.body.fetusesArray.length; i++) {
                    var record =  req.body.fetusesArray[i]
                    var newHistory = {
                        fcf :record.fcf,  
                        situationFetus: record.situationFetus,   
                        presentationFetus : record.presentationFetus, 
                        observation :  record.observation,        
                        positionFetus :  record.positionFetus,
                    }
                    fetuses.push(newHistory)
                }
            }

            var emergency = new emergencyModel({

                dateAttention : moment(req.body.dateAttention).utcOffset('-0500'),
                clinic : req.body.clinic,
                initialObservation : req.body.initialObservation,
                lastControlDate : moment(req.body.lastControlDate).utcOffset('-0500'),

                gestationalAge : req.body.gestationalAge,
                systolicPressure : req.body.systolicPressure,
                diastolicPressure : req.body.diastolicPressure,                
                temperature : req.body.temperature,
                pulse : req.body.pulse,
                breathingFrequency : req.body.breathingFrequency,

                
                systemDiagnosis : req.body.systemDiagnosis,
                specialistName : req.body.specialistName,
                numberControl : req.body.numberControl,
                colegiatureNumber : req.body.colegiatureNumber,
                finalDiagnosis : req.body.finalDiagnosis,
                recommendations : req.body.recommendations ? req.body.recommendations : '',
                interPregnant : req.body.interPregnant,
                terminatePregnancy : req.body.terminatePregnancy,
                pretermBirth : req.body.pretermBirth,

                active : true,
                fetusesDetail: fetuses,
                idPatient:req.body.idPatient,
                idHCPB: req.body.idHCPB
            });

            emergency.save(function (err, newemergency) {
                if (err) {
                    return res.status(400).json({
                        message: 'Error creando registro.',
                        error: err
                    });
                }
                return res.status(200).json(newemergency);
            });

        }else{
            return res.status(400).json({
                message: 'Parámetros incompletos.'
            });
        }
    },

    update: function (req, res) {
        console.log('entró a update', req.body)
        var id = req.body.id;
        emergencyModel.findOne({_id: id}, function (err, emergency) {
            if (err) {
                return res.status(400).json({
                    message: 'Error buscando el registro.',
                    error: err
                });
            }
            else if (!emergency) {
                return res.status(404).json({
                    message: 'No se encontró el cliente.'
                });
            }
            else{
                //Actualizamos
                req.body.diastolicPressure? emergency.diastolicPressure =  req.body.diastolicPressure : null
                req.body.systolicPressure? emergency.systolicPressure = req.body.systolicPressure : null
                req.body.temperature? emergency.temperature = req.body.temperature : null
                req.body.pulse? emergency.pulse = req.body.pulse : null    
                req.body.breathingFrequency? emergency.breathingFrequency = req.body.breathingFrequency : null    
                
                req.body.systemDiagnosis? emergency.systemDiagnosis =  req.body.systemDiagnosis : null
                req.body.finalDiagnosis? emergency.finalDiagnosis = req.body.finalDiagnosis : null
                req.body.interPregnant? emergency.interPregnant = req.body.interPregnant : null
                req.body.terminatePregnancy? emergency.terminatePregnancy = req.body.terminatePregnancy : null    
                req.body.pretermBirth? emergency.pretermBirth = req.body.pretermBirth : null

                req.body.fetusesDetail? emergency.fetusesDetail = req.body.fetusesDetail : null

                //Guardamos cambios
                emergency.save(function (err, emergency) {
                    if (err) {
                        return res.status(400).json({
                            message: 'Error actualizando usuario.',
                            error: err,
                            status:400
                        });
                    }else{
                        return res.json({
                            status: 200,
                            emergency: emergency,
                        });  
                    }
                   
                });                
            }
            //return res.json(emergency);
        });
    },

    delete: function (req, res) {
        var id = req.params.id;
        emergencyModel.findOne({_id: id}, function (err, newRecord) {
            if (err) {
                return res.status(400).json({
                    message: 'Error buscando el registro.',
                    error: err
                });
            }
            if (!newRecord) {
                return res.status(404).json({
                    message: 'No se encontró el registro.'
                });
            }

            newRecord.active = false;

            newRecord.save(function (err, newRecord) {
                if (err) {
                    return res.status(400).json({
                        message: 'Error when updating newRecord.',
                        error: err
                    });
                }

                return res.json(newRecord);
            });
        });
    },

    show: function (req, res) {
        console.log('entró a show emergency ', req.params.id)
        var id = req.params.id;
        emergencyModel.findOne({_id: id}, function (err, user) {
            if (err) {
                return res.status(400).json({
                    message: 'Error buscando el registro.',
                    error: err
                });
            }
            if (!user) {
                return res.status(404).json({                    
                    message: 'No se encontró el registro.'
                });
            }
            return res.json(user);
        });
    },

    showVal: async function (req, res) {
        console.log('entró a showval')
        signsModel.find({}, function (err, signs) {
            if (err) {
                return res.status(400).json({
                    message: 'Error when getting signs.',
                    error: err
                });
            }
            return res.json(signs[0]);
        });

        // const filter = {};
        // const all = signsModel.find(filter);
        // console.log()
        // return res.json(all[0]); 
    },

    notifyEmergency: async function (req, res) {
        //Validamos campos requeridos
        console.log('body:::::::::::', req.body)
        if( 
            req.body.idPregnant  && req.body.temperature != null && req.body.pulse != null && req.body.bloodPressure != null
        ){
            //Buscar HCPB
            
            //var patient = await userModel.findOne({_id: hcpb.idPregnant})
            var patient = await userModel.findOne({_id: req.body.idPregnant})
            console.log('patient::::::', patient)
            var detail = patient.patientDetail
            console.log('idCurrentHCPB::::::', detail.idHCPB)
            var hcpb = await hcpbModel.findOne({_id:  detail.idHCPB})
            console.log('hcpb::::::', hcpb)
            var specialist = await userModel.findOne({_id: hcpb.idSpecialist})
            console.log('specialist::::::', specialist)

            var fullNameS = specialist.name + ' '+ specialist.lastName + ' '+ specialist.surName 
            var emailS= specialist.email

            var monitoring = new monitoringModel({
                dateAttention : moment(req.body.dateAttention).utcOffset('-0500'),
                idHCPB : hcpb._id,
                idPatient: patient._id,
                active: true,
                gestationalAge  : hcpb.currentGestationalAge,
                pulse : req.body.pulse,
                bloodPressure : req.body.bloodPressure, 
                temperature : req.body.temperature,
                patientComment : req.body.patientComment,
                crampPelvicArea  : req.body.crampPelvicArea,
                fatigue  : req.body.fatigue,
                abdominalPain : req.body.abdominalPain,
                swelling : req.body.swelling,
                phone : patient.phone,
                documentNumber : patient.documentNumber,
                fullName : patient.lastName +' '+ patient.surName +' '+patient.name ,
            });

            monitoring.save(function (err, newM) {
                if (err) {
                    return res.status(400).json({
                        message: 'Error creando registro.',
                        error: err
                    });
                }else{
                    var crampPelvicArea = req.body.crampPelvicArea == true ? crampPelvicArea = 'Si' : crampPelvicArea = 'No'
                    var fatigue = req.body.fatigue == true ? fatigue = 'Si' : fatigue = 'No'
                    var abdominalPain =  req.body.abdominalPain == true ? abdominalPain = 'Si' : abdominalPain = 'No'
                    var swelling =  req.body.swelling == true ? swelling = 'Si' : swelling = 'No'
                    const subject = 'Posible Emergencia Médica';
                    const text = 
                    `                            
                    Hola ${fullNameS}
                    Informe de  posible emergencia médica en paciente que usted está monitoreando:                    
                    Detalles:
                    ---------------------------------
                    HCPB: ${hcpb._id}
                    Gestante: ${patient.lastName +' '+ patient.surName +' '+patient.name}
                    DNI: ${patient.documentNumber}
                    Teléfono/celular: ${patient.phone}
                    ---------------------------------
                    Signos y síntomas:

                    Pulso: ${req.body.pulse}
                    Presión Arterial: ${req.body.bloodPressure}
                    Temperatura: ${req.body.temperature}

                    Comentario: ${req.body.patientComment}
                    Edad gestacional: ${hcpb.currentGestationalAge}
                    Calamabre en el área pélvica: ${crampPelvicArea}
                    Fatiga: ${fatigue}
                    Dolor Abdominal: ${abdominalPain}
                    Hinchazón: ${swelling}
                    ---------------------------------
                    Comunicarse con la gestante para evaluar posible emergencia obstétrica.
                    `
                    sendTextMail('sgroja6@gmail.com', emailS, 'upc2021!', subject, text).then((info) => {
                        console.log("info correo enviado: ", info)
                        res.status(200).json({
                            status : 200,
                            message: 'Se ha notificado al especialista' 
                        });
                    }).catch((error) => {
                        console.log("Hubo un error enviando el correo: ", error)
                        res.status(400).json({
                            error:error,                                        
                            message: 'No se pudo notificar al especialista.' 
                        })    
                    })
                }                
            });      

        }else{
            return res.status(400).json({
                message: 'Parámetros incompletos.'
            });
        }
    },
};


/**
 * sendTextMail: Método reutilizable para enviar correos
 * fromEmail: Correo de origen
 * toEmail: Correo destino
 * password: Contraseña de correo de origen
 * subject: Asunto
 * text: Contenido del correo
*/
const sendTextMail = async (fromEmail, toEmail, password, subject, text) => {
    console.log('sendTextMail')
    var userMail = fromEmail.split('@')
    var transporter = nodemailer.createTransport({
        host: 'smtp.gmail.com',
        port: 465,
        secure: true, 
        auth: {
            user: userMail[0], 
            pass: password, 
        },
        tls: {
            rejectUnauthorized: false
        }
    });

    // Configurar email
    var mailOptions = {
        from: fromEmail,
        to: toEmail, 
        subject: subject,
        text: text
    };  

    //Enviar correo al usuario
    console.log('enviando correo')
    const result = await transporter.sendMail(mailOptions).then((info) => {
        // console.log("Todo bien: ", info)
        console.log("Correo Enviado.")
        return info
    }).catch((error) => {
        console.log("Error: ", error)
        return error
    })
    return result;
}

