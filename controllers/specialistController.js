var userModel = require('../models/userModel.js');
var prenatalControlModel = require('../models/prenatalControlModel.js');
var hcpbModel = require('../models/hcpbModel.js');
var emergencyModel = require('../models/emergencyModel.js');
const moment = require('moment');

/**
 * customerController.js
 *
 * @description :: Server-side logic for managing customers.
 */
module.exports = {


    list: function (req, res) {
        //Estamos usando un pipeline de agregación
       userModel.aggregate([
           //Primera fase: extraer data que queremos
           { '$match': { 'active': true, 'isSpecialist': true }},
           //Segunda fase: proyectar campos deseados
           { '$project': {name: 1, lastName: 1,surName:1, secondLastName: 1, fullname: 1, documentType: 1, documentNumber: 1, phone: 1, email:1, address:1,
              active:1, idDistrict:1,birthdate:1,
              "specialistDetail.colegiatureNumber": 1, "specialistDetail.isAdmin": 1, "specialistDetail.specialtiesArray": 1 } 
           }
       ]).then((services) => {
           return res.json(services);
       }).catch((error) => {
           console.log('getSpecialists error: ', error)
           return res.status(400).json({
               message: 'Error consultando los especialistas.',
               error: error
           });
       })
    },

    reportAdmin: async function (req, res) {
        var last_30d = moment().subtract(30, 'days')
        //Total especialistas activos
        var specialists = await userModel.find({active: true, isSpecialist: true})
        console.log('total spe:::', specialists.length)
        //Total de hcpb activos
        var hcpbs = await hcpbModel.find({active: true, status: 'MONITORING'})    
        var avgGestational = 0  
        var promgest =0  
        console.log('total hcpbs:::', hcpbs.length)

        //Prom de controles

        //Prom edad gestacional
        for (var i = 0; i < hcpbs.length; i++) {
            if( hcpbs[i].currentGestationalAge ){
                promgest += hcpbs[i].currentGestationalAge 
            }            
        }
        console.log('promgest::::::', promgest)
        avgGestational = parseInt( promgest / hcpbs.length)
        console.log('avgGestational::::::', avgGestational)

        //N controles este mes
        var controls  = await prenatalControlModel.find({
            "active": true,
            "dateAttention": {"$gte": last_30d},
            }).count()
        //N emergencias este mes 
        var emergencies  = await emergencyModel.find({
            "active": true,
            "dateAttention": {"$gte": last_30d},
            }).count()
        return res.status(200).json({
            total_esp: specialists.length,
            total_hcpb: hcpbs.length,
            total_control: controls,
            total_emergencies : emergencies,
            prom_edad :avgGestational,

        });

 
    },

    create: function (req, res) {
        //Validamos campos requeridos
        if( req.body.name && req.body.lastName  && req.body.surName
            && req.body.documentNumber && req.body.birthdate && req.body.idDistrict
            && req.body.address && req.body.colegiatureNumber
            && req.body.isAdmin && req.body.phone
            && req.body.email   && req.body.specialtiesArray  
        ){
            
            var name = req.body.name
            var lastName = req.body.lastName
            var surName = req.body.surName ? req.body.surName : ''
            var fullname = name + ' ' + lastName + ' ' + surName

            var user = new userModel({
                name : name.toUpperCase(),
                lastName : lastName.toUpperCase(),
                surName : surName ? surName.toUpperCase() : '',
                fullname : fullname.toUpperCase(),
                email : req.body.email,
                documentType : 'DNI',
                documentNumber : req.body.documentNumber.toUpperCase(),
                active : true,
                creationDate : moment(new Date()).utcOffset('-0500'),
                birthdate : moment(req.body.birthdate).utcOffset('-0500'),
                isSpecialist : true,
                address: req.body.address,
                idDistrict: req.body.idDistrict,
                phone: req.body.phone,
                specialistDetail: {
                    isAdmin : req.body.isAdmin,
                    colegiatureNumber: req.body.colegiatureNumber,
                    specialtiesArray: req.body.specialtiesArray,
                }
            });
            user.password = user.generateHash('2021nuevo')
            user.save(function (err, user) {
                if (err) {
                    return res.status(400).json({
                        message: 'Error creando registro.',
                        error: err
                    });
                }
                return res.status(200).json(user);
            });

        }else{
            return res.status(400).json({
                message: 'Parámetros incompletos.'
            });
        }
    },

    update: function (req, res) {
        console.log('entró a update', req.body)
        var id = req.body.id;
        userModel.findOne({_id: id}, function (err, user) {
            if (err) {
                return res.status(400).json({
                    message: 'Error buscando el registro.',
                    error: err
                });
            }
            else if (!user) {
                return res.status(404).json({
                    message: 'No se encontró el cliente.'
                });
            }
            else{
                //Actualizamos los datos del usuario
                req.body.name? user.name =  req.body.name.toUpperCase() : null
                req.body.lastName? user.lastName = req.body.lastName.toUpperCase() : null
                req.body.surName? user.surName = req.body.surName.toUpperCase() : null
                req.body.documentNumber? user.documentNumber = req.body.documentNumber : null
                req.body.birthdate ?  user.birthdate =  req.body.birthdate : null 
                req.body.idDistrict ?  user.idDistrict =  req.body.idDistrict : null 
                req.body.address? user.address = req.body.address : null                
                req.body.email? user.email = req.body.email : null            
                req.body.phone? user.phone = req.body.phone : null      
                

                if(user.specialistDetail){
                    user.specialistDetail.colegiatureNumber = req.body.colegiatureNumber ? req.body.colegiatureNumber : user.specialistDetail.colegiatureNumber;
                    user.specialistDetail.isAdmin = req.body.isAdmin != null ? req.body.isAdmin : user.specialistDetail.isAdmin;
                    user.specialistDetail.specialtiesArray = req.body.specialtiesArray ? req.body.specialtiesArray : user.specialistDetail.specialtiesArray;
                }


                //Guardamos cambios
                user.save(function (err, user) {
                    if (err) {
                        return res.status(400).json({
                            message: 'Error actualizando usuario.',
                            error: err,
                            status:400
                        });
                    }else{
                        return res.json({
                            status: 200,
                            user: user,
                        });  
                    }
                   
                });                
            }
            //return res.json(user);
        });
    },

    delete: function (req, res) {
        var id = req.params.id;
        userModel.findOne({_id: id}, function (err, newRecord) {
            if (err) {
                return res.status(400).json({
                    message: 'Error buscando el registro.',
                    error: err
                });
            }
            if (!newRecord) {
                return res.status(404).json({
                    message: 'No se encontró la especialidad.'
                });
            }

            newRecord.active = false;

            newRecord.save(function (err, newRecord) {
                if (err) {
                    return res.status(400).json({
                        message: 'Error when updating newRecord.',
                        error: err
                    });
                }

                return res.json(newRecord);
            });
        });
    },


    /**
     * ridersController.show()
     */
    show: function (req, res) {
        console.log('entró a show rider ', req.params.id)
        var id = req.params.id;
        userModel.findOne({_id: id}, function (err, user) {
            if (err) {
                return res.status(400).json({
                    message: 'Error buscando el registro.',
                    error: err
                });
            }
            if (!user) {
                return res.status(404).json({
                    message: 'No se encontró el rider.'
                });
            }
            return res.json(user);
        });
    },

    /**
     * ridersController.search()
     */
    search: function (req, res) {
        console.log('entró a rider search', req.body)
        var filtersUser ={}
        //obtener los valores de los filtros
        req.body.fullName ? filtersUser['fullname'] = { $regex: '.*' + req.body.fullName.toUpperCase()  + '.*' }  : null;
        req.body.phone ? filtersUser['phone'] =  { $regex: '.*' + req.body.phone + '.*' }  : null;  
        req.body.documentNumber ? filtersUser['documentNumber'] =  { $regex: '.*' + req.body.documentNumber.toUpperCase()  + '.*' }  : null      
        req.body.email ? filtersUser['email'] =  { $regex: '.*' + req.body.email + '.*' } : null   
        filtersUser['active'] = true
        filtersUser['isRider'] = true
        console.log('filtros use', filtersUser )
        userModel.find(
            filtersUser
        , function (err, users) {
            if (err) {
                console.log('error',err  )
                return res.status(400).json({
                    message: 'Error when getting users.',
                    error: err
                });
            }
            else{
                var finalArray =[]
                if(req.body.isRider == true){
                    console.log('is filters ', users.length)
                    //obtener solo riders
                    for (let index = 0; index < users.length; index++) {
                        if(users[index].riderDetail.currentStatus.code == 'ACTIVE' || users[index].riderDetail.currentStatus.code == 'DELIVERING'){
                            finalArray.push(users[index])
                        }                        
                    }
                }
                console.log('is riders',finalArray.length )
                return res.json(users);
            }
            
        });
    },
   
};
