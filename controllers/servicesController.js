var servicesModel = require('../models/servicesModel.js');
var userModel = require('../models/userModel.js');
var configurationModel = require('../models/configurationModel.js');
//var serviceAccount = require('../keys/clave_privada_client_byclo-49665-firebase-adminsdk-icnis-e2f6c80af0.json');

var admin = require('firebase-admin');
const moment = require('moment');
const geolib = require('geolib');
const { log } = require('debug');

/**
 * servicesController.js
 *
 * @description :: Server-side logic for managing servicess.
 */

async function getRiders (point, ridersWhoRejected) {
    return new Promise((resolve, reject) => {
        configurationModel.find(function (err, configuration) {
            if (err) {
                reject({
                    message: 'Error when getting configuration.',
                    status: 400,
                    error: err
                })
            }
            /**
            * Variable
            * distanceMax: contiene el radio de la distancia máxima en Km por eso se multiplica por 1000
            * 
            */
            var distanceMax = configuration[0].distanceRadius * 1000
            //Estamos usando un pipeline de agregación
            userModel.aggregate([
                { "$geoNear": {
                    "near": {
                        type: "Point",
                        coordinates: point
                    },
                    "maxDistance": distanceMax,
                    "spherical": true,
                    "distanceMultiplier": 1 / 1000,
                    "distanceField": "distance",
                    "key": "riderDetail.location",
                }},
                { '$match': { 'riderDetail.currentStatus.code': { '$in': ['ACTIVE']}, '_id': { '$nin': ridersWhoRejected } }},
                { "$sort": { "distanceField": 1 }},
                //Segunda fase: proyectar campos deseados
                { '$project': { distance: 1, _id: 1, fullname: 1, phone: 1, userPhotoUrl: 1, "riderDetail.rating": 1, "riderDetail.totalServices": 1, "riderDetail.location.coordinates": 1, "riderDetail.totalCredit": 1, "riderDetail.idCurrentService": 1, "riderDetail.currentStatus": 1} }
            ])
            .then((riders) => {
                var closeRiders = []
                let nele = 3;
                riders.length > 2 ? nele = 3 : nele = riders.length
                if(configuration[0].sortBy == 'riders_closest_to_the_origin'){
                    for (let i = 0; i < nele; i++) {
                        closeRiders.push(riders[i])
                    }              
                }else{
                    closeRiders = _.orderBy(riders, ['riderDetail.totalCredit'], ['desc']);
                    for (let i = 0; i < nele; i++) {
                        closeRiders.push(closeRiders[i])
                    }                   
                }
                console.log(closeRiders);
                resolve(closeRiders);
            })
            .catch((error) => {
                console.log('getActiveRiders error: ', error) 
                reject({
                    message: 'Error consultando los riders.',
                    error: error
                    }) 
                })         
        })
    })
}

async function calculateDistance(path){
    let arrayCoordinate = path.map((item) => {
        return {
            latitude: item.coordinates[0],
            longitude: item.coordinates[1]
        }
    })
    return new Promise((resolve, reject) => {
        const dist = geolib.getPathLength(arrayCoordinate);
        resolve (dist/1000)
    })
}
async function sendNotificacion  (data){
    // console.log('sendNotifi data', data)
    // console.log('sendNotifi tokenId', data.tokenId)
    // var result = true
    // if(data.title == undefined || data.body == undefined ||  data.tokenId == undefined ){
    //     result = false
    // }else{
    //     //inicializar   
    //     if (!admin.apps.length) {
    //         admin.initializeApp({
    //             credential: admin.credential.cert(serviceAccount),
    //             databaseURL: "https://byclo-49665.firebaseio.com"
    //         })
    //     }
    //     var message = {
    //         notification: {
    //         title: data.title,
    //         body: data.body, 
    //         },
    //         token : data.tokenId
    //     };
    //     //enviar notificación
    //     await admin.messaging().send(message)
    //     .then((response) => {
    //         console.log('se envió notificación con éxito',response)
    //         result = true})
    //     .catch((error)=>{
    //         console.log('Hubo un error al enviar',error)
    //         result= false
    //     })
    // }      
    // return result
}


module.exports = {
    test: async  function (req, res) {
        var data= {}
        data.title = 'Título notificación'
        data.body = 'Mensaje notificación tessssssssssssssssssssssssssssst'

        var result = await sendNotificacion(data)
        console.log('result ', result )
        if(result == true){
            return res.status(200).json({
                message: 'Todo shido compa',
            });
        }else{
            return res.status(400).json({
                status : 400, 
                message: 'Error enviando correo',
            });
        }
    },
    /**
     * servicesController.list()
     */
    list: function (req, res) {
        servicesModel.find(function (err, services) {
            if (err) {
                return res.status(400).json({
                    message: 'Error when getting services.',
                    error: err
                });
            }
            return res.json(services);
        });
    },

    /**
     * getActiveServices: Consultaremos los servicios activos
     * para poblar el mapa del monitoreo
    */
    getActiveServices: function (req, res) {
        //Estamos usando un pipeline de agregación
        servicesModel.aggregate([ 
            //Primera fase: extraer data que queremos
            { '$match': { 'currentStatus.code': { '$in': ['NEW','PICKINGUP','DELIVERING','ARRIVED','CLOSED', 'OUT_OF_TIME']} }},
            //Segunda fase: proyectar campos deseados
            { '$project': {idRider: 1, riderName: 1, riderPhone: 1, idClient: 1, clientName: 1, clientUrl: 1,creationDate:1, notes:1,packageDescription:1,messageForRider:1, "currentStatus.code": 1, "currentStatus.description": 1, "fromAddress": 1, "toAddress": 1} }
        ]).then((services) => {
            return res.json(services);
        }).catch((error) => {
            console.log('getActiveServices error: ', error)
            return res.status(400).json({
                message: 'Error consultando los servicios.',
                error: error
            });
        })
    },

    /**
     * servicesController.show()
     */
    show: function (req, res) {
        var id = req.params.id;
        servicesModel.findOne({_id: id}, function (err, services) {
            if (err) {
                return res.status(400).json({
                    message: 'Error when getting services.',
                    error: err,
                    status: 400,
                });
            }
            else if (!services) {
                return res.status(400).json({
                    message: 'No such services',
                    status: 400
                });
            }
            else{
            return res.json(
                {   service:services,
                    status:200
                });
            }
        });
    },

    /**
     * servicesController.create(): Usado para crear un nuevo servicio
     * Se registra el nuevo servicio con estado NEW
     * Se actualiza el estado de rider a DELIVERING
     * Se coloca el idCurrentService del rider
     */
    create: async function (req, res) { 
        /**
         * Elementos
         * riderNewStatus -> Nuevo estado del rider
         * newServiceStatus -> Estado inicial para el servicio
        */
        const riderNewStatus = {
            creationDate : moment(new Date()).utcOffset('-0500'),
            code: 'DELIVERING',
            description: 'EN SERVICIO'
        }
        const newServiceStatus = {
            creationDate : moment(new Date()).utcOffset('-0500'),
            code: 'NEW',
            description: 'NUEVO'
        }
        var services = new servicesModel({
			idClient : req.body.idClient,
			clientName : req.body.clientName,
			clientPhone : req.body.clientPhone,
			clientUrl : req.body.clientUrl,
			idRider : req.body.idRider,
			riderName : req.body.riderName,
			riderPhone : req.body.riderPhone,
			riderRating : req.body.riderRating,
			riderUrl : req.body.riderUrl,
			active : true,
			currentStatus : newServiceStatus,
			path : req.body.path,
			fee : req.body.fee,
            notes : req.body.notes,
            packageDescription : req.body.packageDescription,
            messageForRider : req.body.messageForRider,
            creationDate : moment(new Date()).utcOffset('-0500'),
            statusHistory: [newServiceStatus],
            fromAddress: req.body.fromAddress,
            toAddress: req.body.toAddress,
            deliveryDate: req.body.deliveryDate,

        });
        services.save(function (err, services) {
            //Validación de error
            if (err) {
                return res.status(400).json({
                    status : 400, 
                    message: 'Error when creating services',
                    error: err
                });
            } 
            else {
                //Ahora consultamos el rider para editarlo
                userModel.findOne({_id: services.idRider}, function (err, user) {
                    if (err) {
                        return res.status(400).json({
                            message: 'Error when getting services',
                            error: err,
                            status: 400,
                        });
                    }
                    else if(user){
                        var newRiderDetail = user.riderDetail
                        newRiderDetail.currentStatus = riderNewStatus
                        //seteamos el campo idCurrentService
                        newRiderDetail.idCurrentService = services._id;
                        user.riderDetail = newRiderDetail;                        
                        user.save().then((updatedRider) => {
                            console.log('se actualizó el rider', updatedRider)
                            return res.status(201).json({
                                service:services});                                                  
                        }).catch((error) => {
                            console.log('Error actualizando rider: ', error)
                            return res.status(400).json({
                                status : 400,
                                message: 'Error actualizando rider.',
                                error: error
                            });
                        });
                    }
                    else{
                        console.log('No hay rider ', err)
                        return res.status(400).json({
                            message: 'No se encontró rider',
                            error: err,
                            status: 200,
                        });
                    }
                })
            }
        })    
    },

    /**
     * servicesController.update()
     */
    update: function (req, res) {
        console.log('entró a updateService', req.params)
        console.log('entró a body updateService', req.body)
        var id = req.params.id;
        servicesModel.findOne({_id: id}, function (err, services) {
            if (err) {
                return res.status(400).json({
                    message: 'Error when getting services',
                    error: err
                });
            }
            else if (!services) {
                return res.status(404).json({
                    message: 'No such services'
                });
            }
            else{
                services.idClient = req.body.idClient ? req.body.idClient : services.idClient;
                services.clientName = req.body.clientName ? req.body.clientName : services.clientName;
                services.clientPhone = req.body.clientPhone ? req.body.clientPhone : services.clientPhone;
                services.clientUrl = req.body.clientUrl ? req.body.clientUrl : services.clientUrl;
                services.idRider = req.body.idRider ? req.body.idRider : services.idRider;
                services.riderName = req.body.riderName ? req.body.riderName : services.riderName;
                services.riderPhone = req.body.riderPhone ? req.body.riderPhone : services.riderPhone;
                services.riderRating = req.body.riderRating ? req.body.riderRating : services.riderRating;
                services.riderUrl = req.body.riderUrl ? req.body.riderUrl : services.riderUrl;
                services.active = req.body.active ? req.body.active : services.active;
                services.status = req.body.status ? req.body.status : services.status;
                services.path = req.body.path ? req.body.path : services.path;
                services.fee = req.body.fee ? req.body.fee : services.fee;
                services.notes = req.body.notes ? req.body.notes : services.notes;
                services.packageDescription = req.body.packageDescription ? req.body.packageDescription : services.packageDescription;
                services.messageForRider = req.body.messageForRider ? req.body.messageForRider : services.messageForRider;

                services.fromAddress = req.body.fromAddress ? req.body.fromAddress : services.fromAddress;
                services.toAddress = req.body.toAddress ? req.body.toAddress : services.toAddress;

                console.log('servicio listo ', services)
                
                services.save(function (err, services) {
                    if (err) {
                        return res.status(400).json({
                            message: 'Error when updating services.',
                            error: err
                        });
                    }else{
                        return res.status(200).json({
                            status: 200,
                            service:services
                        });
                    }
    
                    
                });
            }
        });
    },

    /**
     * updateLocation: En esta función, se actualiza la posición del 
     * rider en el envío.
     */
    updateLocation: function (req, res) {
        /**
         * Elementos:
         * id -> El id del usuario
         * newLocation -> Es el primer estado que tendrá el rider
         * 
        */
        var id = req.params.id;
        const latitude = req.body.latitude;
        const longitude = req.body.longitude;

        const newLocation = {
            type: "Point",
            coordinates: [latitude,longitude]
        }

        //Consultamos el registro del rider
        servicesModel.findOne({_id: id}, function (err, service) {
            //Validación de error
            if (err) {
                return res.status(400).json({
                    message: 'Error buscando el servicio.',
                    error: err
                });
            }
            //Validación de no encontrado
            if (!service) {
                return res.status(404).json({
                    message: 'No se encontró el servicio.'
                });
            }
            //Colocamos el nuevo punto en el camino del envío

            service.path.push(newLocation);
            //Guardamos cambios
            service.save(function (err, user) {
                if (err) {
                    return res.status(400).json({
                        message: 'Error actualizando servicio.',
                        error: err
                    });
                }
                return res.json(service);
            });
        });
    },

    /**
     * setAcceptedByRider: En este método, se contempla que el rider ha aceptado el servicio.
     * El estado del servicio pasa a PICKINGUP
     * El estado del rider pasa a DELIVERING
     * Se actualiza el idCurrentService del rider
     */
    setAcceptedByRider: async function (req, res) {
        /**
         * Elementos:
         * id: es el id del servicio
         * newStatus: Es el objeto de nuevo estado para el servicio
         * riderNewStatus: Es el nuevo estado para el rider
        */
        var id = req.params.id;
        const newStatus = {
            creationDate : moment(new Date()).utcOffset('-0500'),
            code: 'PICKINGUP',
            description: 'RECOGIENDO'
        }
        const riderNewStatus = {
            creationDate : moment(new Date()).utcOffset('-0500'),
            code: 'DELIVERING',
            description: 'PAQUETE EN CAMINO'
        }
        //Buscamos el servicio a editar
        servicesModel.findOne({_id: id}, function (err, services) {
            //Validación de error
            if (err) {
                return res.status(400).json({
                    message: 'Error consultando servicio.',
                    error: err
                });
            }
            //Validación de no encontrado
            if (!services) {
                return res.status(400).json({
                    message: 'No se encontró el servicio.'
                });
            }
            //Nuevos valores
            services.currentStatus = newStatus;
            services.statusHistory.push(newStatus);
			//Guardado de cambios
            services.save(function (err, services) {
                //Validación de error
                if (err) {
                    return res.status(400).json({
                        message: 'Error actualizando servicio.',
                        error: err
                    });
                }
                else{
                    //Buscaremos el rider para modificar
                    userModel.findOne({_id: services.idRider}).then((rider) => {
                        //Variable temporal para pruebas, en la realidad el riderDetail no será nulo en este punto
                        var newRiderDetail = rider.riderDetail ? rider.riderDetail : {}
                        newRiderDetail.currentStatus = riderNewStatus
                        rider.riderDetail = newRiderDetail;
                        rider.save().then((updatedRider) => {
                            console.log('Se actualizó el rider')
                            //Notificamos al cliente
                            //Buscar al cliente
                            userModel.findOne({_id: services.idClient},async function (err, user) {
                                if (err) {
                                    console.log('no se encontró al cliente')  
                                }else{
                                    //enviar notificación si clientDetail.tokensForNotifications
                                    var result = true
                                    if(user.clientDetail.tokensForNotifications != null || user.clientDetail.tokensForNotifications != [] ){
                                        var data= {}
                                        //asignar token
                                        data.tokenId = user.clientDetail.tokensForNotifications[user.clientDetail.tokensForNotifications.length-1]
                                        //configurar título y mensaje
                                        data.title = 'Servicio aceptado'
                                        data.body = 'El rider aceptó el servicio'                        
                                        result = await sendNotificacion(data)
                                    }
                                    //validar si se envió el mensaje
                                    if(result == false){
                                        console.log('no se pudo notificar al rider')                                       
                                    } 
                                    return res.status(201).json({
                                            service:services});             
                                }
                            }); 
                            //fin de la notificación del cliente                             
                        }).catch((error) => {
                            console.log('Error consultando rider: ', error)
                            return res.status(400).json({
                                message: 'Error consultando rider.',
                                error: error
                            });
                        });
                    }).catch((error) => {
                        console.log('Error consultando rider: ', error)
                        return res.status(400).json({
                            message: 'Error consultando rider.',
                            error: error
                        });
                    })
                }
            });
        });
    },

    /**
     * setAwaitingPackage: En este método, el rider indica que ha llegado al punto
     * de origen y está esperando que el cliente le entregue el paquete.
     * El estado del servicio pasa a AWAITING_PACKAGE
    */
    setAwaitingPackage: async function (req, res) {
        /**
         * Elementos:
         * id: es el id del servicio
         * newStatus: Es el objeto de nuevo estado para el servicio
        */
        var id = req.params.id;
        const newStatus = {
            creationDate : moment(new Date()).utcOffset('-0500'),
            code: 'AWAITING_PACKAGE',
            description: 'ESPERANDO PAQUETE'
        }
        //Buscamos el servicio a editar
        servicesModel.findOne({_id: id},async function (err, services) {
            //Validación de error
            if (err) {
                return res.status(400).json({
                    message: 'Error consultando servicio.',
                    error: err
                });
            }
            //Validación de no encontrado
            if (!services) {
                return res.status(400).json({
                    message: 'No se encontró el servicio.'
                });
            }
            //Nuevos valores
            services.currentStatus = newStatus;
            services.statusHistory.push(newStatus);
            
            //Guardamos los cambios
            services.save(function (err, services) {
                //Validación de error
                if (err) {
                    return res.status(400).json({
                        message: 'Error actualizando servicio.',
                        error: err
                    });
                }
                else{
                    //Notificamos al cliente
                    //Buscar al cliente
                    userModel.findOne({_id: services.idClient},async function (err, user) {
                        if (err) {
                            console.log('No se encontró al cliente')
                        }else{
                            //enviar notificación si clientDetail.tokensForNotifications
                            var result = true
                            if(user.clientDetail.tokensForNotifications != null || user.clientDetail.tokensForNotifications != [] ){
                                var data= {}
                                //asignar token
                                data.tokenId = user.clientDetail.tokensForNotifications[user.clientDetail.tokensForNotifications.length-1]
                                //configurar título y mensaje
                                data.title = 'El rider ha llegado'
                                data.body = 'El rider ha llegado al punto de origen'                        
                                result = await sendNotificacion(data)
                            }
                            //validar si se envió el mensaje
                            if(result == false){
                                console.log('No se pudo notificar al cliente')                                     
                            } 
                            return res.status(200).json({
                                    service:services});   
                                                           
                        }
                    }); 
                    //fin de la notificación del cliente 
                }
            });
        });
    },

    /**
     * setDelivering: En este método, se contempla que el rider ha marcado el servicio como en camino.
     * El estado del servicio pasa a DELIVERING
     */
    setDelivering: async function (req, res) {
        /**
         * Elementos:
         * id: es el id del servicio
         * newStatus: Es el objeto de nuevo estado para el servicio
        */
        var id = req.params.id;
        const newStatus = {
            creationDate : moment(new Date()).utcOffset('-0500'),
            code: 'DELIVERING',
            description: 'PAQUETE EN CAMINO'
        }

        //Buscamos el servicio a editar
        servicesModel.findOne({_id: id}, function (err, services) {
            //Validación de error
            if (err) {
                return res.status(400).json({
                    message: 'Error consultando servicio.',
                    error: err
                });
            }
            //Validación de no encontrado
            if (!services) {
                return res.status(400).json({
                    message: 'No se encontró el servicio.'
                });
            }
            //Nuevos valores
            services.currentStatus = newStatus;
            services.statusHistory.push(newStatus);

            //Acotar los puntos del path para solo mostrar la última posición
            var originPoint = services.path[services.path.length - 1]
            services.path = []
            services.path.push(originPoint)
            
            //Guardamos los cambios
            services.save(function (err, services) {
                //Validación de error
                if (err) {
                    return res.status(400).json({
                        message: 'Error actualizando servicio.',
                        error: err
                    });
                }else{
                    //Notificamos al cliente
                    //Buscar al cliente
                    userModel.findOne({_id: services.idClient},async function (err, user) {
                        if (err) {
                            console.log('No se pudo encontró al cliente')   
                        }else{
                            //enviar notificación si clientDetail.tokensForNotifications
                            var result = true
                            if(user.clientDetail.tokensForNotifications.length == 0){
                                console.log('No se pudo notificar al cliente')        
                            }else{
                                var data= {}
                                //asignar token
                                data.tokenId = user.clientDetail.tokensForNotifications[user.clientDetail.tokensForNotifications.length-1]
                                //configurar título y mensaje
                                data.title = 'Rider en camino'
                                data.body = 'El rider está en camino '                      
                                result = await sendNotificacion(data)
                            }
                            return res.status(200).json({
                                service:services});                                
                        }
                    }); 
                    //fin de la notificación del cliente 
                }                
            });
        });
    },

    /**
     * setRiderArrived: En este método, se contempla que el rider ha llegado al destino.
     * El estado del servicio pasa a ARRIVED
     */
    setRiderArrived: async function (req, res) {
        /**
         * Elementos:
         * id: es el id del servicio
         * newStatus: Es el objeto de nuevo estado para el servicio
        */
        var id = req.params.id;
        const newStatus = {
            creationDate : moment(new Date()).utcOffset('-0500'),
            code: 'ARRIVED',
            description: 'PAQUETE LLEGÓ'
        }
        //Buscamos el servicio a editar
        servicesModel.findOne({_id: id}, function (err, services) {
            //Validación de error
            if (err) {
                return res.status(400).json({
                    message: 'Error consultando servicio.',
                    error: err
                });
            }
            //Validación de no encontrado
            if (!services) {
                return res.status(400).json({
                    message: 'No se encontró el servicio.'
                });
            }
            //Nuevos valores
            services.currentStatus = newStatus;
            services.statusHistory.push(newStatus);
            
            //Guardamos los cambios
            services.save(function (err, services) {
                //Validación de error
                if (err) {
                    return res.status(400).json({
                        message: 'Error actualizando servicio.',
                        error: err
                    });
                }else{
                    //Notificamos al cliente
                    //Buscar al cliente
                    userModel.findOne({_id: services.idClient}, async function (err, user) {
                        if (err) {
                            console.log('No se pudo encontrar al cliente')   
                        }else{
                            //enviar notificación si clientDetail.tokensForNotifications
                            var result = true
                            if(user.clientDetail.tokensForNotifications != null || user.clientDetail.tokensForNotifications != [] ){
                                var data= {}
                                //asignar token
                                data.tokenId = user.clientDetail.tokensForNotifications[user.clientDetail.tokensForNotifications.length-1]
                                //configurar título y mensaje
                                data.title = 'Tu pedido ha llegado'
                                data.body = 'El rider llegó a tu destino'                        
                                result = await sendNotificacion(data)
                            }
                            //validar si se envió el mensaje
                            if(result == false){
                                console.log('No se pudo notificar al cliente')                                    
                            } 
                            return res.status(201).json({
                                    service:services});             
                        }
                    }); 
                    //fin de la notificación del cliente                     
                }

                //return res.json(services);
            });
        });
    },

    /**
     * setClosedByRider: En este método, se contempla que el rider ha terminado el servicio.
     * El estado del servicio pasa a CLOSED
     * El estado del rider pasa a ACTIVE
     * Se actualiza el idCurrentService del rider
     */
    setClosedByRider: function (req, res) {
        /**
         * Elementos:
         * id: es el id del servicio
         * newStatus: Es el objeto de nuevo estado para el servicio
         * riderNewStatus: Es el nuevo estado para el rider
        */
        var id = req.params.id;
        const newStatus = {
            creationDate : moment(new Date()).utcOffset('-0500'),
            code: 'CLOSED',
            description: 'ENVÍO TERMINADO'
        }
        const riderNewStatus = {
            creationDate : moment(new Date()).utcOffset('-0500'),
            code: 'ACTIVE',
            description: 'Activo'
        }
        //Buscamos el servicio a editar
        servicesModel.findOne({_id: id}, async function (err, services) {
            //Validación de error
            if (err) {
                return res.status(400).json({
                    message: 'Error consultando servicio.',
                    error: err
                });
            }
            //Validación de no encontrado
            if (!services) {
                return res.status(400).json({
                    message: 'No se encontró el servicio.'
                });
            }
            //Nuevos valores
            services.currentStatus = newStatus;
            services.statusHistory.push(newStatus);
            //Agregamos la distancia recorrida
            services.totalDistance = await calculateDistance(services.path);
			//Guardado de cambios
            services.save(function (err, services) {
                //Validación de error
                if (err) {
                    return res.status(400).json({
                        message: 'Error actualizando servicio.',
                        error: err
                    });
                }
                //actualizamos al rider
                userModel.findOne({_id: services.idRider}).then((rider) => {
                    //Variable temporal para pruebas, en la relaidad el riderDetail no será nulo en este punto
                    var newRiderDetail = rider.riderDetail ? rider.riderDetail : {}
                    //cambiamos el estado actual
                    newRiderDetail.currentStatus = riderNewStatus
                    //eliminamos el campo idCurrentService
                    newRiderDetail.idCurrentService = undefined
                    //sumamos la distancia recorrida por el rider
                    newRiderDetail.totalDistance = services.totalDistance + rider.riderDetail.totalDistance
                    //Agregamos el servicio a la lista de últimos del rider
                    if(newRiderDetail.lastServices.length < 10){
                        //Si tiene menos de 10 servicios, agregamos el servicio
                        //No colocamos el path porque son muchos puntos
                        services.path = []
                        newRiderDetail.lastServices.push(services)
                    }else{
                        //Eliminamos el servicio más antiguo
                        newRiderDetail.lastServices.shift();
                        //Agregamos el servicio
                        newRiderDetail.lastServices.push(services)
                    }
                    //Agregamos 1 al total de servicios
                    newRiderDetail.totalServices++
                    //reducimos el crédito del rider
                    newRiderDetail.totalCredit--
                    rider.riderDetail = newRiderDetail;
                    rider.save().then((updatedRider) => {
                        //Actualizamos al cliente
                        userModel.findOne({_id: services.idClient}).then((client) => {
                            //Obtenemos el detalle del cliente a modificar
                            var newClientDetail = client.clientDetail ? client.clientDetail : {}
                            //Agregamos el servicio a la lista de últimos del client
                            if(newClientDetail.lastServices.length < 10){
                                //Si tiene menos de 10 servicios, agregamos el servicio
                                newClientDetail.lastServices.push(services)
                            }else{
                                //Eliminamos el servicio más antiguo
                                newClientDetail.lastServices.shift();
                                //Agregamos el servicio
                                newClientDetail.lastServices.push(services)
                            }
                            //sumamos la distancia del nuevo servicio 
                            client.clientDetail.totalDistance = services.totalDistance + client.clientDetail.totalDistance
                            //Agregamos 1 al total de servicios
                            newClientDetail.totalServices++
                            client.clientDetail = newClientDetail;
                            client.save().then((updatedClient) => {
                                // console.log('Rider modificado con éxito!: ', updatedClient)
                                return res.json(services);
                            }).catch((error) => {
                                console.log('Error actualizando cliente: ', error)
                                return res.status(400).json({
                                    message: 'Error actualizando cliente.',
                                    error: error
                                });
                            });
                        }).catch((error) => {
                            console.log('Error consultando cliente: ', error)
                            return res.status(400).json({
                                message: 'Error consultando cliente.',
                                error: error
                            });
                        })
                    }).catch((error) => {
                        console.log('Error actualizando rider: ', error)
                        return res.status(400).json({
                            message: 'Error actualizando rider.',
                            error: error
                        });
                    });
                }).catch((error) => {
                    console.log('Error consultando rider: ', error)
                    return res.status(400).json({
                        message: 'Error consultando rider.',
                        error: error
                    });
                })

                
            });
        });
    },

    /**
     * servicesController.remove()
     */
    remove: function (req, res) {
        var id = req.params.id;
        servicesModel.findByIdAndRemove(id, function (err, services) {
            if (err) {
                return res.status(400).json({
                    message: 'Error when deleting the services.',
                    error: err
                });
            }
            return res.status(204).json();
        });
    },

    //Service from the app----------------------------

    /**
     * servicesController: Usado para crear un nuevo servicio
     * Se registra el nuevo servicio con estado PENDDING_CONFIRMATION porque se asignará un rider después
     */
    createServiceForTheApp: function (req, res) {
        /**
         * Elementos
         * newServiceStatus -> Estado inicial para el servicio
        */
        configurationModel.find(function (err, configuration) {
            if (err) {
                reject({
                    message: 'Error when getting configuration.',
                    status: 400,
                    error: err
                })
            }
            var hour = moment(new Date()).utcOffset('-0500').format("HH")
            var minutes = moment(new Date()).utcOffset('-0500').format("mm")

            let hourStart = parseInt(configuration[0].bussinessHour.start.hour ? configuration[0].bussinessHour.start.hour : 0,10)
            let minuteStart = configuration[0].bussinessHour.start.minute
            let hourEnd = parseInt(configuration[0].bussinessHour.end.hour ? configuration[0].bussinessHour.end.hour : 0,10)
            let minuteEnd = configuration[0].bussinessHour.end.minute

            let isHour = true
            let newServiceStatus  ={}

            if(hour >= hourStart && hour <= hourEnd ){
                newServiceStatus = {
                    creationDate : moment(new Date()).utcOffset('-0500'),
                    code: 'PENDDING_CONFIRMATION',
                    description: 'POR CONFIRMAR'
                }
            }else{
                isHour = false
                newServiceStatus = {
                    creationDate : moment(new Date()).utcOffset('-0500'),
                    code: 'OUT_OF_TIME',
                    description: 'FUERA DE HORA'
                }
            }
                
            var services = new servicesModel({
                idClient : req.body.idClient,
                clientName : req.body.clientName,
                clientPhone : req.body.clientPhone,
                clientUrl : req.body.clientUrl,
                active : true,
                currentStatus : newServiceStatus,
                fee : req.body.fee,
                paymentMethod: req.body.paymentMethod,
                notes : req.body.notes,
                packageDescription: req.body.packageDescription,
                messageForRider: req.body.messageForRider,
                creationDate : moment(new Date()).utcOffset('-0500'),
                statusHistory: [newServiceStatus],
                fromAddress: req.body.fromAddress,
                toAddress: req.body.toAddress,
                ridersWhoRejected:[]
            });
            req.body.deliveryDate ? services.deliveryDate = req.body.deliveryDate : null
            //Validamos el metodo de pago, si es efectivo debe enviarse le monto con el que paga
            if(req.body.paymentMethod == 'EFECTIVO'){
                if(req.body.totalCash){
                    services.totalCash = req.body.totalCash
                }else{
                    return res.status(400).json({
                        message: 'Debe ingresar el monto con el que cancelará.' 
                    })
                } 
            }
            services.save(function (err, services) {
                //Validación de error
                if (err) {
                    console.log(err);
                    return res.status(400).json({
                        status : 400, 
                        message: 'Error when creating services',
                        error: err
                    });
                }
                
                if(hour >= hourStart && hour <= hourEnd){                    
                    //Notificamos al cliente
                    //Buscar al cliente
                    userModel.findOne({_id: services.idClient}, async function (err, user) {
                        if (err) {
                            return res.status(400).json({
                                message: 'Error when getting client',
                                error: err,
                                status: 400,
                            });
                        }else{
                                return res.status(200).json(services)  
                        }
                    }); 
                    //fin de la notificación del cliente                     
                }else{
                    return res.status(400).json({
                        title: 'Horario de atención',
                        message: 'No fue posible registrar su pedido, nuestro horario de atención es de ' + hourStart + ':'+ minuteStart + ' a ' + hourEnd + ':'+ minuteEnd + '. Gracias por su comprensión.' 
                    })
                }

            });
        })
    },

    /**
     * getRiders: En este método, permite obtener los tres riders más cercanos de donde está el usuario
     * 
     */
    getCloseRiders: function (req, res) {
        var id = req.params.id;
        //Buscamos el servicio
        servicesModel.findOne({_id: id}, async function (err, services){
            if (err) {
                return res.status(400).json({
                    message: 'Error buscando riders.',
                    error: err
                });
            }
            /**
            * Variable
            * point: contiene las coordenadas de la dirección desde dónde se enviará el paquete
            * 
            */
            var point = services.fromAddress.location.coordinates
            var ridersWhoRejected  = services.ridersWhoRejected
            //let riders = await getRiders(point);    
            getRiders(point, ridersWhoRejected)
            .then((riders) => {
                console.log(riders);
                return res.status(200).json(riders);
            })
            .catch((error) => {
                return res.status(400).json({
                    message: 'Error buscando riders',
                    error: error
                });
            })

        })
    },

    /**
     * requestRider: En este método, permite asignar un rider al servicio
     * 
     */
    requestRider:async function (req, res) {
        console.log(req.body);
        if(req.body.idRider && req.body.riderName && req.body.riderPhone && req.body.point){
            var id = req.params.id;
            //Buscamos el servicio
            servicesModel.findOne({_id: id}, async function (err, services){
                if (err) {
                    return res.status(400).json({
                        message: 'Error buscando el servicio.',
                        error: err
                    });
                }
                /**
                * Variable
                * point: contiene las coordenadas de la dirección desde dónde se enviará el paquete
                * riderNewStatus: contiene el nuevo estado del rider
                * newServiceStatus: contiene el nuevo estado del servicio
                * 
                */
                var point = {
                    type: 'Point',
                    coordinates: [req.body.point.latitude, req.body.point.longitude]
                }
                const riderNewStatus = {
                    creationDate : moment(new Date()).utcOffset('-0500'),
                    code: 'DELIVERING',
                    description: 'PAQUETE EN CAMINO'
                }
                const newServiceStatus = {
                    creationDate : moment(new Date()).utcOffset('-0500'),
                    code: 'NEW',
                    description: 'NUEVO'
                }
                
                services.idRider = req.body.idRider
                services.riderName = req.body.riderName
                services.riderPhone = req.body.riderPhone
                req.body.riderUrl ? services.riderUrl = req.body.riderUrl : services.riderUrl = null
                services.currentStatus = newServiceStatus
                services.statusHistory.push(newServiceStatus)
                services.path = [point]
                
                
                //Guardamos los cambios
                services.save(function (err, services) {
                    //Validación de error
                    if (err) {
                        return res.status(400).json({
                            message: 'Error actualizando servicio.',
                            error: err
                        });
                    }else{
                        userModel.findOne({_id: req.body.idRider},async function (err, user) {
                            if (err) {
                                return res.status(400).json({
                                    message: 'Error obteniendo al rider',
                                    error: err
                                });
                            }
                            else if(user){
                                var newRiderDetail = user.riderDetail
                                newRiderDetail.currentStatus = riderNewStatus
                                //seteamos el campo idCurrentService
                                newRiderDetail.idCurrentService = services._id;
                                user.riderDetail = newRiderDetail;                                
                                user.save().then((updatedRider) => {
                                    console.log('se actualizó el rider', updatedRider)
                                    //Notificamos al cliente
                                    //Buscar al cliente
                                    userModel.findOne({_id: services.idClient},async function (err, user) {
                                        if (err) {
                                            console.log('No se pudo encontrar al cliente')  
                                        }else{
                                            //enviar notificación si clientDetail.tokensForNotifications
                                            var result = true
                                            if(user.clientDetail.tokensForNotifications != null || user.clientDetail.tokensForNotifications != [] ){
                                                var data= {}
                                                //asignar token
                                                data.tokenId = user.clientDetail.tokensForNotifications[user.clientDetail.tokensForNotifications.length-1]
                                                //configurar título y mensaje
                                                data.title = 'Servicio asignado'
                                                data.body = 'Se asignó un rider al servicio'                        
                                                result = await sendNotificacion(data)
                                            }
                                            //validar si se envió el mensaje
                                            if(result == false){
                                                console.log('No se pudo notificar al cliente')                                      
                                            } 
                                              return res.status(200).json({services});
                                                                             
                                        }
                                    }); 
                                    //fin de la notificación del cliente 
                                    
                                    
                                }).catch((error) => {
                                    console.log('Error actualizando rider: ', error)
                                    return res.status(400).json({
                                        message: 'Error actualizando rider.',
                                        error: error
                                    });
                                });
                            }
                            else{
                                console.log('No hay rider ', err)
                                return res.status(400).json({
                                    message: 'No se encontró rider',
                                    error: err,
                                });
                            }
                        })
                    }    
                });
            })
        }else{
            return res.status(400).json({
                message: 'Complete todos los campos',
            });
        }
    },
    
    /**
     * getServiceStatus: En este método, permite ver el estado del servicio
     * 
     */
    getServiceStatus: function (req, res) {
        var id = req.params.id;
        //Buscamos el servicio
        servicesModel.findOne({_id: id}, async function (err, service){
            if (err) {
                return res.status(400).json({
                    message: 'Error buscando el servicio.',
                    error: err
                });
            }
            else{
                return res.status(200).json(service)
            }
        })
},    

    /**
     * rateRiderByClient: En este método, permite asignar una calificación al rider, por parte del cliente
     * 
     */
    rateRiderByClient: function (req, res) {
        var id = req.params.id;
        //Buscamos el servicio a editar
        if(req.body.rating){
            servicesModel.findOne({_id: id}, function (err, services) {
                req.body.ratingMessageForRider ? services.ratingMessageForRider = req.body.ratingMessageForRider : null
                services.ratingForRider = req.body.rating
                services.save(function (err, services) {
                    if (err) {
                        return res.status(400).json({
                            message: 'Error calificando al rider en el servicio.',
                            error: err
                        });
                    }
                    userModel.findOne({_id: services.idRider}, function (err, rider) {
                        /**
                        * Variable
                        * totalServices: contiene los servicio realizados por el rider menos el último que se va a calificar
                        * rating: contiene la calificación actual del rider
                        * newRating: contiene la nueva calificación del rider para lo cual se suma el nuevo valor y se divide entre dos                         
                        */
                        var newRating =  parseInt(req.body.rating)
                            
                        // si el cliente tiene mas de un servicio, entonces se calcular el total de servicio, sino, se le asigna 1
                        var totalServices = 1
                        rider.riderDetail.totalServices > 1 ? totalServices = rider.riderDetail.totalServices - 1 : rider.riderDetail.totalServices = totalServices
                        
                        // Actualizar la sumatoria de rating's 
                        var flagRatingSumDB = isNaN(rider.riderDetail.ratingSum)
                        if(flagRatingSumDB == false){
                            ratingSumDB = rider.riderDetail.ratingSum
                            //sumar nuevo rating
                            rider.riderDetail.ratingSum = rider.riderDetail.ratingSum + newRating
                        }else{
                            rider.riderDetail.ratingSum = newRating
                        }  
                        
                        // calculamos el nuevo rating del cliente
                        rider.riderDetail.rating =(rider.riderDetail.ratingSum  / rider.riderDetail.totalServices).toFixed(1)    

                        rider.save(function (err, riderUpdated) {
                            if (err) {
                                return res.status(400).json({
                                    message: 'No se pudo calificar al rider.',
                                    error: err
                                });
                            }
                            return res.status(200).json({riderUpdated});
                        })
                    })
                });
            })

        }else{
            return res.status(204).json({message: 'Parámetros incompletos'});
        }

    },
    /**
     * servicesController.search()
     */
    search: function (req, res) {
        console.log('entró a clients search', req.body)
        var filterService ={}
        //obtener los valores de los filtros
        req.body.riderName ? filterService['riderName'] = { $regex: '.*' + req.body.riderName.toUpperCase()  + '.*' }  : null;
        req.body.clientName ? filterService['clientName'] = { $regex: '.*' + req.body.clientName.toUpperCase()  + '.*' }  : null;
        req.body.riderPhone ? filterService['riderPhone'] =  { $regex: '.*' + req.body.riderPhone + '.*' }  : null;  
        filterService['active'] = true

        console.log('filtros use', filterService )
        servicesModel.find(
            filterService
        , function (err, services) {
            if (err) {
                console.log('error',err  )
                return res.status(400).json({
                    message: 'Error when getting services.',
                    error: err
                });
            }
            else{
                var finalArray =[]
                //si se consulta el estado del servicio
                if(req.body.status){
                    //obtener solo los registros con ese estado
                    for (let index = 0; index < services.length; index++) {
                        if(services[index].currentStatus.code == req.body.status){
                            finalArray.push(services[index])
                        }                        
                    }
                }else{
                    finalArray = services
                }
                console.log('total users',finalArray.length )
                return res.json(
                    finalArray
                    );
            }
            
        });
    },   

    /**
     * rateClientByRider: En este método, permite asignar una calificación al cliente por parte del rider
     * 
     */
    rateClientByRider: function (req, res) {
        console.log('parámetros ',req.params.id )
        var id = req.params.id;
        //Buscamos el servicio a editar
        if(req.body.rating){
            servicesModel.findOne({_id: id}, function (err, services) {
                req.body.ratingMessageForClient ? services.ratingMessageForClient = req.body.ratingMessageForClient : null
                services.ratingForClient = req.body.rating
                services.save(function (err, services) {
                    if (err) {
                        return res.status(400).json({
                            message: 'Error calificando al cliente en el servicio.',
                            error: err
                        });
                    }
                    userModel.findOne({_id: services.idClient}, function (err, client) {

                        /**
                        * Variable
                        * totalServices: contiene los servicio realizados por el rider menos el último que se va a calificar
                        * rating: contiene la calificación actual del rider
                        * newRating: contiene la nueva calificación del rider para lo cual se suma el nuevo valor y se divide entre dos                         
                        */
                        var newRating =  parseInt(req.body.rating)
                        
                        // si el cliente tiene mas de un servicio, entonces se calcular el total de servicio, sino, se le asigna 1
                        var totalServices = 1
                        client.clientDetail.totalServices > 1 ? totalServices = client.clientDetail.totalServices - 1 : client.clientDetail.totalServices = totalServices
                        
                        // Actualizar la sumatoria de rating's 
                        var flagRatingSumDB = isNaN(client.clientDetail.ratingSum)
                        if(flagRatingSumDB == false){
                            ratingSumDB = client.clientDetail.ratingSum
                            //sumar nuevo rating
                            client.clientDetail.ratingSum = client.clientDetail.ratingSum + newRating
                        }else{
                            client.clientDetail.ratingSum = newRating
                        }                        
                        
                        // calculamos el nuevo rating del cliente
                        client.clientDetail.rating = (client.clientDetail.ratingSum  / client.clientDetail.totalServices).toFixed(1)                        
                        console.log('último client.clientDetail.rating ',client.clientDetail.rating )
                        client.save(function (err, clientUpdated) {
                            if (err) {
                                return res.status(400).json({
                                    message: 'Error calificando al cliente.',
                                    error: err
                                });
                            }
                            return res.status(200).json(clientUpdated);
                        })
                    })
                });
            })

        }else{
            return res.status(400).json({message: 'Parámetros incompletos'});
        }

    },

    /**
     * setRejectedByRider: En este método, permite rechazar el servicio por parte de un rider
     * 
     */
    setRejectedByRider: function (req, res){
        /**
         * Elementos:
         * id: es el id del servicio
         * newStatus: Es el objeto de nuevo estado para el servicio (Rechazado)
         * riderNewStatus: Es el nuevo estado para el rider (Activo)
        */
        var id = req.params.id;
        const newStatus = {
           creationDate : moment(new Date()).utcOffset('-0500'),
           code: 'REJECTED_BY_RIDER',
           description: 'RECHAZADO POR EL RIDER'
        }
        const riderNewStatus = {
            creationDate : moment(new Date()).utcOffset('-0500'),
            code: 'ACTIVE',
            description: 'Activo'
        }
       //Buscamos el servicio a editar
       servicesModel.findOne({_id: id}, function (err, services) {
           //Validación de error
           if (err) {
               return res.status(400).json({
                   message: 'Error consultando servicio.',
                   error: err
               });
           }
           //Validación de no encontrado
           if (!services) {
               return res.status(400).json({
                   message: 'No se encontró el servicio.'
               });
           }
           //agregamos el rider que rechazo el servicio
           services.ridersWhoRejected.push( services.idRider );
           //Nuevos valores
           services.currentStatus = newStatus;
           services.statusHistory.push(newStatus);
           //Guardado de cambios
           services.save(function (err, services) {
               //Validación de error
               if (err) {
                   return res.status(400).json({
                       message: 'Error actualizando servicio.',
                       error: err
                   });
               }
               //Buscaremos el rider para modificar
               userModel.findOne({_id: services.idRider}).then((rider) => {
                    rider.riderDetail.currentStatus = riderNewStatus
                    rider.save().then((updatedRider) => {
                        // console.log('Rider modificado con éxito!: ', updatedRider)
                        return res.json(services);
                    }).catch((error) => {
                        console.log('Error consultando rider: ', error)
                        return res.status(400).json({
                            message: 'Error consultando rider.',
                            error: error
                        });
                    });
                }).catch((error) => {
                    console.log('Error consultando rider: ', error)
                    return res.status(400).json({
                        message: 'Error consultando rider.',
                        error: error
                    });
                })
            });
        });
    },

    /**
     * setCanceledByClient: En este método, permite cancelar el servicio por parte de un cliente
     * 
     */
    setCanceledByClient: function (req, res){
        /**
         * Elementos:
         * id: es el id del servicio
         * newStatus: Es el objeto de nuevo estado para el servicio (Cancelado)
         * riderNewStatus: Es el nuevo estado para el rider (Activo)
        */
        var id = req.params.id;
        const newStatus = {
           creationDate : moment(new Date()).utcOffset('-0500'),
           code: 'CANCELLED_BY_CLIENT',
           description: 'CANCELADO POR EL CLIENTE'
        }
        const riderNewStatus = {
            creationDate : moment(new Date()).utcOffset('-0500'),
            code: 'ACTIVE',
            description: 'Activo'
        }
       //Buscamos el servicio a editar
       servicesModel.findOne({_id: id}, function (err, services) {
           //Validación de error
           if (err) {
               return res.status(400).json({
                   message: 'Error consultando servicio.',
                   error: err
               });
           }
           //Validación de no encontrado
           if (!services) {
               return res.status(400).json({
                   message: 'No se encontró el servicio.'
               });
           }
           //Nuevos valores
           services.currentStatus = newStatus;
           services.statusHistory.push(newStatus);
           //Guardado de cambios
           services.save(function (err, services) {
               //Validación de error
               if (err) {
                   return res.status(400).json({
                       message: 'Error actualizando servicio.',
                       error: err
                   });
               }
               //Buscaremos el rider para modificar
               userModel.findOne({_id: services.idRider}).then((rider) => {
                    rider.riderDetail.currentStatus = riderNewStatus
                    rider.save().then((updatedRider) => {
                        // console.log('Rider modificado con éxito!: ', updatedRider)
                        return res.json(services);
                    }).catch((error) => {
                        console.log('Error consultando rider: ', error)
                        return res.status(400).json({
                            message: 'Error consultando rider.',
                            error: error
                        });
                    });
                }).catch((error) => {
                    console.log('Error consultando rider: ', error)
                    return res.status(400).json({
                        message: 'Error consultando rider.',
                        error: error
                    });
                })
            });
        });
    }

};
