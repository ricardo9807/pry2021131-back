var newsModel = require('../models/newsModel.js');
var moment = require('moment');
/**
 * newsController.js
 *
 * @description :: Server-side logic for managing news.
 */
module.exports = {

    /**
     * newsController.list()
     */
    list: function (req, res) {
        newsModel.find({active: true}, function (err, news) {
            if (err) {
                return res.status(400).json({
                    message: 'Error when getting news.',
                    error: err
                });
            }
            return res.json(news);
        });
    },

    /**
     * newsController.search()
     */
    search: function (req, res) {
        console.log('entró a clients search', req.body)
        var filters ={}
        //obtener los valores de los filtros
        req.body.size ? filters['size'] = req.body.size  : null;
        req.body.target ? filters['target'] = req.body.target  : null;
        req.body.title ? filters['title'] =  { $regex: '.*' + req.body.title + '.*' }  : null;  
        filters['active'] = true

        console.log('filtros use', filters )
        newsModel.find(
            filters
        , function (err, finalArray) {
            if (err) {
                console.log('error',err  )
                return res.status(400).json({
                    message: 'Error when getting news.',
                    error: err
                });
            }
            else{
                return res.json(finalArray);
            }
            
        });
    },

    /**
     * newsController.searchOp()
     */
    searchOp: function (req, res) {
        console.log('entró', req.body)
        /**
         * modelo de lo que va a recibir
         * req.body.filters:[
         * {
         *   name: "title", 
         *   value: "tit",
         *   operator: "include"
         * },
         * {
         *   name: "title", 
         *   value: "tit",
         *   operator: "F"
         * },
         * {
         *   name: "title", 
         *   value: "tit",
         *   operator: "include"
         * },
         * ]
         * 
         * 
         */
        var filtersArray = req.body.filters    
        //recorrer el arreglo para armar el filtro
        for (let index = 0; index < filtersArray.length; index++) {
            if(filtersArray[index].type == 'numeric'){
                var operator = filtersArray[index].operator
                switch (operator) {
                    case '=':
                      filters[nameObj] = filtersArray[index].value;;
                      break;
                    case '>':
                      filters[nameObj] = { $gt:  filtersArray[index].value  };
                      break;
                    case '<':
                      filters[nameObj] = { $lt:  filtersArray[index].value  };
                      break;
                    case '>=':
                      filters[nameObj] = { $gte:  filtersArray[index].value  };
                      break;
                    case '<=':
                      filters[nameObj] = { $lte:  filtersArray[index].value  };
                      break;
                    default:
                      filters[nameObj] = filtersArray[index].value;;
                  }
            }
            
        }
        var filters = {}
        filters['active'] = true
        req.query.size != 'undefined' ? filters['size'] =  req.query.size  : null
        req.query.target != 'undefined' ? filters['target'] =  req.query.target  : null        
        req.query.title != 'undefined' ? filters['title'] = { $regex: '.*' + req.query.title + '.*' } :null
        console.log('filters ', filters)   
        newsModel.find(
            filters
        , function (err, news) {
            if (err) {
                console.log('error',err  )
                return res.status(400).json({
                    message: 'Error when getting news.',
                    error: err
                });
            }
            return res.json(news);
        });
    },

    /**
     * newsController.show()
     */
    show: function (req, res) {
        var id = req.params.id;
        newsModel.findOne({_id: id}, function (err, newRecord) {
            if (err) {
                return res.status(400).json({
                    message: 'Error buscando el registro..',
                    error: err
                });
            }
            if (!newRecord) {
                return res.status(404).json({
                    message: 'No se encontró la noticia.'
                });
            }
            return res.json(newRecord);
        });
    },

    /**
     * newsController.create()
     * Esta función se usará para crear noticias: 
     * esde la intranet.
     */
    create: function (req, res) {
        //Validamos campos requeridos
        // console.log('create: ', req.body)
        if(req.body.title && req.body.target && req.body.content){

            /**
             * Variables: 
             * target: Contiene el público objetivo de la noticia.
             * attachment: Contiene la imagen de la noticia.
             * published: Flag que indica si debe publicarse o no.
             * content: El contenido en texto de la noticia.
             * size: El tamaño de la noticia.
             */

            var target = req.body.target
            var attachment = req.body.attachment
            var published = false
            var content = req.body.content
            var title = req.body.title
            var size = req.body.size

            var newRecord = new newsModel({
                target : target,
                content : content,
                attachment : attachment,
                published : published,
                active : true,
                creationDate : moment(new Date()).utcOffset('-0500'),
                title: title,
                size: size,
            });

            newRecord.save(function (err, newTemp) {
                if (err) {
                    console.log('error ', err)
                    return res.status(400).json({
                        message: 'Error creando registro.',
                        error: err
                    });
                }
                return res.status(200).json(newTemp);
            });

        }else{
            return res.status(400).json({
                message: 'Parámetros incompletos.'
            });
        }
    },

    /**
     * newsController.update()
     */
    update: function (req, res) {
        var id = req.params.id;
        newsModel.findOne({_id: id}, function (err, newRecord) {
            if (err) {
                return res.status(400).json({
                    message: 'Error buscando el registro.',
                    error: err
                });
            }
            if (!newRecord) {
                return res.status(404).json({
                    message: 'No se encontró la noticia.'
                });
            }

            var target = req.body.target ? req.body.target : newRecord.target;
            var content = req.body.content ? req.body.content : newRecord.content;
            var title = req.body.title ? req.body.title : newRecord.title;
            var size = req.body.size    

            newRecord.target = target;
            newRecord.size = size;
            newRecord.content = content;
            newRecord.title = title;
            newRecord.active = req.body.active ? req.body.active : newRecord.active;
            newRecord.published = req.body.published ? req.body.published : newRecord.published;
            newRecord.attachment = req.body.attachment ? req.body.attachment : newRecord.attachment;
            
            newRecord.save(function (err, newRecord) {
                if (err) {
                    return res.status(400).json({
                        message: 'Error when updating newRecord.',
                        error: err
                    });
                }

                return res.json(newRecord);
            });
        });  
    },

    /**
     * newsController.deleteNew(): Permite eliminar lógicamente la noticia
     * Se le cambia su campo active a false
    */
    deleteNew: function (req, res) {
        var id = req.params.id;
        newsModel.findOne({_id: id}, function (err, newRecord) {
            if (err) {
                return res.status(400).json({
                    message: 'Error buscando el registro.',
                    error: err
                });
            }
            if (!newRecord) {
                return res.status(404).json({
                    message: 'No se encontró la noticia.'
                });
            }

            newRecord.active = false;

            newRecord.save(function (err, newRecord) {
                if (err) {
                    return res.status(400).json({
                        message: 'Error when updating newRecord.',
                        error: err
                    });
                }

                return res.json(newRecord);
            });
        });
    },

    /**
     * newsController.deleteNew(): Permite publicar la noticia
     * Se le cambia su campo published a true
    */
    publishNew: function (req, res) {

        var id = req.params.id;
        newsModel.findOne({_id: id}, function (err, newRecord) {
            if (err) {
                return res.status(400).json({
                    message: 'Error buscando el registro.',
                    error: err
                });
            }
            if (!newRecord) {
                return res.status(404).json({
                    message: 'No se encontró la noticia.'
                });
            }

            newRecord.published = true;

            newRecord.save(function (err, newRecord) {
                if (err) {
                    return res.status(400).json({
                        message: 'Error when updating newRecord.',
                        error: err
                    });
                }

                return res.json(newRecord);
            });
        });
    },

    /**
     * newsController.deleteNew(): Permite publocultaricar la noticia
     * Se le cambia su campo published a false
    */
    unpublishNew: function (req, res) {
    var id = req.params.id;
    newsModel.findOne({_id: id}, function (err, newRecord) {
        if (err) {
            return res.status(400).json({
                message: 'Error buscando el registro.',
                error: err
            });
        }
        if (!newRecord) {
            return res.status(404).json({
                message: 'No se encontró la noticia.'
            });
        }

        newRecord.published = false;

        newRecord.save(function (err, newRecord) {
            if (err) {
                return res.status(400).json({
                    message: 'Error when updating newRecord.',
                    error: err
                });
            }

            return res.json(newRecord);
        });
    });
    },

    /**
     * newsController.remove()
     */
    remove: function (req, res) {
        var id = req.params.id;
        newsModel.findByIdAndRemove(id, function (err, newRecord) {
            if (err) {
                return res.status(400).json({
                    message: 'Error when deleting the newRecord.',
                    error: err
                });
            }
            return res.status(204).json();
        });
    },

    //---------News for app

    /**
     * newsController.remove()
     */
    getPublishedNews: function (req, res) {
        var target = req.query.target;
        newsModel.find(
            {
                active: true,
                target: target,
                published: true 
            }, function (err, news) {
            if (err) {
                return res.status(400).json({
                    message: 'Error when getting news.',
                    error: err
                });
            }
            return res.json(news);
        });
    }
};
