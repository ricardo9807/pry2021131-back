var hcpbModel = require('../models/hcpbModel.js');
var userModel = require('../models/userModel.js');
const moment = require('moment');
const _ = require('lodash');
/**
 * customerController.js
 *
 * @description :: Server-side logic for managing customers.
 */
module.exports = {

    listByPatient: async function (req, res) {
        try{
            var idPatient = req.params.id;
            var data = await hcpbModel.find({idPregnant: idPatient}).populate('idPregnant')
            return res.status(200).json(data);
        }catch(error){
            console.log(error)
            return res.status(400).json({
                message: 'Error consultando las HCPBs.',
                error: error
            });
        }
    },

    list: function (req, res) {
        //Estamos usando un pipeline de agregación
       hcpbModel.aggregate([
           //Primera fase: extraer data que queremos
           { '$match': { 'active': true}},
           { "$addFields": { "custom_field": "$obj.obj_field1" } },
           {
           "$lookup": {
                    from: "users",
                    localField: "idPregnant",
                    foreignField: "_id",
                    as: "patient"
                }
            }
       ]).then((hcpbs) => {
           return res.json(hcpbs);
       }).catch((error) => {
           console.log('getHCPBs error: ', error)
           return res.status(400).json({
               message: 'Error consultando las HCPBs.',
               error: error
           });
       })
    },

    create: function (req, res) {
        //Validamos campos requeridos
        if( req.body.ageMenarche
            && req.body.ageFirstSexualExperience && req.body.lastContraceptiveMethod && req.body.lastDateMenstruation
            && req.body.pregnantHeight && req.body.pregnantWeight
            && req.body.gestationalAge && req.body.probableDateBirth &&  req.body.familyHistory 
            && req.body.vaccinationRecord  && req.body.previousPregnancy  &&  req.body.medicalHistory && req.body.idPregnant && req.body.idSpecialist
        ){
            var previous= []
            if(req.body.previousPregnancy.length > 0){
                for (var i = 0; i < req.body.previousPregnancy.length; i++) {
                    var record =  req.body.previousPregnancy[i]
                    var newPrevious = {
                        pregnancyType: record.pregnancyType, 
                        pregnancyTermination: record.pregnancyTermination,  
                        dateOfBirth: moment(record.dateOfBirth).utcOffset('-0500'),          
                        observation: record.observation,        
                        neonateStatus: record.neonateStatus,       
                        placeOfBirth: record.placeOfBirth,  
                    }
                    previous.push(newPrevious)
                }
            }

            var famHistory= []
            if(req.body.familyHistory.length > 0){
                for (var i = 0; i < req.body.familyHistory.length; i++) {
                    var record =  req.body.familyHistory[i]
                    var newHistory = {
                        documentNumber :record.documentNumber,  
                        relation: record.relation,   
                        fullName : record.fullName, 
                        observation :  record.observation,        
                        antecedent :  record.antecedent,
                        motherPatient : record.motherPatient,    
                        fatherPatient : record.fatherPatient,   
                    }
                    famHistory.push(newHistory)
                }
            }

            var antecedent= []
            if(req.body.medicalHistory.length > 0){
                for (var i = 0; i < req.body.medicalHistory.length; i++) {
                    var record =  req.body.medicalHistory[i]
                    var newHistory = {
                        codeDisease : record.id,     
                        description : record.description, 
                        observation : record.observation,
                        diagnosisDate : record.diagnosisDate,  
                    }
                    antecedent.push(newHistory)
                }
            }


            var hcpb = new hcpbModel({
                creationDate : moment(new Date()).utcOffset('-0500'),
                initialDiagnostic : req.body.initialDiagnostic,
                ageMenarche : req.body.ageMenarche,
                ageFirstSexualExperience : req.body.ageFirstSexualExperience,
                lastContraceptiveMethod : req.body.lastContraceptiveMethod,
                lastDateMenstruation : moment(req.body.lastDateMenstruation).utcOffset('-0500'),
                pregnantHeight : req.body.pregnantHeight,
                pregnantWeight : req.body.pregnantWeight,
                probableDateBirth : moment(req.body.probableDateBirth).utcOffset('-0500'),
                idPregnant : req.body.idPregnant,
                idSpecialist: req.body.idSpecialist,
                active : true,
                vaccinationRecord: {
                    rubella : req.body.vaccinationRecord.rubella,
                    hepatitisB: req.body.vaccinationRecord.hepatitisB,
                    papilloma: req.body.vaccinationRecord.papilloma,
                    yellowFever : req.body.vaccinationRecord.yellowFever,
                    ah1n1: req.body.vaccinationRecord.ah1n1,
                    influenza: req.body.vaccinationRecord.influenza,                    
                },
                previousPregnancy: previous,
                medicalHistory: antecedent,
                familyHistory:famHistory,
                gestationalAge: req.body.gestationalAge,
                currentControl:0,
                currentGestationalAge: req.body.gestationalAge,
                //
                previousPretermPregnancy: req.body.previousPretermPregnancy,
                multiplePregnancy: req.body.multiplePregnancy,
                shortUterineNeck: req.body.shortUterineNeck,
                highStressLevel: req.body.highStressLevel,
                //
                status: 'MONITORING',
            });

            hcpb.save(function (err, newHcpb) {
                if (err) {
                    return res.status(400).json({
                        message: 'Error creando registro.',
                        error: err
                    });
                }else{
                    userModel.findOne({_id: req.body.idPregnant}, function (err, user) {
                        user.patientDetail.hcpbActive =true
                        user.patientDetail.idHCPB = newHcpb._id
                        //Guardamos cambios
                        user.save(function (err, user) {}); 
                    })
                    return res.status(200).json(newHcpb);
                }
            });

        }else{
            return res.status(400).json({
                message: 'Parámetros incompletos.'
            });
        }
    },

    update: function (req, res) {
        console.log('entró a update', req.body)
        var id = req.body.id;
        hcpbModel.findOne({_id: id}, function (err, hcpb) {
            if (err) {
                return res.status(400).json({
                    message: 'Error buscando el registro.',
                    error: err
                });
            }
            else if (!hcpb) {
                return res.status(404).json({
                    message: 'No se encontró el cliente.'
                });
            }
            else{
                //Actualizamos los datos del usuario
                req.body.ageMenarche? hcpb.ageMenarche =  req.body.ageMenarche : null
                req.body.ageFirstSexualExperience? hcpb.ageFirstSexualExperience = req.body.ageFirstSexualExperience : null
                req.body.gestationalAge? hcpb.gestationalAge = req.body.gestationalAge : null
                req.body.probableDateBirth? hcpb.probableDateBirth = req.body.probableDateBirth : null    
                req.body.idSpecialist? hcpb.idSpecialist = req.body.idSpecialist : null    
                

                if(req.body.vaccinationRecord){
                    hcpb.vaccinationRecord.rubella = req.body.vaccinationRecord.rubella,
                    hcpb.vaccinationRecord.hepatitisB = req.body.vaccinationRecord.hepatitisB,
                    hcpb.vaccinationRecord.papilloma = req.body.vaccinationRecord.papilloma,
                    hcpb.vaccinationRecord.yellowFever = req.body.vaccinationRecord.yellowFever,
                    hcpb.vaccinationRecord.ah1n1 = req.body.vaccinationRecord.ah1n1,
                    hcpb.vaccinationRecord.influenza = req.body.vaccinationRecord.influenza
                }
                if(req.body.medicalHistory){
                    hcpb.medicalHistory = req.body.medicalHistory
                }

                //Guardamos cambios
                hcpb.save(function (err, hcpb) {
                    if (err) {
                        return res.status(400).json({
                            message: 'Error actualizando hcpb.',
                            error: err,
                            status:400
                        });
                    }else{
                        return res.json({
                            status: 200,
                            hcpb: hcpb,
                        });  
                    }
                   
                });                
            }
            //return res.json(hcpb);
        });
    },

    terminated: function (req, res) {
        console.log('entró a terminated', req.body)
        var _id = req.body._id;
        hcpbModel.findOne({_id:_id}, function (err, hcpb) {
            if (err) {
                return res.status(400).json({
                    message: 'Error buscando el registro.',
                    error: err
                });
            }
            else if (!hcpb) {
                return res.status(404).json({
                    message: 'No se encontró el cliente.'
                });
            }
            else{
                //Actualizamos los datos del usuario
                var terminatedPregnancy={
                    gestationalAge  :  req.body.gestationalAge,  
                    pregnancyType:  req.body.pregnancyType, 
                    pregnancyTermination:  req.body.pregnancyTermination, 
                    dateOfBirth:  moment(req.body.dateOfBirth).utcOffset('-0500'),         
                    observation:  req.body.observation,        
                    neonateStatus:  req.body.neonateStatus,       
                    placeOfBirth:  req.body.placeOfBirth,   
                    pretermBirth:  req.body.pretermBirth, 
                }
                hcpb.terminatedPregnancy = terminatedPregnancy
                hcpb.status = terminatedPregnancy.pretermBirth == '1'? 'TERMINATED_PRETERM_BIRTH' : 'TERMINATED'

                //Guardamos cambios
                hcpb.save(function (err, hcpb) {
                    if (err) {
                        return res.status(400).json({
                            message: 'Error actualizando hcpb.',
                            error: err,
                            status:400
                        });
                    }else{
                        return res.json({
                            status: 200,
                            hcpb: hcpb,
                        });  
                    }
                   
                });                
            }
            //return res.json(hcpb);
        });
    },

    delete: function (req, res) {
        var id = req.params.id;
        hcpbModel.findOne({_id: id}, function (err, newRecord) {
            if (err) {
                return res.status(400).json({
                    message: 'Error buscando el registro.',
                    error: err
                });
            }
            if (!newRecord) {
                return res.status(404).json({
                    message: 'No se encontró la especialidad.'
                });
            }

            newRecord.active = false;

            newRecord.save(function (err, newRecord) {
                if (err) {
                    return res.status(400).json({
                        message: 'Error when updating newRecord.',
                        error: err
                    });
                }else{
                    userModel.findOne({_id: newRecord.idPregnant}, function (err, user) {
                        user.patientDetail.hcpbActive =false
                        //Guardamos cambios
                        user.save(function (err, user) {}); 
                    })
    
                    return res.json(newRecord);
                }

            });
        });
    },

    /**
     * ridersController.show()
     */
    show: function (req, res) {
        console.log('entró a show hcpb ', req.params.id)
        var id = req.params.id;
        hcpbModel.findOne({_id: id}, async function (err, user) {
            if (err) {
                return res.status(400).json({
                    message: 'Error buscando el registro.',
                    error: err
                });
            }
            if (!user) {
                return res.status(404).json({
                    message: 'No se encontró el registro.'
                });
            }else{                
                const patient = await userModel.findOne({_id: user.idPregnant}).exec()
                return res.status(200).json({
                    hcpb: user,
                    patientDetail: patient
                });
            }
            
        });
    },

    /**
     * ridersController.search()
     */
    search: function (req, res) {
        console.log('entró a rider search', req.body)
        var filtersUser ={}
        //obtener los valores de los filtros
        req.body.fullName ? filtersUser['fullname'] = { $regex: '.*' + req.body.fullName.toUpperCase()  + '.*' }  : null;
        req.body.phone ? filtersUser['phone'] =  { $regex: '.*' + req.body.phone + '.*' }  : null;  
        req.body.documentNumber ? filtersUser['documentNumber'] =  { $regex: '.*' + req.body.documentNumber.toUpperCase()  + '.*' }  : null      
        req.body.email ? filtersUser['email'] =  { $regex: '.*' + req.body.email + '.*' } : null   
        filtersUser['active'] = true
        filtersUser['isRider'] = true
        console.log('filtros use', filtersUser )
        hcpbModel.find(
            filtersUser
        , function (err, users) {
            if (err) {
                console.log('error',err  )
                return res.status(400).json({
                    message: 'Error when getting users.',
                    error: err
                });
            }
            else{
                var finalArray =[]
                if(req.body.isRider == true){
                    console.log('is filters ', users.length)
                    //obtener solo riders
                    for (let index = 0; index < users.length; index++) {
                        if(users[index].riderDetail.currentStatus.code == 'ACTIVE' || users[index].riderDetail.currentStatus.code == 'DELIVERING'){
                            finalArray.push(users[index])
                        }                        
                    }
                }
                console.log('is riders',finalArray.length )
                return res.json(users);
            }
            
        });
    },
   
};
