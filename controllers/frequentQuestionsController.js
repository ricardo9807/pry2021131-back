var frequentQuestionsModel = require('../models/frequentQuestionsModel.js');
var moment = require('moment');
/**
 * frequentQuestionsControllers.js
 *
 * @description :: Server-side logic for managing questions.
 */
module.exports = {

    /**
     * frequentQuestionsControllers.list()
     */
    list: function (req, res) {
       
        var filters = {}
        filters['active'] = true        
        console.log('filters ', filters)   
        frequentQuestionsModel.find(
            filters
        , function (err, questions) {
            if (err) {
                console.log('error',err  )
                return res.status(400).json({
                    message: 'Error when getting questions.',
                    error: err
                });
            }
            return res.json(questions);
        });        

    },

    /**
     * frequentQuestionsControllers.filter()
     */
    search: function (req, res) {
        console.log('entró', req.query)
        var filters = {}
        filters['active'] = true
        req.query.target != 'undefined' ? filters['target'] =  req.query.target  : null        
        
        frequentQuestionsModel.find(
            filters
        , function (err, questions) {
            if (err) {
                console.log('error',err  )
                return res.status(400).json({
                    status: 400,
                    message: 'Error when getting questions.',
                    error: err
                });
            }
            return res.json({status:200 , questions:questions});
        });      
    },

    /**
     * frequentQuestionsControllers.show()
     */
    show: function (req, res) {
        var id = req.params.id;
        frequentQuestionsModel.findOne({_id: id}, function (err, newFrequentQuestion) {
            if (err) {
                return res.status(400).json({
                    message: 'Error buscando el registro 10.',
                    error: err
                });
            }
            if (!newFrequentQuestion) {
                return res.status(404).json({
                    message: 'No se encontró la noticia.'
                });
            }
            return res.json(
            {   status:200,
                frequentQuestion :newFrequentQuestion
            });
        });
    },

    /**
     * frequentQuestionsControllers.create()
     * Esta función se usará para crear preguntas frecuentes: 
     * esde la intranet.
     */
    create: function (req, res) {
        //Validamos campos requeridos
        // console.log('create: ', req.body)
        if(req.body.question && req.body.target){

            /**
             * Variables: 
             * target: Contiene el público objetivo de la noticia.
             * question: El contenido de la pregunta.
             * answer: la respuesta para la pregunta.
             */

            var target = req.body.target
            var question = req.body.question
            var answer = req.body.answer ? answer = req.body.answer : null 

            var newQuestion = new frequentQuestionsModel({
                question : question,
                target : target,
                answer : answer,
                active : true,
                creationDate : moment(new Date()).utcOffset('-0500'),
            });

            newQuestion.save(function (err, newTemp) {
                if (err) {
                    console.log('error ', err)
                    return res.status(400).json({
                        status: 400,
                        message: 'Error creando registro.',
                        error: err
                    });
                }
                return res.status(200).json({
                    question: newTemp,
                    status : 200});
            });

        }else{
            return res.status(400).json({
                status: 400,
                message: 'Parámetros incompletos.'
            });
        }
    },

    /**
     * frequentQuestionsControllers.update()
     */
    update: function (req, res) {
        var id = req.params.id;
        frequentQuestionsModel.findOne({_id: id}, function (err, newFrequentQuestion) {
            if (err) {
                return res.status(400).json({
                    status: 400,
                    message: 'Error buscando el registro.',
                    error: err
                });
            }
            if (!newFrequentQuestion) {
                return res.status(404).json({
                    status: 400,
                    message: 'No se encontró el registro.'
                });
            }
            else{

                console.log('body:::::::::::', req.body )
                var target = req.body.target ? req.body.target : newFrequentQuestion.target;
                var answer = req.body.answer ? req.body.answer : newFrequentQuestion.answer;
                var question = req.body.question ? req.body.question : newFrequentQuestion.question;
                var active = req.body.active != null ? req.body.active : newFrequentQuestion.active;

                newFrequentQuestion.target = target;
                newFrequentQuestion.question = question;
                newFrequentQuestion.active = active;
                newFrequentQuestion.answer = answer;
                
                newFrequentQuestion.save(function (err, newFrequentQuestion) {
                    if (err) {
                        return res.status(400).json({
                            status:400,
                            message: 'Error actualizando la pregunta.',
                            error: err
                        });
                    }

                    return res.json({
                        status:200,
                        frequentQuestion: newFrequentQuestion
                    });
                });
            }
        
        });  
    
    },

    /**
    * frequentQuestionsController.publishQuestion(): Permite publicar la pregunta
    * actualiza el campo published del registro
    */
   publishQuestion: function (req, res) {
        var id = req.params.id;
        var publishQuestion = req.body.publishQuestion;
        console.log('params ',req.body.publishQuestion )
        frequentQuestionsModel.findOne({_id: id}, function (err, question) {
            if (err) {
                return res.status(400).json({
                    message: 'Error buscando el registro.',
                    error: err,
                    status: 400
                });
            }
            else if (!question) {
                return res.status(404).json({
                    message: 'No se encontró la noticia.',
                    status:400,
                });
            }
            else{
                question.published = publishQuestion;
                question.save(function (err, newQuestion) {
                    if (err) {
                        return res.status(400).json({
                            message: 'No se pudo actualizar la pregunta.',
                            error: err,
                            status: 400
                        });
                    }else{
                        return res.json({status:200, question:newQuestion});
                    }            
                });
            }
        });
   },

    /**
    * frequentQuestionsController.unpublishQuestion(): Permite cambiar el estado 
    * de publicación 
    * Actualiza el campo published del registro
    */
    // unpublishQuestion: function (req, res) {
    //     var id = req.params.id;
    //     frequentQuestionsModel.findOne({_id: id}, function (err, question) {
    //         if (err) {
    //             return res.status(400).json({
    //                 message: 'Error buscando el registro.',
    //                 error: err,
    //                 status: 400
    //             });
    //         }
    //         else if (!question) {
    //             return res.status(404).json({
    //                 message: 'No se encontró la noticia.',
    //                 status:400,
    //             });
    //         }
    //         else{
    //             question.published = true;
    //             question.save(function (err, newQuestion) {
    //                 if (err) {
    //                     return res.status(400).json({
    //                         message: 'Error when updating question.',
    //                         error: err,
    //                         status: 400
    //                     });
    //                 }else{
    //                     return res.json({status:200, question:newQuestion});
    //                 }            
    //             });
    //         }
    //     });
    // },

};
