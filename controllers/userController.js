var userModel = require('../models/userModel.js');
const moment = require('moment');
const nodemailer = require("nodemailer");
var configurationModel = require('../models/configurationModel.js');
const _ = require('lodash');

/**
 * userController.js
 *
 * @description :: Server-side logic for managing users.
 */
module.exports = {
    validateUser:function (req, res) {
        console.log("validetauser::::::::::::", req.body)
        //Validamos campos requeridos
        if(req.body.email && req.body.colegiatureNumber && req.body.documentNumber ){
            //Primero, consultamos si el correo ya existe
            userModel.findOne({email: req.body.email.toLowerCase()}, function(err, user) {
                console.log("user 1", user)
                if(user){
                    return res.status(200).json({
                        status: 400,
                        message: 'El correo ya está registrado.'
                    });
                }else{
                    //Segundo, consultamos si el documento ya existe
                    userModel.findOne({documentNumber: req.body.documentNumber.toLowerCase()}, function(err, user) {
                    console.log("user 2", user)
                    if(user){
                        return res.status(200).json({
                            status : 400,
                            message: 'El documento ya está registrado.'
                        });
                    }else{
                        if(req.body.colegiatureNumber != 0){

                            userModel.aggregate([
                                { '$match': { 'active': true, 'isSpecialist': true }},
                                { '$project': {"specialistDetail.colegiatureNumber": 1, "specialistDetail.isAdmin": 1, "specialistDetail.specialtiesArray": 1 } 
                                }
                            ]).then((doctors) => {
                               var exists =  false
                               for (let i = 0; i < doctors.length; i++) {
                                console.log('doctor',doctors[i] )
                                console.log('repetido',doctors[i].specialistDetail.colegiatureNumber) 
                                  if(doctors[i].specialistDetail.colegiatureNumber == req.body.colegiatureNumber){
                                    exists = true
                                    console.log('repetido',doctors[i] )
                                  }                                   
                               }
                               if(exists == true){
                                return res.status(200).json({
                                    status : 400,
                                    message: 'Número de colegiatura ya está registrado.'
                                });
                               }else{
                                return res.status(200).json(true);
                               }
                            }).catch((error) => {
                                console.log('getSpecialists error: ', error)
                                return res.status(400).json({
                                    message: 'Error consultando los especialistas.',
                                    error: error
                                });
                            })

                        }else{
                            return res.status(200).json(true);
                        }

                    }                    
                    });
                }                    
            });
        }else{
            return res.status(400).json({
                message: 'Parámetros incompletos.'
            });
        }        
    },
    /**
     * userController.signUp()
     */
    signUp: function (req, res) {
        //Validamos campos requeridos
        if(req.body.name && req.body.lastName && req.body.surName
            && req.body.email && req.body.phone && req.body.password){
        
            //Primero consultamos si el correo ya existe
            userModel.findOne({email: req.body.email.toLowerCase()}, function(err, user) {
                if(user){
                    return res.status(400).json({
                        message: 'El correo ya está registrado.'
                    });
                }else{
                    //Registramos el nuevo usuario
                    /**
                     * Variables: 
                     * name: Contiene el nombre del body
                     * lastName: Contiene los apellidos del body
                     * fullname: Es la concatenación de las otras variables.
                    */

                    var name = req.body.name
                    var lastName = req.body.lastName
                    var surName = req.body.surName
                    var fullname = name + ' ' + lastName + ' ' + surName 
                    //Creamos el modelo principal con datos generales                 
                    var user = new userModel({
                        name : name.toUpperCase(),
                        lastName : lastName.toUpperCase(),
                        surName : surName ? surName.toUpperCase() : '',
                        fullname : fullname.toUpperCase(),
                        email : req.body.email,
                        documentType : 'DNI',
                        documentNumber : req.body.documentNumber.toUpperCase(),
                        active : true,
                        creationDate : moment(new Date()).utcOffset('-0500'),
                        birthdate : moment(req.body.birthdate).utcOffset('-0500'),
                        isSpecialist : true,
                        address: req.body.address,
                        idDistrict: req.body.idDistrict,
                        phone: req.body.phone,
                        status: 1,
                        //Todos los usuarios son clientes
                        specialistDetail: {
                            isAdmin : false,
                            colegiatureNumber: req.body.colegiatureNumber,
                            specialtiesArray: req.body.specialtiesArray,
                        },
                    })
                    user.password = user.generateHash(req.body.password)

                    user.save(function (err, user) {
                        if (err) {
                            console.log(err);
                            return res.status(400).json({
                                message: 'Error registrando usuario.',
                                status: '400',
                                error: err
                            });
                        }else{
                            // //El usuario fue creado, ahora enviaremos el correo
                            // //1. Obtener credenciales del emisor desde la configuración
                            // configurationModel.find(function (err, configuration) {
                            //     if (err) {
                            //         console.log("Error consultando configuración")
                            //     }
                            //     if(configuration){
                            //         // 2. Usando la configuración, ejecutar el envío de correo
                            //         var id = user._id
                            //         const subject = 'Gracias por registrarte en Guille';
                            //         const text = `Hola ${fullname.toUpperCase()}!
                            //         Confirma tu correo electrónico haciendo clic aquí:
                            //         http://localhost:7776/shared-public/confirmEmail/${id}`

                            //         sendTextMail(configuration[0].email, req.body.email, configuration[0].password, subject, text).then((info) => {
                            //             console.log("info correo enviado: ", info)
                            //             // res.status(200).json({
                            //             //     message: 'Se envió el correo para recuperar la contraseña'
                            //             // });
                            //         }).catch((error) => {
                            //             console.log("Hubo un error enviando el correo: ", error)
                            //             // res.status(400).json(error);
                            //         })
                            //     }  
                            // })            
                            //Después de enviar el correo, retornamos el registro exitoso:
                            return res.status(200).json(user);           
                        }
                        
                    });
                }
            });

        }else{
            return res.status(400).json({
                message: 'Parámetros incompletos.'
            });
        }
    },

    /**
     * userController.show()
     */
    show: function (req, res) {
        console.log('entró a user controller -> show')
        userModel.findById(req.params.id,
        //userModel.findOne({_id:id}, 
            function (err, user) {
            if (err) {
                return res.status(400).json({
                    message: 'Error buscando el registro.',
                    error: err
                });
            }
            if (!user) {
                return res.status(404).json({
                    message: 'No se encontró el usuario.'
                });
            }
            return res.json(user);
        });
    },

    /**
     * userController.update()
     */
    update: function (req, res) {
        var id = req.params.id;
        userModel.findOne({_id: id}, function (err, user) {
            if (err) {
                return res.status(400).json({
                    message: 'Error buscando el registro.',
                    error: err
                });
            }
            if (!user) {
                return res.status(404).json({
                    message: 'No se encontró el usuario.'
                });
            }
            else{
                var name = req.body.name ? req.body.name : user.name;
                var lastName = req.body.lastName ? req.body.lastName : user.lastName;
                var secondLastName = req.body.secondLastName ? req.body.secondLastName : user.secondLastName;
                var fullname = name + ' ' + lastName + ' ' + secondLastName
    
                user.name = name.toUpperCase();
                user.lastName = lastName.toUpperCase();
                user.secondLastName = secondLastName ? secondLastName.toUpperCase() : '';
                user.fullname = fullname.toUpperCase();
                user.email = req.body.email ? req.body.email : user.email;
                user.phone = req.body.phone ? req.body.phone : user.phone;
                user.secondPhone = req.body.secondPhone ? req.body.secondPhone : user.secondPhone;
                user.documentType = req.body.documentType ? req.body.documentType : user.documentType;
                user.documentNumber = req.body.documentNumber ? req.body.documentNumber : user.documentNumber;
                user.userPhotoUrl = req.body.userPhotoUrl ? req.body.userPhotoUrl : user.userPhotoUrl;
                user.active = req.body.active ? req.body.active : user.active;
                user.riderDetail = req.body.riderDetail ? req.body.riderDetail : user.riderDetail;
                user.clientDetail = req.body.clientDetail ? req.body.clientDetail : user.clientDetail;
                user.address = req.body.address ? req.body.address : user.address;
                user.ruc = req.body.ruc ? req.body.ruc : user.ruc;
                user.birthdate = req.body.birthdate ? req.body.birthdate : user.birthdate;
                user.isAdmin = req.body.isAdmin ? req.body.isAdmin : user.isAdmin;
                user.isRider = req.body.isRider ? req.body.isRider : user.isRider;
    
                if(user.riderDetail){
                    user.riderDetail.vehicle = req.body.vehicle ? req.body.vehicle : user.riderDetail.vehicle;
                    user.riderDetail.urlFacebook = req.body.urlFacebook ? req.body.urlFacebook : user.riderDetail.urlFacebook;
                    user.riderDetail.urlInstagram = req.body.urlInstagram ? req.body.urlInstagram : user.riderDetail.urlInstagram;
                    user.riderDetail.paymentMethods = req.body.paymentMethods ? req.body.paymentMethods : user.riderDetail.paymentMethods;
                }
    
                user.save(function (err, user) {
                    if (err) {
                        return res.status(400).json({
                            message: 'Error when updating user.',
                            error: err,
                            status: 400
                        });
                    }
                    return res.json({
                        status: 200,
                        user:user
                    });
                });
            }

            
        });
        
    },

    /**
     * userController.remove()
     */
    remove: function (req, res) {
        var id = req.params.id;
        userModel.findByIdAndRemove(id, function (err, user) {
            if (err) {
                return res.status(400).json({
                    message: 'Error when deleting the user.',
                    error: err
                });
            }
            return res.status(204).json();
        });
    },
    
    /**
     *  userController.login
     */
    login: function (req, res) {
        console.log('entró a login', req.body)
        userModel.findOne({email: req.body.email.toLowerCase()}, function(err, user) {
            console.log('entró al find', user)
            //Verificamos que no haya error
            if(err){
                return res.status(400).json({
                    message: 'Error iniciando sesión.',
                    code: 400,
                    error: err
                });
            }
            //Verificamos que exista el usuario
            if(!user){
                return res.status(400).json({
                    message: 'El correo no está registrado.',
                    code: 400,
                });
            }else{
                console.log('existe usuario', user)
                //Validamos la contraseña
                if (!user.validPassword(req.body.password)) {
                    return res.status(400).json({
                            message: 'Contraseña incorrecta.',
                            code: 400,
                        });
                }else {
                   
                        return res.status(200).json(user);
                    
                }
            }
            
        });
    },

    /**
     *  userController.sendEmailResetPassword
     */
    sendEmailResetPassword : function (req, res){
        console.log('entró a sendEmailResetPassword',req.body )
        var email = req.body.email ? email =  req.body.email :  email= null   
        // Validar que enviaron un email
        if(email != null){
            // Obtener email y contraseña
            configurationModel.find(function (err, configuration) {
                if (err) {
                    return res.status(400).json({
                        message: 'Error when getting configuration.',
                        status: 400,
                        error: err
                    });
                }
                if(configuration){
                    // Validar que sea un email registrado
                    userModel.findOne({email: req.body.email.toLowerCase()}, function(err, user) {
                        if(user){
                            console.log('usuario look', user)
                            // Crear token único
                            var token  = Math.random().toString(36).substr(2)
                            // Guardar token en el usuario
                            user.token = token 

                            user.save(function (err, user) {
                                console.log('entró a user.save')
                                if (err) {
                                    console.log(err);
                                    return res.status(400).json({
                                        message: 'Error actualizando registro.',
                                        status: '400',
                                        error: err
                                    });
                                }
                                else{                                
                                    const subject = 'Recuperar tu contraseña  ';
                                    const text = 
                                    `                            
                                    Hola ${user.fullname}!
                                    Hace poco pediste que se restableciera la contraseña de esta cuenta  : ${email}
                                    Para actualizar tu contraseña, haz clic en el siguiente link:
                                    http://localhost:7776/shared/forgotMyPassword/${token}
                                    `
                                    sendTextMail(configuration[0].email, req.body.email, configuration[0].password, subject, text).then((info) => {
                                        console.log("info correo enviado: ", info)
                                        res.status(200).json({
                                            status : 200,
                                        });
                                    }).catch((error) => {
                                        console.log("Hubo un error enviando el correo: ", error)
                                        res.status(400).json({
                                            error:error,                                        
                                            message: 'Se envió el correo para recuperar la contraseña' 
                                        })    
                                    })
                                }                          
                            });              
                        }
                        else
                        {
                            return res.status(400).json({
                                status: 400,
                                message: 'El correo ingresado no está registrado.'
                            });
                        }                    
                    });                
                }           
            }) 
        }
        else{
            return res.status(400).json({
                status: 400,
                message: 'Campos incompletos.'
            });
        }
    },

    /**
     *  userController.changePassword
     */
    changePassword : function (req, res){
        console.log('changePassword:::::::',req.body )
        userModel.findById(req.body._id,
            function (err, user) {
            if (err) {
                return res.status(400).json({
                    message: 'Error actualizando el registro.',
                    error: err
                });
            }
            else if (!user) {
                return res.status(404).json({
                    message: 'No se encontró el usuario.'
                });
            }else if (user){
                if (!user.validPassword(req.body.currentPassword)) {
                    return res.status(400).json({
                            message: 'La contraseña ingresada es incorrecta',
                            code: 400,
                        });
                }else {
                    user.password = user.generateHash(req.body.password)
                    user.save(function (err, user) {
                        if (err) {
                            console.log(err);
                            return res.status(400).json({
                                message: 'Error actualizando registro',
                                status: 400,
                                error: err
                            });
                        }else{
                            return res.status(200).json({
                                message:'Actualizado',
                                status: 200,
                            });                          
                        }                        
                    });                                       
                }
            }
        });       
    },

    /**
     *  userController.confirmEmail
     */
    confirmEmail : function (req, res){
        var _id = req.body.id
        // Paso 1: Buscar usuario por _id
        userModel.findOne({_id: _id}, function (err, user) {
            if (err) {
                console.log("error");
                return res.status(400).json({
                    message: 'Error buscando el usuario.',
                    error: err
                });
            }
            if (!user) {
                console.log("no hay usuario");
                return res.status(400).json({
                    message: 'El usuario no existe',
                    error: err
                });
            } 
            if (user){
                if(user.emailConfirmed == true){
                    return res.status(400).json({
                        message: 'Registro ya confirmado',
                    });
                }else{
                    // Paso 2: Actualizar campo emailConfirmed
                    user.emailConfirmed = true 
                    // Paso 3: Actualizar usuario
                    user.save(function (err, user) {
                        if (err) {
                            console.log(err);
                            return res.status(400).json({
                                message: 'Error actualizando registro.',
                                error: err
                            });
                        }else{
                            return res.status(200).json(user);                          
                        }
                        
                    });
                }
            }
            
        }) 
        
    },   
    /**
     *  addToken -> permite agregar el token del usuario para enviar las notificaciones push
     */
    addToken : function (req, res){
        var userId = null 
        var token = null 
        var origin = null 
        var currentToken = null 

        req.params.id ? userId = req.params.id : null
        req.body.token ? token = req.body.token : null
        req.body.origin ? origin = req.body.origin : null
        req.body.currentToken ? currentToken = req.body.currentToken : null

        if(userId != null || token != null || origin != null){
            console.log('userid', userId)
            console.log('origin', origin)
            console.log('token', token)
            console.log('currentToken', currentToken)
            // Paso 1: Buscar usuario por id
            userModel.findOne({_id: userId}, function (err, user) {
                if (err) {
                    console.log("error");
                    return res.status(400).json({
                        message: 'Error buscando el registro.',
                        error: err
                    });
                }
                if (!user) {
                    console.log("no hay usuario");
                    return res.status(400).json({
                        message: 'No se encontró al usuario'
                    });
                } 
                if (user){
                    console.log("si hay usuario");
                    //Diferenciar el origen de la solicitud
                    //Origen : Client
                    if(origin == 'Client'){
                        //Validar si existe el arreglo user.clientDetail.tokensForNotifications
                        if(user.clientDetail.tokensForNotifications != undefined){
                            //Validar si se va a reemplazar un token actual
                            if( token != undefined && currentToken != undefined){
                                console.log('existe token y current token')
                                //buscamos el token actual y lo eliminamos del array
                                var i = user.clientDetail.tokensForNotifications.indexOf( currentToken ); 
                                if ( i !== -1 ) {
                                    user.clientDetail.tokensForNotifications.splice( i, 1 );
                                }
                            }    
                        }
                        //Si no existe el arreglo crearlo
                        else{
                            user.clientDetail.tokensForNotifications = []
                        }
                        //Agregar el nuevo token
                        user.clientDetail.tokensForNotifications.push(token)  
    
                    }
                    //Origen : Distinto a Client
                    else{
                        if(!user.riderDetail){
                            return res.status(400).json({
                                message: 'Error actualizando registro, no es un rider.',
                                error: err
                            });
                        }else{
                            //Igualar el atributo user.riderDetail.tokensForNotification al token 
                            user.riderDetail.tokenForNotifications = token
                        }
                    }
    
                    // Paso 4: Actualizar usuario
                    user.save(function (err, user) {
                        if (err) {
                            console.log(err);
                            return res.status(400).json({
                                message: 'Error actualizando registro.',
                                status: '400',
                                error: err
                            });
                        }else{
                            return res.status(200).json(user);                          
                        }
                        
                    });
                }
                
            }) 
        }else{
            console.log("Parámetros incompletos");
            return res.status(400).json({
                message: 'userId, token , origin no puedes llevar valores indefinidos o nulos',
            });            
        }
        
    },
    /**
     *  deleteToken -> permite eliminar el token de un usuario
     */
    deleteToken : function (req, res){
        var userId = null 
        var token = null 
        var origin = null 

        req.params.id ? userId = req.params.id : null
        req.body.token ? token = req.body.token : null
        req.body.origin ? origin = req.body.origin : null

        if(userId != null || token != null || origin != null){
        // Paso 1: Buscar usuario por id
        userModel.findOne({_id: userId}, function (err, user) {
            if (err) {
                console.log("error");
                return res.status(400).json({
                    message: 'Error buscando el registro.',
                    error: err
                });
            }
            if (!user) {
                console.log("no hay usuario");
                return res.status(400).json({
                    message: 'No existe el usuario'
                });
            } 
            if (user){
                //Diferenciar el origen de la solicitud
                //Origen : Client
                if(origin == 'Client'){
                    //Enviado desde la App cliente
                    console.log('inicio arreglo ', user.clientDetail.tokensForNotifications)
                    if(user.clientDetail.tokensForNotifications != undefined){    
                        console.log("currentToken: ", token)
                        if(token != undefined ) {
                            //buscamos el token actual y lo eliminamos del array
                            var newArray =  user.clientDetail.tokensForNotifications
                            for( var i = 0; i <newArray.length; i++){     
                                if ( newArray[i] === token) {                             
                                    newArray.splice(i, 1); 
                                }                            
                            }                            
                            //Creamos una variable independiente con todo el contenido de clientDetail
                            var newClientDetail = user.clientDetail
                            newClientDetail.tokensForNotifications = newArray 
                            //Chancamos el atributo clientDetail del json
                            user.clientDetail = newClientDetail

                        }else{
                            return res.status(400).json({
                                message: 'Campos incompletos, ingrese token que desea eliminar.',
                                error: err
                            });                
                        }
                    }
                    console.log('final arreglo ', user.clientDetail.tokensForNotifications)
                }
                //Origen : Distinto a Client
                else{
                    if(!user.riderDetail){
                        return res.status(400).json({
                            message: 'Error actualizando registro, no es un rider.',
                            error: err
                        });
                    }else{
                        user.riderDetail.tokenForNotifications = null
                    }
                    
                }

                // Paso 4: Actualizar usuario
                user.save(function (err, user) {
                    if (err) {
                        console.log(err);
                        return res.status(400).json({
                            message: 'Error actualizando registro.',
                            error: err
                        });
                    }else{
                        return res.status(200).json({message: 'Token eliminado', user: user});                          
                    }
                    
                });
            }
            
        }) 
        } else{
            console.log("Parámetros incompletos");
            return res.status(400).json({
                message: 'userId, token , origin no puedes llevar valores indefinidos o nulos',
            });            
        }
        
    },
    /**
     * sendMail: Función para probar envío de correos
    */
    sendMailTest : async function (req, res){
        const subject = 'Gracias por registrarte en Byclo';
        const text = `Hola!\nPrueba de correo desde al backend!`
        sendTextMail(req.body.fromEmail, req.body.toEmail, req.body.password, subject, text).then((info) => {
            console.log("info correo enviado: ", info)
            res.status(200).json({
                message: 'Se envió el correo para recuperar la contraseña'
            });
        }).catch((error) => {
            res.status(400).json(error);
        })
    }
};

/**
 * sendTextMail: Método reutilizable para enviar correos
 * fromEmail: Correo de origen
 * toEmail: Correo destino
 * password: Contraseña de correo de origen
 * subject: Asunto
 * text: Contenido del correo
*/
const sendTextMail = async (fromEmail, toEmail, password, subject, text) => {
    console.log('sendTextMail')
    var userMail = fromEmail.split('@')
    var transporter = nodemailer.createTransport({
        host: 'smtp.gmail.com',
        port: 465,
        secure: true, 
        auth: {
            user: userMail[0], 
            pass: password, 
        }
    });

    // Configurar email
    var mailOptions = {
        from: fromEmail,
        to: toEmail, 
        subject: subject,
        text: text
    };  

    //Enviar correo al usuario
    console.log('enviando correo')
    const result = await transporter.sendMail(mailOptions).then((info) => {
        // console.log("Todo bien: ", info)
        console.log("Correo Enviado.")
        return info
    }).catch((error) => {
        console.log("Error: ", error)
        return error
    })
    return result;
}
