var prenatalControlModel = require('../models/prenatalControlModel.js');
var hcpbModel = require('../models/hcpbModel.js');
var mongoose = require('mongoose');
const moment = require('moment');

/**
 * customerController.js
 *
 * @description :: Server-side logic for managing customers.
 */
module.exports = {


    list: function (req, res) {
       //Estamos usando un pipeline de agregación
       var idHcpb = req.params.idHcpb;
       console.log('idHcpb', idHcpb)
       prenatalControlModel.aggregate([
           //Primera fase: extraer data que queremos
           { '$match': { 'active': true, 'idHCPB':  mongoose.Types.ObjectId(idHcpb)}},
           { "$addFields": { "custom_field": "$obj.obj_field1" } },
           {
           "$lookup": {
                    from: "users",
                    localField: "idSpecialist",
                    foreignField: "_id",
                    as: "specialist"
                }
            }
       ]).then((prenatalControls) => {
           return res.json(prenatalControls);
       }).catch((error) => {
           console.log('getprenatalControls error: ', error)
           return res.status(400).json({
               message: 'Error consultando los controles prenatales.',
               error: error
           });
       })
    },

    create: function (req, res) {
        //Validamos campos requeridos
        if( 
            req.body.dateAttention && req.body.actualWeight && req.body.weightGain && req.body.imc
            && req.body.temperature && req.body.systolicPressure &&  req.body.pulse
            && req.body.uterineHeight 
            && req.body.diastolicPressure  &&  req.body.breathingFrequency 
            && req.body.systemDiagnosis  && req.body.finalDiagnosis  &&  req.body.nextControlDate 
            && req.body.numberControl  && req.body.idHCPB  &&  req.body.idSpecialist 
            && req.body.recommendations && req.body.fetusesArray
        ){
            var fetuses= []
            if(req.body.fetusesArray.length > 0){
                for (var i = 0; i < req.body.fetusesArray.length; i++) {
                    var record =  req.body.fetusesArray[i]
                    var newHistory = {
                        fcf :record.fcf,  
                        situationFetus: record.situationFetus,   
                        presentationFetus : record.presentationFetus, 
                        observation :  record.observation,        
                        positionFetus :  record.positionFetus,
                    }
                    fetuses.push(newHistory)
                }
            }

            var prenatalControl = new prenatalControlModel({
                dateAttention : moment(req.body.dateAttention).utcOffset('-0500'),
                actualWeight : req.body.actualWeight,
                weightGain : req.body.weightGain,
                pulse: req.body.pulse,
                imc : req.body.imc,
                nextControlDate : moment(req.body.nextControlDate).utcOffset('-0500'),
                temperature : req.body.temperature,
                systolicPressure : req.body.systolicPressure,
                diastolicPressure : req.body.diastolicPressure,                
                uterineHeight : req.body.uterineHeight,
                breathingFrequency : req.body.breathingFrequency,
                systemDiagnosis : req.body.systemDiagnosis,
                finalDiagnosis : req.body.finalDiagnosis,
                numberControl : req.body.numberControl,
                active : true,
                fetusesDetail: fetuses,
                recommendations:req.body.recommendations,
                gestationalAge: req.body.gestationalAge,
                nameSpecialist: req.body.nameSpecialist,
                idSpecialist:req.body.idSpecialist,
                idHCPB: req.body.idHCPB
            });

            prenatalControl.save(function (err, newprenatalControl) {
                if (err) {
                    return res.status(400).json({
                        message: 'Error creando registro.',
                        error: err
                    });
                }else{
                    hcpbModel.findOne({_id: req.body.idHCPB}, function (err, hcpb) {
                        hcpb.currentControl =req.body.numberControl
                        hcpb.currentGestationalAge = req.body.gestationalAge
                        hcpb.nextControlDate =moment(req.body.nextControlDate).utcOffset('-0500')
                        hcpb.save(function (err, user) {}); 
                    })
                    return res.status(200).json(newprenatalControl);
                }
                
            });

        }else{
            return res.status(400).json({
                message: 'Parámetros incompletos.'
            });
        }
    },

    update: function (req, res) {
        console.log('entró a update', req.body)
        var id = req.body.id;
        prenatalControlModel.findOne({_id: id}, function (err, prenatalControl) {
            if (err) {
                return res.status(400).json({
                    message: 'Error buscando el registro.',
                    error: err
                });
            }
            else if (!prenatalControl) {
                return res.status(404).json({
                    message: 'No se encontró el cliente.'
                });
            }
            else{
                //Actualizamos los datos del usuario
                req.body.ageMenarche? prenatalControl.ageMenarche =  req.body.ageMenarche : null
                req.body.ageFirstSexualExperience? prenatalControl.ageFirstSexualExperience = req.body.ageFirstSexualExperience : null
                req.body.gestationalAge? prenatalControl.gestationalAge = req.body.gestationalAge : null
                req.body.probableDateBirth? prenatalControl.probableDateBirth = req.body.probableDateBirth : null    
                

                if(req.body.vaccinationRecord){
                    prenatalControl.vaccinationRecord.rubella = req.body.vaccinationRecord.rubella,
                    prenatalControl.vaccinationRecord.hepatitisB = req.body.vaccinationRecord.hepatitisB,
                    prenatalControl.vaccinationRecord.papilloma = req.body.vaccinationRecord.papilloma,
                    prenatalControl.vaccinationRecord.yellowFever = req.body.vaccinationRecord.yellowFever,
                    prenatalControl.vaccinationRecord.ah1n1 = req.body.vaccinationRecord.ah1n1,
                    prenatalControl.vaccinationRecord.influenza = req.body.vaccinationRecord.influenza
                }


                //Guardamos cambios
                prenatalControl.save(function (err, prenatalControl) {
                    if (err) {
                        return res.status(400).json({
                            message: 'Error actualizando usuario.',
                            error: err,
                            status:400
                        });
                    }else{
                        return res.json({
                            status: 200,
                            prenatalControl: prenatalControl,
                        });  
                    }
                   
                });                
            }
            //return res.json(prenatalControl);
        });
    },

    delete: function (req, res) {
        var id = req.params.id;
        prenatalControlModel.findOne({_id: id}, function (err, newRecord) {
            if (err) {
                return res.status(400).json({
                    message: 'Error buscando el registro.',
                    error: err
                });
            }
            if (!newRecord) {
                return res.status(404).json({
                    message: 'No se encontró la especialidad.'
                });
            }

            newRecord.active = false;

            newRecord.save(function (err, newRecord) {
                if (err) {
                    return res.status(400).json({
                        message: 'Error when updating newRecord.',
                        error: err
                    });
                }

                return res.json(newRecord);
            });
        });
    },

    show: function (req, res) {
        console.log('entró a show prenatalControl ', req.params.id)
        var id = req.params.id;
        prenatalControlModel.findOne({_id: id}, function (err, user) {
            if (err) {
                return res.status(400).json({
                    message: 'Error buscando el registro.',
                    error: err
                });
            }
            if (!user) {
                return res.status(404).json({                    
                    message: 'No se encontró el registro.'
                });
            }
            return res.json(user);
        });
    },


};
