var userModel = require('../models/userModel.js');
var prenatalControlModel = require('../models/prenatalControlModel.js');
var hcpbModel = require('../models/hcpbModel.js');
const moment = require('moment');


/**
 * customerController.js
 *
 * @description :: Server-side logic for managing customers.
 */
module.exports = {


    list: function (req, res) {
        console.log('entró a list patients')
        //Estamos usando un pipeline de agregación
       userModel.aggregate([
           //Primera fase: extraer data que queremos
           { '$match': { 'active': true, 'isPatient':true }},
           { "$addFields": { "custom_field": "$obj.obj_field1" } },
        //    //Segunda fase: proyectar campos deseados
        //    { '$project': {name: 1, lastName: 1,surName:1, secondLastName: 1, fullname: 1, documentType: 1, documentNumber: 1, phone: 1, email:1, address:1,
        //       active:1, idDistrict:1,birthdate:1,
        //       "patientDetail.hcpbActive": 1, "patientDetail.status": 1, "patientDetail.affiliationDate": 1 } 
        //    }
       ]).then((services) => {
           return res.json(services);
       }).catch((error) => {
           console.log('getSpecialists error: ', error)
           return res.status(400).json({
               message: 'Error consultando los especialistas.',
               error: error
           });
       })
    },

    getValues:async function (req, res) {
       console.log('entró a show getValues ', req.params.id)
       var id = req.params.id;
       console.log('entró a list patients')
       var patient = await userModel.findOne({_id:req.params.id})
       var hcpb = await hcpbModel.findOne({_id:  patient.patientDetail.idHCPB})
       var doctor = await userModel.findOne({_id:hcpb.idSpecialist})
       var prenatalControl =await  prenatalControlModel.find({idHCPB: hcpb._id}).sort({ _id: -1 }).limit(1)
       var data={}
       var specialist = {
           name: doctor.name + ' '+doctor.lastName +' '+doctor.surName ,
           documentNumber: doctor.documentNumber,
           colegiatureNumber: doctor.specialistDetail.colegiatureNumber,
           phone: doctor.phone,
       }
       data.specialist = specialist
       var now = moment()
       data.age =  now.diff(patient.birthdate, 'years')
       data.currentGestationalAge =  hcpb.currentGestationalAge       
       data.nextControl = prenatalControl[0].nextControlDate
       data.classification = prenatalControl[0].systemDiagnosis
       data.actualWeight = prenatalControl[0].actualWeight
       data.multiplePregnancy = hcpb.multiplePregnancy == 1 ? 'Si': 'No'
       data.previousPretermPregnancy = hcpb.previousPretermPregnancy == 1 ? 'Si': 'No'
       data.pregnantHeight = hcpb.pregnantHeight
       //
       data.name = patient.name +' '+ patient.lastName+' '+patient.surName
       data.email = patient.email
       data.phone = patient.phone
       data.documentNumber = patient.documentNumber
       
       return res.json(data);
    }, 

    create: function (req, res) {
        //Validamos campos requeridos
        if( req.body.name && req.body.lastName  && req.body.surName
            && req.body.documentNumber && req.body.birthdate && req.body.idDistrict
            && req.body.address     
        ){
            
            var name = req.body.name
            var lastName = req.body.lastName
            var surName = req.body.surName ? req.body.surName : ''
            var fullname = name + ' ' + lastName + ' ' + surName

            var user = new userModel({
                name : name.toUpperCase(),
                lastName : lastName.toUpperCase(),
                surName : surName ? surName.toUpperCase() : '',
                fullname : fullname.toUpperCase(),
                email : req.body.email,
                documentType : 'DNI',
                documentNumber : req.body.documentNumber.toUpperCase(),
                active : true,
                creationDate : moment(new Date()).utcOffset('-0500'),
                birthdate : moment(req.body.birthdate).utcOffset('-0500'),
                isSpecialist : false,
                isPatient: true,
                address: req.body.address,
                idDistrict: req.body.idDistrict,
                phone: req.body.phone,
                patientDetail: {
                    status: 'NEW_PATIENT',
                    affiliationDate : moment(new Date()).utcOffset('-0500'),
                    hcpbActive: false,
                }
            });
            user.password = user.generateHash('2021nuevo')
            user.save(function (err, user) {
                if (err) {
                    return res.status(400).json({
                        message: 'Error creando registro.',
                        error: err
                    });
                }
                return res.status(200).json(user);
            });

        }else{
            return res.status(400).json({
                message: 'Parámetros incompletos.'
            });
        }
    },

    update: function (req, res) {
        console.log('entró a update', req.body)
        var id = req.body.id;
        userModel.findOne({_id: id}, function (err, user) {
            if (err) {
                return res.status(400).json({
                    message: 'Error buscando el registro.',
                    error: err
                });
            }
            else if (!user) {
                return res.status(404).json({
                    message: 'No se encontró el cliente.'
                });
            }
            else{
                //Actualizamos los datos del usuario
                req.body.name? user.name =  req.body.name.toUpperCase() : null
                req.body.lastName? user.lastName = req.body.lastName.toUpperCase() : null
                req.body.surName? user.surName = req.body.surName.toUpperCase() : null
                req.body.documentNumber? user.documentNumber = req.body.documentNumber : null
                //user.fullname = user.lastName+' '+user.surName+' '+user.lastName
                req.body.birthdate ?  user.birthdate =  req.body.birthdate : null 
                req.body.idDistrict ?  user.idDistrict =  req.body.idDistrict : null 
                req.body.address? user.address = req.body.address : null                
                req.body.email? user.email = req.body.email : null    
                req.body.phone? user.phone = req.body.phone : null              
                

                if(user.patientDetailSchema){
                    user.patientDetailSchema.status = req.body.statusPatient ? req.body.statusPatient : user.patientDetailSchema.status;
                }
                user.password = user.generateHash('2021nuevo')

                //Guardamos cambios
                user.save(function (err, user) {
                    if (err) {
                        return res.status(400).json({
                            message: 'Error actualizando usuario.',
                            error: err,
                            status:400
                        });
                    }else{
                        return res.json({
                            status: 200,
                            user: user,
                        });  
                    }
                   
                });                
            }
            //return res.json(user);
        });
    },

    delete: function (req, res) {
        var id = req.params.id;
        userModel.findOne({_id: id}, function (err, newRecord) {
            if (err) {
                return res.status(400).json({
                    message: 'Error buscando el registro.',
                    error: err
                });
            }
            if (!newRecord) {
                return res.status(404).json({
                    message: 'No se encontró la paciente con el id ingresado.'
                });
            }

            newRecord.active = false;

            newRecord.save(function (err, newRecord) {
                if (err) {
                    return res.status(400).json({
                        message: 'Error when updating newRecord.',
                        error: err
                    });
                }

                return res.json(newRecord);
            });
        });
    },


    /**
     * ridersController.show()
     */
    show: function (req, res) {
        console.log('entró a show rider ', req.params.id)
        var id = req.params.id;
        userModel.findOne({_id: id}, function (err, user) {
            if (err) {
                return res.status(400).json({
                    message: 'Error buscando el registro.',
                    error: err
                });
            }
            if (!user) {
                return res.status(404).json({
                    message: 'No se encontró el rider.'
                });
            }
            return res.json(user);
        });
    },

    /**
     * ridersController.search()
     */
    search: function (req, res) {
        console.log('entró a rider search', req.body)
        var filtersUser ={}
        //obtener los valores de los filtros
        req.body.fullName ? filtersUser['fullname'] = { $regex: '.*' + req.body.fullName.toUpperCase()  + '.*' }  : null;
        req.body.phone ? filtersUser['phone'] =  { $regex: '.*' + req.body.phone + '.*' }  : null;  
        req.body.documentNumber ? filtersUser['documentNumber'] =  { $regex: '.*' + req.body.documentNumber.toUpperCase()  + '.*' }  : null      
        req.body.email ? filtersUser['email'] =  { $regex: '.*' + req.body.email + '.*' } : null   
        filtersUser['active'] = true
        filtersUser['isRider'] = true
        console.log('filtros use', filtersUser )
        userModel.find(
            filtersUser
        , function (err, users) {
            if (err) {
                console.log('error',err  )
                return res.status(400).json({
                    message: 'Error when getting users.',
                    error: err
                });
            }
            else{
                var finalArray =[]
                if(req.body.isRider == true){
                    console.log('is filters ', users.length)
                    //obtener solo riders
                    for (let index = 0; index < users.length; index++) {
                        if(users[index].riderDetail.currentStatus.code == 'ACTIVE' || users[index].riderDetail.currentStatus.code == 'DELIVERING'){
                            finalArray.push(users[index])
                        }                        
                    }
                }
                console.log('is riders',finalArray.length )
                return res.json(users);
            }
            
        });
    },
   
};
