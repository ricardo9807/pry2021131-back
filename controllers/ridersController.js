var userModel = require('../models/userModel.js');
const moment = require('moment');

/**
 * customerController.js
 *
 * @description :: Server-side logic for managing customers.
 */
module.exports = {

    /**
     * getActiveRiders: Consultamos los riders activos 
     * para poblar el mapa del Monitoreo
     */
    getActiveRiders: function (req, res) {
        console.log('llegó a getActiveRiders')
        //Estamos usando un pipeline de agregación
        userModel.aggregate([
            //Primera fase: extraer data que queremos
            { '$match': { 'riderDetail.currentStatus.code': { '$in': ['ACTIVE','DELIVERING']} }},
            //Segunda fase: proyectar campos deseados
            { '$project': {name: 1, lastName: 1, secondLastName: 1, fullname: 1, documentType: 1, documentNumber: 1, phone: 1, userPhotoUrl: 1, email:1,
            //    riderDetail: 1}}
            "riderDetail.location.coordinates": 1, "riderDetail.idCurrentService": 1, "riderDetail.currentStatus": 1 , "riderDetail.recruitmentStatus": 1} }
        ]).then((services) => {
            return res.json(services);
        }).catch((error) => {
            console.log('getActiveRiders error: ', error)
            return res.status(400).json({
                message: 'Error consultando los riders.',
                error: error
            });
        })
    },

    /**
     * getAllRiders: Consultamos los riders  
     * para poblar maestro de riders
     */
    getAllRiders: function (req, res) {
        //Estamos usando un pipeline de agregación
       userModel.aggregate([
           //Primera fase: extraer data que queremos
           { '$match': { 'isRider': true }},
           //Segunda fase: proyectar campos deseados
           { '$project': {name: 1, lastName: 1, secondLastName: 1, fullname: 1, documentType: 1, documentNumber: 1, phone: 1, userPhotoUrl: 1, email:1, "riderDetail.location.coordinates": 1, "riderDetail.idCurrentService": 1, 
           "riderDetail.recruitmentStatus": 1, "riderDetail.currentStatus": 1, "riderDetail.uploadedDocs": 1 , "riderDetail.paymentMethods": 1 } }
       ]).then((services) => {
           return res.json(services);
       }).catch((error) => {
           console.log('getAllRiders error: ', error)
           return res.status(400).json({
               message: 'Error consultando los riders.',
               error: error
           });
       })
    },

    /**
     * ridersController.update()
     */
    updateDocumentation: function (req, res) {
        var id = req.params.id;
        userModel.findOne({_id: id}, function (err, user) {
            if (err) {
                return res.status(400).json({
                    message: 'Error buscando el registro.',
                    error: err
                });
            }
            if (!user) {
                return res.status(404).json({
                    message: 'No se encontró el usuario.'
                });
            }
            else{
                
                console.log("documentos",user.riderDetail.uploadedDocs )
                var uploadedDocs = user.riderDetail.uploadedDocs
                if(req.body.formDocumentation.document){
                    if(uploadedDocs.length > 0){
                        var index1 = -1
                        for (let index = 0; index < uploadedDocs.length; index++) {
                            if(uploadedDocs[index].type == 'document'){
                                index1 = index
                            }                     
                        }
                        console.log('index doc', index1)
                        index1 != -1 ? user.riderDetail.uploadedDocs[index1] = req.body.formDocumentation.document :  user.riderDetail.uploadedDocs.push(req.body.formDocumentation.document)
                    }else{
                        user.riderDetail.uploadedDocs.push(req.body.formDocumentation.document)
                    }
                }
                if(req.body.formDocumentation.invoice){
                    if(uploadedDocs.length > 0){
                        var index2 = -1
                        for (let index = 0; index < uploadedDocs.length; index++) {
                            if(uploadedDocs[index].type == 'invoice'){
                                index2 = index
                            }                     
                        }
                        console.log('index inv', index2)
                        index2 != -1 ? user.riderDetail.uploadedDocs[index2] = req.body.formDocumentation.invoice :  user.riderDetail.uploadedDocs.push(req.body.formDocumentation.invoice)
                    }else{
                        user.riderDetail.uploadedDocs.push(req.body.formDocumentation.invoice)
                    }
                }
                if(req.body.formDocumentation.policeCertificate){
                    if(uploadedDocs.length > 0){
                        var index3 = -1
                        for (let index = 0; index < uploadedDocs.length; index++) {
                            if(uploadedDocs[index].type == 'policeCertificate'){
                                index3 = index
                            }                    
                        }
                        index3 != -1 ? user.riderDetail.uploadedDocs[index3] = req.body.formDocumentation.policeCertificate :  user.riderDetail.uploadedDocs.push(req.body.formDocumentation.policeCertificate)
                    }else{
                        user.riderDetail.uploadedDocs.push(req.body.formDocumentation.policeCertificate)
                    }
                }     
                if(req.body.formDocumentation.criminalCertificate){
                    if(uploadedDocs.length > 0){
                        var index4 = -1
                        for (let index = 0; index < uploadedDocs.length; index++) {
                            if(uploadedDocs[index].type == 'criminalCertificate'){
                                index4 = index
                            }                      
                        }
                        index4 != -1 ? user.riderDetail.uploadedDocs[index4] = req.body.formDocumentation.criminalCertificate :  user.riderDetail.uploadedDocs.push(req.body.formDocumentation.criminalCertificate)
                    }else{
                        user.riderDetail.uploadedDocs.push(req.body.formDocumentation.criminalCertificate)
                    }
                }
                if(req.body.formDocumentation.enrollment){
                    console.log('entró a enroll')
                    if(uploadedDocs.length > 0){
                        var index5 =-1
                        for (let index = 0; index < uploadedDocs.length; index++) {
                            if(uploadedDocs[index].type == 'enrollment'){
                                index5 = index
                            }                     
                        }
                        index5 != -1 ? user.riderDetail.uploadedDocs[index5] = req.body.formDocumentation.enrollment :  user.riderDetail.uploadedDocs.push(req.body.formDocumentation.enrollment)
                    }else{
                        user.riderDetail.uploadedDocs.push(req.body.formDocumentation.enrollment)
                    }
                }
                if(req.body.formDocumentation.vehicle){
                    if(uploadedDocs.length > 0){
                        var index6 = -1
                        for (let index = 0; index < uploadedDocs.length; index++) {
                            if(uploadedDocs[index].type == 'vehicle'){
                                index6 = index
                            }                     
                        }
                        index6 != -1 ? user.riderDetail.uploadedDocs[index6] = req.body.formDocumentation.vehicle :  user.riderDetail.uploadedDocs.push(req.body.formDocumentation.vehicle)
                    }else{
                        user.riderDetail.uploadedDocs.push(req.body.formDocumentation.vehicle)
                    }
                }       
                                   
                // req.body.formDocumentation.document ? user.riderDetail.recruitment.documentation.document = req.body.formDocumentation.document : null;
                // req.body.formDocumentation.invoice ? user.riderDetail.recruitment.documentation.invoice = req.body.formDocumentation.invoice : null;
                // req.body.formDocumentation.policeCertificate ? user.riderDetail.recruitment.documentation.policeCertificate = req.body.formDocumentation.policeCertificate : null;
                // req.body.formDocumentation.criminalCertificate ? user.riderDetail.recruitment.documentation.criminalCertificate = req.body.formDocumentation.criminalCertificate : null;
                // req.body.formDocumentation.enrollment ? user.riderDetail.recruitment.documentation.enrollment = req.body.formDocumentation.enrollment : null;
                // req.body.formDocumentation.vehicle ? user.riderDetail.recruitment.documentation.vehicle = req.body.formDocumentation.vehicle : null;

                user.save(function (err, user) {
                    if (err) {
                        return res.status(400).json({
                            message: 'Error when updating user.',
                            status:400,
                            error: err
                        });
                    }else{
                        return res.json({
                            status: 200,
                            user:user
                        });
                    }                    
                });
            }
        });
        
    },

    /**
     * setupRider: En esta función, se declara al usuario como rider sin pasar por proceso de reclu
     */
    setupRider: function (req, res) {
        /**
         * Elementos:
         * id -> El id del usuario
         * riderNewStatus -> Es el primer estado que tendrá el rider
         * newRiderDetail -> Es el detalle del rider, tendrá permiso aprobado sin documentos 
         * ni estados de reclutamiento previos
        */
        var id = req.params.id;
        const riderNewStatus = {
            creationDate : moment(new Date()).utcOffset('-0500'),
            code: 'INACTIVE',
            description: 'Inactivo'
        }
        const newRiderDetail = {
            totalCredit: 0,
            totalServices: 0,
            lastServices: [],
            recruitmentStatus: [{
                creationDate : moment(new Date()).utcOffset('-0500'),
                code: 'PERMISSION_APPROVED',
                description: 'Rider aprobado'
            }],
            currentStatus: riderNewStatus,
            uploadedDocs: []
        }
        //Consultamos el registro del usuairo
        userModel.findOne({_id: id}, function (err, user) {
            //Validación de error
            if (err) {
                return res.status(400).json({
                    message: 'Error buscando el registro.',
                    error: err,
                    status: 400
                });
            }
            //Validación de no encontrado
            else if (!user) {
                return res.status(404).json({
                    message: 'No se encontró el rider.',
                    status: 400
                });
            }
            //validamos is el registro tiene el campo user.riderDetail
            else {                
                if(user.riderDetail){

                    //Actualizamos el último estado de reclutamiento 
                    var newRecruitmentStatus ={
                        creationDate : moment(new Date()).utcOffset('-0500'),
                        code: 'PERMISSION_APPROVED',
                        description: 'Rider aprobado'
                    }
                    user.riderDetail.recruitmentStatus.push(newRecruitmentStatus)
                    //Actualizar rider
                    user.save(function (err, user) {
                        if (err) {
                            return res.status(400).json({
                                message: 'Error creando rider.',
                                error: err
                            });
                        }else{
                            return res.json({status: 200, rider: user});
                        }                
                    });

                }else{
                    //Colocamos el detalle de rider
                    user.riderDetail = newRiderDetail;
                    //Guardamos cambios
                    user.save(function (err, user) {
                        if (err) {
                            return res.status(400).json({
                                message: 'Error creando rider.',
                                error: err
                            });
                        }else{
                            return res.json({status: 200, rider: user});
                        }                
                    });
                }
            }

        });
        
    },

    /**
     * setActive: En esta función, se cambia el estado del rider a ACTIVE
     */
    setActive: function (req, res) {
        /**
         * Elementos:
         * id -> El id del usuario
         * riderNewStatus -> Es el primer estado que tendrá el rider
         * 
        */
        var id = req.params.id;
        const riderNewStatus = {
            creationDate : moment(new Date()).utcOffset('-0500'),
            code: 'ACTIVE',
            description: 'Activo'
        }

        //Consultamos el registro del rider
        userModel.findOne({_id: id}, function (err, user) {
            //Validación de error
            if (err) {
                return res.status(400).json({
                    message: 'Error buscando el rider.',
                    error: err
                });
            }
            //Validación de no encontrado
            if (!user) {
                return res.status(404).json({
                    message: 'No se encontró el rider.'
                });
            }
            //Colocamos el detalle de rider
            var newRiderDetail = user.riderDetail
            newRiderDetail.currentStatus = riderNewStatus
            user.riderDetail = newRiderDetail;
            //Guardamos cambios
            user.save(function (err, user) {
                if (err) {
                    return res.status(400).json({
                        message: 'Error actualizando rider.',
                        error: err
                    });
                }
                return res.json(user);
            });
        });
        
    },

    /**
     * setAsetInactivective: En esta función, se cambia el estado del rider a INACTIVE
     */
    setInactive: function (req, res) {
        /**
         * Elementos:
         * id -> El id del usuario
         * riderNewStatus -> Es el primer estado que tendrá el rider
         * 
        */
        var id = req.params.id;
        const riderNewStatus = {
            creationDate : moment(new Date()).utcOffset('-0500'),
            code: 'INACTIVE',
            description: 'Inactivo'
        }

        //Consultamos el registro del rider
        userModel.findOne({_id: id}, function (err, user) {
            //Validación de error
            if (err) {
                return res.status(400).json({
                    message: 'Error buscando el rider.',
                    error: err
                });
            }
            //Validación de no encontrado
            if (!user) {
                return res.status(404).json({
                    message: 'No se encontró el rider.'
                });
            }
            //Colocamos el detalle de rider
            var newRiderDetail = user.riderDetail
            newRiderDetail.currentStatus = riderNewStatus
            user.riderDetail = newRiderDetail;
            //Guardamos cambios
            user.save(function (err, user) {
                if (err) {
                    return res.status(400).json({
                        message: 'Error actualizando rider.',
                        error: err
                    });
                }
                return res.json(user);
            });
        });
        
    },

    /**
     * updateLocation: En esta función, se cambia la localización del rider.
     */
    updateLocation: function (req, res) {
        /**
         * Elementos:
         * id -> El id del usuario
         * newLocation -> Es el primer estado que tendrá el rider
         * 
        */
        var id = req.params.id;
        const latitude = req.body.latitude;
        const longitude = req.body.longitude;

        const newLocation = {
            type: "Point",
            coordinates: [latitude,longitude]
        }

        //Consultamos el registro del rider
        userModel.findOne({_id: id}, function (err, user) {
            //Validación de error
            if (err) {
                return res.status(400).json({
                    message: 'Error buscando el rider.',
                    error: err
                });
            }
            //Validación de no encontrado
            if (!user) {
                return res.status(404).json({
                    message: 'No se encontró el rider.'
                });
            }
            //Colocamos el detalle de rider
            var newRiderDetail = user.riderDetail
            newRiderDetail.location = newLocation
            user.riderDetail = newRiderDetail;
            //Guardamos cambios
            user.save(function (err, user) {
                if (err) {
                    return res.status(400).json({
                        message: 'Error actualizando rider.',
                        error: err
                    });
                }
                return res.json(user);
            });
        });
    },

    /**
     * ridersController.show()
     */
    show: function (req, res) {
        console.log('entró a show rider ', req.params.id)
        var id = req.params.id;
        userModel.findOne({_id: id}, function (err, user) {
            if (err) {
                return res.status(400).json({
                    message: 'Error buscando el registro.',
                    error: err
                });
            }
            if (!user) {
                return res.status(404).json({
                    message: 'No se encontró el rider.'
                });
            }
            return res.json(user);
        });
    },

    /**
     * ridersController.search()
     */
    search: function (req, res) {
        console.log('entró a rider search', req.body)
        var filtersUser ={}
        //obtener los valores de los filtros
        req.body.fullName ? filtersUser['fullname'] = { $regex: '.*' + req.body.fullName.toUpperCase()  + '.*' }  : null;
        req.body.phone ? filtersUser['phone'] =  { $regex: '.*' + req.body.phone + '.*' }  : null;  
        req.body.documentNumber ? filtersUser['documentNumber'] =  { $regex: '.*' + req.body.documentNumber.toUpperCase()  + '.*' }  : null      
        req.body.email ? filtersUser['email'] =  { $regex: '.*' + req.body.email + '.*' } : null   
        filtersUser['active'] = true
        filtersUser['isRider'] = true
        console.log('filtros use', filtersUser )
        userModel.find(
            filtersUser
        , function (err, users) {
            if (err) {
                console.log('error',err  )
                return res.status(400).json({
                    message: 'Error when getting users.',
                    error: err
                });
            }
            else{
                var finalArray =[]
                if(req.body.isRider == true){
                    console.log('is filters ', users.length)
                    //obtener solo riders
                    for (let index = 0; index < users.length; index++) {
                        if(users[index].riderDetail.currentStatus.code == 'ACTIVE' || users[index].riderDetail.currentStatus.code == 'DELIVERING'){
                            finalArray.push(users[index])
                        }                        
                    }
                }
                console.log('is riders',finalArray.length )
                return res.json(users);
            }
            
        });
    },

    /**
     * ridersController.setEnabled()
     */
    setEnabled: function (req, res) {
        console.log('entró a rider search',req.params.id)
        var id = req.params.id;
        userModel.findOne({_id: id}
        , function (err, user) {
            if (err) {
                console.log('error',err  )
                return res.status(400).json({
                    message: 'Error buscando registro.',
                    error: err
                });
            }
            else{
                //Actualizamos el último estado de reclutamiento 
                var newRecruitmentStatus ={
                    creationDate : moment(new Date()).utcOffset('-0500'),
                    code: 'PERMISSION_APPROVED',
                    description: 'Rider aprobado'
                }
                user.riderDetail.recruitmentStatus.push(newRecruitmentStatus)
                //Actualizar rider
                user.save(function (err, user) {
                    if (err) {
                        return res.status(400).json({
                            message: 'Error actualizando rider.',
                            error: err
                        });
                    }else{
                        return res.json(user);
                    }                
                });
            }
            
        });
    },

    /**
     * ridersController.setdisabled()
     */
    setdisabled: function (req, res) {
        console.log('entró a rider search',req.params.id)
        var id = req.params.id;
        userModel.findOne({_id: id}
        , function (err, user) {
            if (err) {
                console.log('error',err  )
                return res.status(400).json({
                    message: 'Error buscando registro.',
                    error: err
                });
            }
            else{
                //Actualizamos el último estado de reclutamiento 
                var newRecruitmentStatus ={
                    creationDate : moment(new Date()).utcOffset('-0500'),
                    code: 'DISABLED',
                    description: 'Rider inhabilitado'
                }
                user.riderDetail.recruitmentStatus.push(newRecruitmentStatus)
                //Actualizar rider
                user.save(function (err, user) {
                    if (err) {
                        return res.status(400).json({
                            message: 'Error actualizando rider.',
                            error: err
                        });
                    }else{
                        return res.json(user);
                    }                
                });
            }
            
        });
    },
};
