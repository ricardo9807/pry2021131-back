var addressModel = require('../models/childrens/addressModel');
var userModel = require('../models/userModel');
const moment = require('moment');
const _ = require('lodash');

/**
 * addressController.js
 *
 * @description :: Server-side logic for managing address of users.
 */
module.exports = {

    /**
     * addressController.create()
     * Esta función se usará para crear la dirección de un usuario
     * desde la App Cliente.
     */
    create: function (req, res) {
        var id = req.params.id;
        if(req.body.address){
            
            /**
             * Variables: 
             * addressLine1: Contiene la dirección y el distrito
             * interior: Contiene el detalle de la dirección como el piso, nro de departamento
             * point: Contiene la latitud y longitud de la dirección
             * address: Contiene todos los datos que se agregarán a la lista de direcciones del usuario
             */
            
            var point = {
                type: 'Point',
                coordinates: [req.body.address.points.lat, req.body.address.points.lng]
            }

            var address = {
                addressLine1: req.body.address.addressLine1,
                interior: req.body.address.interior ,
                location: point
            }
            console.log("Seteo Address");
            userModel.findOne({_id: id}, function (err, user) {
                if (err) {
                    console.log("no hay usuario");
                    return res.status(400).json({
                        message: 'Error buscando el registro.',
                        error: err
                    });
                }
                if (!user) {
                    console.log("no hay usuario");
                    return res.status(404).json({
                        message: 'No se encontró el usuario.'
                    });
                }
                /**
                 * Variables: 
                 * auxAddresses: Contiene el arreglo actual de direcciones
                 */
            
                auxAddresses = _.cloneDeep(user.clientDetail.addresses)
                auxAddresses.push(address)
                user.clientDetail.addresses = auxAddresses;

                user.save(function (err, user) {
                    if (err) {
                        return res.status(400).json({
                            message: 'Error creando registro.',
                            error: err
                        });
                    }
                    return res.status(200).json(user);
                });
            
            }) 

        }else{
            return res.status(400).json({
                message: 'Parámetros incompletos.'
            });
        }
    },

    update: function (req, res) {
        var id = req.params.id;
        if(req.body.addresses){
            userModel.findOne({_id: id}, function (err, user) {
                if (err) {
                    return res.status(400).json({
                        message: 'Error buscando el registro.',
                        error: err
                    });
                }
                if (!user) {
                    return res.status(404).json({
                        message: 'No se encontró el usuario.'
                    });
                }

                user.clientDetail.addresses = req.body.addresses

                user.save(function (err, user) {
                    if (err) {
                        return res.status(400).json({
                            message: 'Error creando registro.',
                            error: err
                        });
                    }
                    console.log(user.clientDetail.addresses);
                    return res.status(200).json(user);
                });
            
            })
        }
    },

};