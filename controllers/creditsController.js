var creditsModel = require('../models/creditsModel.js');
var userModel = require('../models/userModel.js');
var moment = require('moment');
/**
 * creditsController.js
 *
 * @description :: Server-side logic for managing creditss.
 */
module.exports = {

    /**
     * creditsController.list()
     */
    list: function (req, res) { 
        creditsModel.find({active: true}, function (err, credits) {
            if (err) {
                return res.status(400).json({
                    message: 'Error when getting credits 1.',
                    error: err
                });
            }
            return res.json(credits);
        });
    },

    /**
     * creditsController.search()
     */
    // search: function (req, res) {
    //     console.log('entró', req.body)
    //     var filters = {}
    //     filters['active'] = true
    //     req.query.fullName != 'undefined' ? filters['riderName'] = { $regex: '.*' + req.query.fullName + '.*' } :null
    //     req.query.phone != 'undefined' ? filters['riderPhone'] =  { $regex: '.*' + req.query.phone + '.*' } :null     
    //     req.query.documentNumber != 'undefined' ? filters['riderDocumentNumber'] = { $regex: '.*' + req.query.documentNumber + '.*' } :null
    //     req.query.currentStatus != 'undefined' ? filters['currentStatus.code'] = req.query.currentStatus  : null        
    //     console.log('filtro credito ', filters)
    //     creditsModel.find(
    //         filters 
    //     , function (err, credits) {
    //         if (err) {
    //             console.log('error',err  )
    //             return res.status(400).json({
    //                 message: 'Error when getting credits.',
    //                 error: err
    //             });
    //         }
    //         return res.json(credits);
    //     });
    // },

    search: function (req, res) {
        console.log('entró', req.body)
        var filters = {}
        filters['active'] = true
        req.body.fullName ? filters['riderName'] = { $regex: '.*' + req.body.fullName.toUpperCase() + '.*' } :null
        req.body.phone ? filters['riderPhone'] =  { $regex: '.*' + req.body.phone + '.*' } :null     
        req.body.documentNumber  ? filters['riderDocumentNumber'] = { $regex: '.*' + req.body.documentNumber.toUpperCase() + '.*' } :null
        req.body.currentStatus ? filters['currentStatus.code'] = req.body.currentStatus  : null      
        req.body.idRider ? filters['idRider'] = req.body.idRider  : null      
        console.log('filtro credito ', filters)
        creditsModel.find(
            filters 
        , function (err, credits) {
            if (err) {
                console.log('error',err  )
                return res.status(400).json({
                    message: 'Error when getting credits.',
                    error: err
                });
            }
            return res.json(credits);
        });
    },

    /**
     * creditsController.listCreditsByRider()
     */
    listCreditsByRider: function (req, res) {
        creditsModel.find({ idRider: req.params.id, active: true} , function (err, credits) {

            if (err) {
                console.log('error',err  )
                return res.status(400).json({
                    message: 'Error when getting credits.',
                    error: err
                });
            }
            else{
                let creditsApproved = credits// []
                // credits.map((item ) => {
                //     if(item.currentStatus.code == 'APPROVED'){
                //         creditsApproved.push(item)
                //     }
                // })    
                return res.json(creditsApproved);
            }
        });
    },

    /**
     * creditsController.searchOp()
     */
    searchOp: function (req, res) {
        console.log('entró 0000', req.body )
        console.log('entró 0000', req.body[0].name )
        //console.log('entró', req.body )
        /**
         * modelo de lo que va a recibir
         * req.body.filters:[
         * {
         *   name: "title", 
         *   value: "tit",
         *   operator: "include"
         * },
         * {
         *   name: "title", 
         *   value: "tit",
         *   operator: "F"
         * },
         * {
         *   name: "title", 
         *   value: "tit",
         *   operator: "include"
         * },
         * ]
         * 
         * 
         */
        var filtersArray = req.body    
        var filters = {}
        filters['active'] = true
        //recorrer el arreglo para armar el filtro
        for (let index = 0; index < filtersArray.length; index++) {
            var nameObj=filtersArray[index].name
            if(filtersArray[index].operator == 'F'){
                filters[nameObj] = filtersArray[index].value
            }else{
                console.log('inputComponent ',filtersArray[index] )
                if(filtersArray[index].type == 'numeric'){
                    var operator = filtersArray[index].operator
                    switch (operator) {
                        case '=':
                          filters[nameObj] = filtersArray[index].value;;
                          break;
                        case '>':
                          filters[nameObj] = { $gt:  filtersArray[index].value  };
                          break;
                        case '<':
                          filters[nameObj] = { $lt:  filtersArray[index].value  };
                          break;
                        case '>=':
                          filters[nameObj] = { $gte:  filtersArray[index].value  };
                          break;
                        case '<=':
                          filters[nameObj] = { $lte:  filtersArray[index].value  };
                          break;
                        default:
                          filters[nameObj] = filtersArray[index].value;;
                      }
                }
                if(filtersArray[index].type == 'string'){
                    var operator = filtersArray[index].operator
                    switch (operator) {
                        case 'include':
                          filters[filtersArray[index].name] = { $regex: '.*' + filtersArray[index].value + '.*' };
                          break;
                        case 'equals':
                          filters[filtersArray[index].name] = filtersArray[index].value;
                          break;
                        case '%like':
                          filters[filtersArray[index].name] = { $regex: '.*' + filtersArray[index].value  };
                          break;
                        case 'like%':
                          filters[filtersArray[index].name] = { $regex: filtersArray[index].value + '.*' };
                          break;
                        case 'null':
                          filters[filtersArray[index].name] = null;
                          break;
                        default:
                          filters[filtersArray[index].name] = filtersArray[index].value;
                      }
                }
            }
            
            
        }

    
        console.log('filtro credito ', filters)
        creditsModel.find(
            filters
        , function (err, news) {
            if (err) {
                console.log('error',err  )
                return res.status(400).json({
                    message: 'Error when getting news.',
                    error: err
                });
            }
            return res.json(news);
        });
    },

    /**
     * creditsController.show()
     */
    show: function (req, res) {
        var id = req.params.id;
        creditsModel.findOne({_id: id}, function (err, credits) {
            if (err) {
                return res.status(400).json({
                    message: 'Error when getting credits 2.',
                    error: err
                });
            }
            if (!credits) {
                return res.status(404).json({
                    message: 'No such credits'
                });
            }
            return res.json(credits);
        });
    },

    /**
     * creditsController.create(): Este método crea un nuevo crédito asociado a un rider
     * Si se genera de la App de rider, se recibe el idRider
     * Si se genera de la web, se recibe el número de documento
     */
    create: function (req, res) {
        /**
         * Variables:
         * idRider: identificador del rider en la BD
         * documentNumber: Número de documento del rider
         * amount: Monto del crédito
         * document: Comprobante físico del depósito del rider a Byclo
        */
        if(req.body.idRider || req.body.documentNumber && req.body.amount){
            //Consultamos el rider
            var filters = {}
            req.body.idRider ? filters._id = req.body.idRider : null
            req.body.documentNumber ? filters.documentNumber = req.body.documentNumber : null
            
            userModel.findOne(filters, function (err, rider) {
                console.log('encontramos rider: ', rider)
                if (err) { //Si ocurre un error
                    return res.status(400).json({
                        message: 'Error consultando al rider',
                        error: err
                    });
                }else if(!rider){ //Si no se encuentra el registro
                    return res.status(400).json({
                        message: 'No se encontró al rider especificado',
                        error: err
                    });
                }else if(!rider.riderDetail){  //Si el rider no está habilitado
                    return res.status(400).json({
                        message: 'El rider no se encuentra habilitado',
                        error: err
                    });
                }else{ //Se encontró al rider
                    const newStatus = {
                        creationDate : moment(new Date()).utcOffset('-0500'),
                        code: 'PENDING_APPROVEMENT',
                        description: 'Pendiente de Aprobación'
                    }
        
                    var credits = new creditsModel({
                        idRider : rider._id,
                        riderName : rider.fullname,
                        riderDocumentNumber : rider.documentNumber,
                        riderPhone : rider.phone,
                        statusHistory : newStatus,
                        currentStatus : newStatus,
                        document : req.body.document,
                        amount : req.body.amount,
                        creationDate : moment(new Date()).utcOffset('-0500'),
                        active : true,
                    });
                    req.body.createdBy ? credits.createdBy = req.body.createdBy : null                    
                    credits.save(function (err, credits) {
                        if (err) {
                            return res.status(400).json({
                                message: 'Error when creating credits',
                                error: err
                            });
                        }
                        return res.status(200).json(credits);
                    });
                }
            })
        }else{
            return res.status(400).json({
                message: 'Parámetros incompletos.'
            });
        }
    },

    /**
     * creditsController.update()
     */
    update: function (req, res) {
        var id = req.params.id;
        creditsModel.findOne({_id: id}, function (err, credits) {
            if (err) {
                return res.status(400).json({
                    message: 'Error when getting credits 3',
                    error: err
                });
            }
            if (!credits) {
                return res.status(404).json({
                    message: 'No such credits'
                });
            }

            credits.idRider = req.body.idRider ? req.body.idRider : credits.idRider;
			credits.document = req.body.document ? req.body.document : credits.document;
			
            credits.save(function (err, credits) {
                if (err) {
                    return res.status(400).json({
                        message: 'Error when updating credits.',
                        error: err
                    });
                }

                return res.json(credits);
            });
        });
    },
    /**
     * createCreditByAdmin.create(): Este método crea un nuevo crédito asociado a un rider
     * creado por el administrador
     */
    createCreditByAdmin: function (req, res) {
        /**
         * Variables:
         * idRider: identificador del rider en la BD
         * documentNumber: Número de documento del rider
         * amount: Monto del crédito
         * document: Comprobante físico del depósito del rider a Byclo
        */
        if(req.body.idRider || req.body.documentNumber && req.body.amount){
            //Consultamos el rider
            var filters = {}
            req.body.idRider ? filters._id = req.body.idRider : null
            req.body.documentNumber ? filters.documentNumber = req.body.documentNumber : null
            
            userModel.findOne(filters, function (err, rider) {
                console.log('encontramos rider: ', rider)
                if (err) { //Si ocurre un error
                    return res.status(400).json({
                        message: 'Error consultando al rider',
                        error: err
                    });
                }else if(!rider){ //Si no se encuentra el registro
                    return res.status(400).json({
                        message: 'No se encontró al rider especificado',
                        error: err
                    });
                }else if(!rider.riderDetail){  //Si el rider no está habilitado
                    return res.status(400).json({
                        message: 'El rider no se encuentra habilitado',
                        error: err
                    });
                }else{ //Se encontró al rider
                    const newStatus = {
                        creationDate : moment(new Date()).utcOffset('-0500'),
                        code: 'APPROVED',
                        description: 'Saldo Aprobado'
                    }
        
                    var credits = new creditsModel({
                        idRider : rider._id,
                        riderName : rider.fullname,
                        riderDocumentNumber : rider.documentNumber,
                        riderPhone : rider.phone,
                        statusHistory : newStatus,
                        currentStatus : newStatus,
                        document : req.body.document,
                        amount : req.body.amount,
                        creationDate : moment(new Date()).utcOffset('-0500'),
                        active : true,
                    });
                    req.body.createdBy ? credits.createdBy = req.body.createdBy : null                    
                    credits.save(function (err, credits) {
                        if (err) {
                            return res.status(400).json({
                                message: 'Error when creating credits',
                                error: err
                            });
                        }else{
                            //actualizamos al rider
                            userModel.findOne({_id: credits.idRider}).then((rider) => {
                                //Variable temporal para pruebas, en la realidad el riderDetail no será nulo en este punto
                                var newRiderDetail = rider.riderDetail ? rider.riderDetail : {}
                                //Actualizamos el crédito del rider
                                if(newRiderDetail.totalCredit ){
                                    newRiderDetail.totalCredit = newRiderDetail.totalCredit + credits.amount;
                                }else{
                                    newRiderDetail.totalCredit = credits.amount;
                                }                       
                                rider.save().then((updatedRider) => {
                                    return res.status(200).json(credits);
                                }).catch((error) => {
                                    console.log('Error actualizando saldo del rider: ', error)
                                    return res.status(400).json({
                                        message: 'Error actualizando saldo del rider.',
                                        error: error
                                    });
                                })
                            }).catch((error) => {
                                console.log('Error consultando el rider: ', error)
                                return res.status(400).json({
                                    message: 'Error actualizando saldo del rider.',
                                    error: error
                                });
                            })
                        }                        
                    });
                }
            })
        }else{
            return res.status(400).json({
                message: 'Parámetros incompletos.'
            });
        }
    },
    /**
     * creditsController.approveCredit: En este método, se valida que 
     * la recarga es válida y se procede a brindar el crédito al rider
     */
    approveCredit: function (req, res) {
        /**
         * Variables:
         * id: Identificador de la recarga
        */
        var id = req.params.id;

        const newStatus = {
            creationDate : moment(new Date()).utcOffset('-0500'),
            code: 'APPROVED',
            description: 'Saldo Aprobado'
        }

        creditsModel.findOne({_id: id}, function (err, credits) {
            if (err) {
                return res.status(400).json({
                    message: 'Error consultando recarga.',
                    error: err
                });
            }
            if (!credits) {
                return res.status(404).json({
                    message: 'No se encontró la recarga.'
                });
            }
            credits.currentStatus = newStatus;
            credits.statusHistory.push(newStatus);

            credits.save(function (err, credits) {
                if (err) {
                    return res.status(400).json({
                        message: 'Error when updating credits.',
                        error: err
                    });
                }else{
                    //actualizamos al rider
                    userModel.findOne({_id: credits.idRider}).then((rider) => {
                        //Variable temporal para pruebas, en la relaidad el riderDetail no será nulo en este punto
                        var newRiderDetail = rider.riderDetail ? rider.riderDetail : {}
                        //Aumentamos el crédito del rider
                        if(newRiderDetail.totalCredit ){
                            newRiderDetail.totalCredit = newRiderDetail.totalCredit + credits.amount;
                        }else{
                            newRiderDetail.totalCredit = credits.amount;
                        }                       
                        rider.save().then((updatedRider) => {
                            return res.json(credits);
                        }).catch((error) => {
                            console.log('Error actualizando saldo del rider: ', error)
                            return res.status(400).json({
                                message: 'Error actualizando saldo del rider.',
                                error: error
                            });
                        })
                    }).catch((error) => {
                        console.log('Error consultando el rider: ', error)
                        return res.status(400).json({
                            message: 'Error actualizando saldo del rider.',
                            error: error
                        });
                    })
                }
                // return res.json(credits);
            });
        });
    },

    /**
     * creditsController.rejectCredit: En este método, se rechaza 
     * la recarga 
     */
    rejectCredit: function (req, res) {
        /**
         * Variables:
         * id: Identificador de la recarga
        */
        var id = req.params.id;

        const newStatus = {
            creationDate : moment(new Date()).utcOffset('-0500'),
            code: 'REJECTED',
            description: 'Saldo Rechazado'
        }

        creditsModel.findOne({_id: id}, function (err, credits) {
            if (err) {
                return res.status(400).json({
                    message: 'Error consultando recarga.',
                    error: err
                });
            }
            if (!credits) {
                return res.status(404).json({
                    message: 'No se encontró la recarga.'
                });
            }
            credits.currentStatus = newStatus;
            credits.statusHistory.push(newStatus);

            credits.save(function (err, credits) {
                if (err) {
                    return res.status(400).json({
                        message: 'Error when updating credits.',
                        error: err
                    });
                }else{
                    return res.json(credits);
                }
            });
        });
    },

    /**
     * newsController.deleteCredit(): Permite eliminar lógicamente la noticia
     * Se le cambia su campo active a false
    */
    deleteCredit: function (req, res) {
    var id = req.params.id;
    creditsModel.findOne({_id: id}, function (err, credit) {
        if (err) {
            return res.status(400).json({
                message: 'Error buscando el registro.',
                error: err
            });
        }
        if (!credit) {
            return res.status(404).json({
                message: 'No se encontró la recarga.'
            });
        }

        credit.active = false;

        credit.save(function (err, credit) {
            if (err) {
                return res.status(400).json({
                    message: 'Error when updating credit.',
                    error: err
                });
            }
            return res.json(credit);
        });
    });
    },

    /**
     * creditsController.remove()
     */
    remove: function (req, res) {
        var id = req.params.id;
        creditsModel.findByIdAndRemove(id, function (err, credits) {
            if (err) {
                return res.status(400).json({
                    message: 'Error when deleting the credits.',
                    error: err
                });
            }
            return res.status(204).json();
        });
    }
};
