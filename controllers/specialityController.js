var specialityModel = require('../models/specialityModel.js');
var moment = require('moment');
/**
 * specialityController.js
 *
 * @description :: Server-side logic for managing speciality.
 */
module.exports = {

    /**
     * specialityController.list()
     */
    list: function (req, res) {
        specialityModel.find({active: true}, function (err, speciality) {
            if (err) {
                return res.status(400).json({
                    message: 'Error when getting speciality.',
                    error: err
                });
            }
            return res.json(speciality);
        });
    },

    /**
     * specialityController.show()
     */
    show: function (req, res) {
        var id = req.params.id;
        specialityModel.findOne({_id: id}, function (err, newRecord) {
            if (err) {
                return res.status(400).json({
                    message: 'Error buscando el registro..',
                    error: err
                });
            }
            if (!newRecord) {
                return res.status(404).json({
                    message: 'No se encontró la noticia.'
                });
            }
            return res.json(newRecord);
        });
    },

    /**
     * specialityController.create()
     * Esta función se usará para crear noticias: 
     * esde la intranet.
     */
    create: function (req, res) {
        //Validamos campos requeridos
        // console.log('create: ', req.body)
        if(req.body.name && req.body.description && req.body.idClinic){

            var name = req.body.name
            var description = req.body.description
            var idClinic = req.body.idClinic

            var newRecord = new specialityModel({
                idClinic : idClinic,
                active : true,
                creationDate : moment(new Date()).utcOffset('-0500'),
                name: name,
                description: description,
            });

            newRecord.save(function (err, newTemp) {
                if (err) {
                    console.log('error ', err)
                    return res.status(400).json({
                        message: 'Error creando registro.',
                        error: err
                    });
                }
                return res.status(200).json(newTemp);
            });

        }else{
            return res.status(400).json({
                message: 'Parámetros incompletos.'
            });
        }
    },

    /**
     * specialityController.update()
     */
    update: function (req, res) {
        var id = req.params.id;
        specialityModel.findOne({_id: id}, function (err, newRecord) {
            if (err) {
                return res.status(400).json({
                    message: 'Error buscando el registro.',
                    error: err
                });
            }
            if (!newRecord) {
                return res.status(404).json({
                    message: 'No se encontró la noticia.'
                });
            }

            var name = req.body.name ? req.body.name : newRecord.name;
            var description = req.body.description ? req.body.description : newRecord.description;


            newRecord.name = name;
            newRecord.description = description;


            newRecord.save(function (err, newRecord) {
                if (err) {
                    return res.status(400).json({
                        message: 'Error when updating newRecord.',
                        error: err
                    });
                }

                return res.json(newRecord);
            });
        });  
    },

    /**
     * specialityController.deleteNew(): Permite eliminar lógicamente la noticia
     * Se le cambia su campo active a false
    */
    delete: function (req, res) {
        var id = req.params.id;
        specialityModel.findOne({_id: id}, function (err, newRecord) {
            if (err) {
                return res.status(400).json({
                    message: 'Error buscando el registro.',
                    error: err
                });
            }
            if (!newRecord) {
                return res.status(404).json({
                    message: 'No se encontró la especialidad.'
                });
            }

            newRecord.active = false;

            newRecord.save(function (err, newRecord) {
                if (err) {
                    return res.status(400).json({
                        message: 'Error when updating newRecord.',
                        error: err
                    });
                }

                return res.json(newRecord);
            });
        });
    },


};
