var userModel = require('../models/userModel.js');
var servicesModel = require('../models/servicesModel.js');
const moment = require('moment');
var mongoose = require('mongoose');

/**
 * clientsController.js
 *
 * @description :: Server-side logic for managing customers.
 */
module.exports = {

    /**
     * getClients: Se ejecuta desde el maestro de clientes, 
     * Filtra según los parámetros incluidos.
     */
    getClients: function (req, res) {
        userModel.aggregate([
            { '$match': { 'active': true } },
            { '$project': { fullname: 1, email: 1, documentType: 1, documentNumber: 1, phone: 1, userPhotoUrl: 1, name: 1, lastName: 1, clientDetail:1, secondLastName: 1, address: 1}}
        ]).then((services) => {
            return res.json(services);
        }).catch((error) => {
            console.log('getClients error: ', error)
            return res.status(400).json({
                message: 'Error consultando los clientes.',
                error: error
            });
        })
    },

    /**
     * getActiveServices: Consultaremos los servicios activos
     * para poblar el mapa del monitoreo
    */
    getActiveServices: function (req, res) {
       let idClient = req.params.id
        //Estamos usando un pipeline de agregación
        servicesModel.aggregate([ 
            //Primera fase: extraer data que queremos
            { '$match': { 'idClient': mongoose.Types.ObjectId(idClient), 'currentStatus.code': { '$in': ['NEW','PICKINGUP','DELIVERING','ARRIVED' ]} }},
            //Segunda fase: proyectar campos deseados
            { '$project': {path: 1, idRider: 1, riderName: 1, riderPhone: 1, notes:1, packageDescription:1, messageForRider:1, "currentStatus.code": 1, "currentStatus.description": 1, "fromAddress": 1, "toAddress": 1} }
        ]).then((services) => {
            return res.json(services);
        }).catch((error) => {
            console.log('getActiveServices error: ', error)
            return res.status(400).json({
                message: 'Error consultando los envíos.',
                error: error
            });
        })
    },

    /**
     * getClient: Se ejecuta desde el maestro de clientes, 
     * Filtra el id del cliente
     */
    getClient: function (req, res) {
        var idClient= req.params.id
        console.log('entró a get', req.params)
        userModel.aggregate([
            { '$match': { 'active': true, '_id':idClient } },
            { '$project': { fullname: 1, email: 1, documentType: 1, documentNumber: 1, phone: 1, userPhotoUrl: 1, name: 1, lastName: 1, password:1, secondLastName: 1}}
        ]).then((services) => {
            return res.json(services);
        }).catch((error) => {
            console.log('getClients error: ', error)
            return res.status(400).json({
                message: 'Error consultando los clientes.',
                error: error
            });
        })
    },
    /**
     * clientsController.create()
     * Esta función se usará para crear usuarios tipo cliente: 
     * desde la App Clientey desde la intranet.
     */
    create: function (req, res) {
        //Validamos campos requeridos
        if(req.body.name && req.body.lastName && req.body.email && req.body.documentNumber){
            /**
             * Variables: 
             * name: Contiene el nombre del body
             * lastName: Contiene el apellido paterno del body
             * secondLastName: Contiene el apellido materno del body
             * fullname: Es la concatenación de las otras variables.
             */

            var name = req.body.name
            var lastName = req.body.lastName
            var secondLastName = req.body.secondLastName ? req.body.secondLastName : ''
            var fullname = name + ' ' + lastName + ' ' + secondLastName

            var user = new userModel({
                name : name.toUpperCase(),
                lastName : lastName.toUpperCase(),
                secondLastName : secondLastName ? secondLastName.toUpperCase() : '',
                fullname : fullname.toUpperCase(),
                email : req.body.email,
                password : req.body.password,
                phone : req.body.phone,
                documentType : req.body.documentType,
                documentNumber : req.body.documentNumber.toUpperCase(),
                userPhotoUrl : req.body.userPhotoUrl,
                active : true,
                creationDate : moment(new Date()).utcOffset('-0500'),
                isAdmin : false,
                source: req.body.source,
                clientDetail: {
                    totalServices: 0,
                    lastServices: [],
                    addresses: req.body.addresses ? req.body.addresses : []
                }
            });

            user.save(function (err, user) {
                if (err) {
                    return res.status(400).json({
                        message: 'Error creando registro.',
                        error: err
                    });
                }
                return res.status(200).json(user);
            });

        }else{
            return res.status(400).json({
                message: 'Parámetros incompletos.'
            });
        }
    },

    /**
     * clientsController.updateProfilePhoto()
     */
    updateProfilePhoto: function (req, res) {
        var id = req.params.id;
        userModel.findOne({_id: id}, function (err, user) {
            if (err) {
                return res.status(400).json({
                    message: 'Error buscando el registro.',
                    error: err
                });
            }
            else if (!user) {
                return res.status(404).json({
                    message: 'No se encontró el cliente.'
                });
            }
            else{
                //Actualizamos la url                
                user.userPhotoUrl = req.body.imageUrl;
                //Guardamos cambios
                user.save(function (err, user) {
                    if (err) {
                        return res.status(400).json({
                            message: 'Error actualizando usuario.',
                            error: err,
                            status:400
                        });
                    }else{
                        return res.json({
                            status: 200,
                            user: user,
                            message: 'Se actualizó la foto de perfil correctamente'
                        });  
                    }
                   
                });                
            }
            return res.json(user);
        });
    },

    /**
     * clientsController.update()
     * Actualiza los campos básicos del cliente
     */
    update: function (req, res) {
        console.log('entró a update', req.body)
        var id = req.body.id;
        userModel.findOne({_id: id}, function (err, user) {
            if (err) {
                return res.status(400).json({
                    message: 'Error buscando el registro.',
                    error: err
                });
            }
            else if (!user) {
                return res.status(404).json({
                    message: 'No se encontró el cliente.'
                });
            }
            else{
                //Actualizamos los datos del usuario
                req.body.name? user.name =  req.body.name : null
                req.body.lastName? user.lastName = req.body.lastName : null
                req.body.phone? user.phone = req.body.phone : null
                req.body.email? user.email = req.body.email : null  
                //Guardamos cambios
                user.save(function (err, user) {
                    if (err) {
                        return res.status(400).json({
                            message: 'Error actualizando usuario.',
                            error: err,
                            status:400
                        });
                    }else{
                        return res.json({
                            status: 200,
                            user: user,
                            message: 'Se actualizaron los datos del cliente correctamente'
                        });  
                    }
                   
                });                
            }
            //return res.json(user);
        });
    },

    /**
     * clientsController.changePassword()
     */
    changePassword: function (req, res) {
        console.log('entro a change', req.body)
        var id = req.body.id;
        userModel.findOne({_id: id}, function (err, user) {
            if (err) {
                return res.status(400).json({
                    message: 'Error buscando el registro.',
                    error: err
                });
            }
            else if (!user) {
                return res.status(404).json({
                    message: 'No se encontró el cliente.'
                });
            }
            else{
                oldPassword = req.body.password
                newPassword = req.body.newPassword
                if(user.password != oldPassword){
                    return res.json({
                        status: 400,
                        message: 'La contraseña actual no coincide'
                    });
                }else{
                    user.password = newPassword
                    //Guardamos cambios
                    user.save(function (err, user) {
                        if (err) {
                            return res.status(400).json({
                                message: 'Error actualizando usuario.',
                                error: err,
                                status:400
                            });
                        }else{
                            return res.json({
                                status: 200,
                                user: user,
                                message: 'Se actualizó la foto de perfil correctamente'
                            });  
                        }
                    
                    }); 
                }               
            }            
        });
    },

    /**
     * clientsController.search()
     */
    search: function (req, res) {
        console.log('entró a clients search', req.body)
        var filtersUser ={}
        //obtener los valores de los filtros
        req.body.fullName ? filtersUser['fullname'] = { $regex: '.*' + req.body.fullName.toUpperCase()  + '.*' }  : null;
        req.body.phone ? filtersUser['phone'] =  { $regex: '.*' + req.body.phone + '.*' }  : null;  
        req.body.documentNumber ? filtersUser['documentNumber'] =  { $regex: '.*' + req.body.documentNumber.toUpperCase() + '.*' }  : null      
        req.body.email ? filtersUser['email'] =  { $regex: '.*' + req.body.email + '.*' } : null   
        filtersUser['active'] = true

        console.log('filtros use', filtersUser )
        userModel.find(
            filtersUser
        , function (err, users) {
            if (err) {
                console.log('error',err  )
                return res.status(400).json({
                    message: 'Error when getting users.',
                    error: err
                });
            }
            else{
                var finalArray =[]
                if(req.body.isClient == true){
                    console.log('is users ', users.length)
                    //obtener solo riders
                    for (let index = 0; index < users.length; index++) {
                        if(users[index].clientDetail){
                            finalArray.push(users[index])
                        }                        
                    }
                }
                console.log('is clients',finalArray.length )
                return res.json(
                    {
                        clients:finalArray,
                        status:200
                    });
            }
            
        });
    },

};
