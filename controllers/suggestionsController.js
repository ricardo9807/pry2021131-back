var suggestionsModel = require('../models/suggestionsModel.js');
var moment = require('moment');
/**
 * suggestionsController.js
 *
 * @description :: Server-side logic for managing suggestions.
 */
module.exports = {

    /**
     * suggestionsController.list()
     */
    list: function (req, res) {
        suggestionsModel.find({active: true})
        .populate('idClient')
        .exec(function(err, suggestions) {
            // do stuff with post
            return res.json(suggestions);
        });
    },

    /**
     * suggestionsController.search()
     */
    search: function (req, res) {
        console.log('entró', req.body)
        var filters = {}
        var filtersUser ={}

        //obtener los valores de los filtros
        req.body.nameClient ? filtersUser['fullname'] = { $regex: '.*' + req.body.nameClient + '.*' }  : null;
        req.body.phoneClient ? filtersUser['phone'] =  { $regex: '.*' + req.body.phoneClient + '.*' }  : null;  
        req.body.creationDate ? filters['creationDate'] =  new Date(req.body.creationDate)  : null        
        req.body.status != null ? filters['active'] = req.body.status :null

        console.log('filtros sug', filters )
        console.log('filtros use', filtersUser )
        suggestionsModel.find(filters)
        .populate({
            //incluir los datos del idCliente asociado a la sugerencia
            path: 'idClient', 
            model: 'user', 
            //Hacer match con los filtors de usuario: phone, fullname
            match: filtersUser 
        })
        .exec(function(err, suggestions) {
            if (err) {
                console.log('error',err  )
                return res.status(400).json({
                    message: 'Error when getting suggestions.',
                    error: err,
                    status:200
                });
            }else{
                var finalArray=[]
                console.log('resultado ', )
                //enviar al front solo las sugerencias que tengan idClient !=null (con información)
                for (let index = 0; index < suggestions.length; index++) {
                   if(suggestions[index].idClient != null){
                        finalArray.push(suggestions[index])
                   }                    
                }
                return res.json(
                    {
                        suggestions:finalArray,
                        status:200
                    });
            }            
        });
    },

   
    /**
     * suggestionsController.create()
     */
    create: function (req, res) {
        console.log(req.body);
        if(req.body.id && req.body.suggestion){
            var suggestion = new suggestionsModel({
                idClient: req.body.id,
                content: req.body.suggestion,
                active: 'true',
                creationDate: moment(new Date()).utcOffset('-0500')
            })
            suggestion.save(function (err, user) {
                if (err) {
                    console.log(err);
                    return res.status(400).json({
                        message: 'Error creando registro.',
                        status: '400',
                        error: err
                    });
                }else{
                    return res.status(200).json(suggestion);
                }
            })
        }else{
            return res.status(400).json(
                {
                    message: "Complete todos los campos",
                });
        }
    },

};
