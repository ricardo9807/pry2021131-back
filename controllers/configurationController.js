var configurationModel = require('../models/configurationModel.js');
/**
 * userController.js
 *
 * @description :: Server-side logic for managing users.
 */
module.exports = {

    /**
     * configurationController.list()
     */
    list: function (req, res) {
        console.log('entró a list')
        configurationModel.find(function (err, configuration) {
            if (err) {
                return res.status(400).json({
                    message: 'Error when getting configuration.',
                    status: 400,
                    error: err
                });
            }
            // console.log(configuration[0].sortBy);
            return res.status(200).send({configuration:configuration , status: 200});             
        }) 
    },

    /**
     * configurationController.save()
     */
    save: function (req, res) {
        //obtener configuración
        configurationModel.find(function (err, configuration) {
            console.log('configuration ',configuration)
            console.log('req', req.body)

            if(configuration.length == 0){
                //no existe el documento, hay que crearlo
                console.log('')
                var configurationObj =  new configurationModel()
                req.body.distanceRadius ? configurationObj.distanceRadius = req.body.distanceRadius :null
                req.body.sortBy ? configurationObj.sortBy = req.body.sortBy :null
                req.body.flatAmount ? configurationObj.flatAmount = req.body.flatAmount :null
                req.body.servicePercentage ? configurationObj.servicePercentage = req.body.servicePercentage : null
                req.body.bussinessHour ? configurationObj.bussinessHour = req.body.bussinessHour :null
                req.body.usePercentage == true? configurationObj.usePercentage = req.body.usePercentage :  configurationObj.usePercentage =false
                req.body.useFlatAmount == true? configurationObj.useFlatAmount = req.body.useFlatAmount :  configurationObj.useFlatAmount =false
                req.body.timerInterval ? configurationObj.timerInterval = req.body.timerInterval :null
                req.body.feePerKm ? configurationObj.feePerKm = req.body.feePerKm :null
                req.body.paymentMethods ? configurationObj.paymentMethods = req.body.paymentMethods :null
                req.body.pollutionFactor ? configurationObj.pollutionFactor = req.body.pollutionFactor :null
                req.body.checkServiceInterval ? configurationObj.checkServiceInterval = req.body.checkServiceInterval :null
                req.body.email ? configurationObj.email = req.body.email :null
                req.body.password ? configurationObj.password = req.body.password :null
                req.body.initialBonus ? configurationObj.initialBonus = req.body.initialBonus :null
                req.body.adminPhone ? configurationObj.adminPhone = req.body.adminPhone :null 
                req.body.clientCallSupport == true? configurationObj.clientCallSupport = req.body.clientCallSupport :  configurationObj.clientCallSupport =false
                req.body.contactSupportNumber ? configurationObj.contactSupportNumber = req.body.contactSupportNumber :null
                req.body.contactAdminNumber ? configurationObj.contactAdminNumber = req.body.contactAdminNumber :null 

                configurationObj.save(function (err, configFinal) {
                    if (err) {
                        return res.status(400).json({
                            message: 'Error when creating config.',
                            status:400,
                            error: err
                        });
                    } else{
                        return res.json({configuration: configFinal, status: 200});
                    }
                });
            }else{
                //existe el documento, se debe actualizar
                var updateObj = configuration[0]
                req.body.distanceRadius ? updateObj.distanceRadius = req.body.distanceRadius :null
                req.body.sortBy ? updateObj.sortBy = req.body.sortBy :null
                req.body.flatAmount ? updateObj.flatAmount = req.body.flatAmount :null
                req.body.servicePercentage ? updateObj.servicePercentage = req.body.servicePercentage :null
                req.body.bussinessHour ? updateObj.bussinessHour = req.body.bussinessHour :null   
                req.body.usePercentage == true ? updateObj.usePercentage = req.body.usePercentage : updateObj.usePercentage = false
                req.body.useFlatAmount == true ? updateObj.useFlatAmount = req.body.useFlatAmount : updateObj.useFlatAmount = false
                req.body.timerInterval ? updateObj.timerInterval = req.body.timerInterval :null
                req.body.feePerKm ? updateObj.feePerKm = req.body.feePerKm :null
                req.body.paymentMethods ? updateObj.paymentMethods = req.body.paymentMethods :null
                req.body.pollutionFactor ? updateObj.pollutionFactor = req.body.pollutionFactor :null
                req.body.checkServiceInterval ? updateObj.checkServiceInterval = req.body.checkServiceInterval :null
                req.body.email ? updateObj.email = req.body.email :null
                req.body.password ? updateObj.password = req.body.password :null
                req.body.initialBonus ? updateObj.initialBonus = req.body.initialBonus :null
                req.body.adminPhone ? updateObj.adminPhone = req.body.adminPhone :null
                req.body.clientCallSupport == true? updateObj.clientCallSupport = true : updateObj.clientCallSupport = false
                req.body.contactSupportNumber ? updateObj.contactSupportNumber = req.body.contactSupportNumber :null
                req.body.contactAdminNumber ? updateObj.contactAdminNumber = req.body.contactAdminNumber :null

                updateObj.save(function (err, updateObj) {
                    if (err) {
                        return res.status(400).json({
                            message: 'Error when updating config.',
                            status:400,
                            error: err
                        });
                    }else{
                        return res.json({configuration: updateObj, status: 200});
                    }
                });

            }

        });
        
    },

    
};
