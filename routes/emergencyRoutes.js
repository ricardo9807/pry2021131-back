var express = require('express');
var router = express.Router();
var emergencyController = require('../controllers/emergencyController.js');

/*
 * GET
 */
router.get('/showVal', emergencyController.showVal);
router.get('/:idHcpb/getAllEmergencies', emergencyController.list);

/*
 * GET
 */
router.get('/:id', emergencyController.show);

/*
 * POST
 */
router.post('/notifyEmergency', emergencyController.notifyEmergency);
router.post('/', emergencyController.create);

/*
 * PUT
 */
router.put('/:id/delete', emergencyController.delete); 
router.put('/:id', emergencyController.update);




module.exports = router;
