var express = require('express');
var router = express.Router();
var servicesController = require('../controllers/servicesController.js');

/*
 * GET
 */
router.get('/', servicesController.list);
router.get('/getActiveServices', servicesController.getActiveServices);

/*
 * GET
 */
router.get('/:id', servicesController.show);
router.get('/app/:id/getRiders', servicesController.getCloseRiders);
router.get('/app/:id/getServicesStatus', servicesController.getServiceStatus);


/*
 * POST
 */
router.post('/test', servicesController.test);
router.post('/', servicesController.create);
router.post('/app/', servicesController.createServiceForTheApp);


/*
 * PUT
 */
router.put('/search', servicesController.search);
router.put('/:id', servicesController.update);
router.put('/:id/setAcceptedByRider', servicesController.setAcceptedByRider);
router.put('/:id/setAwaitingPackage', servicesController.setAwaitingPackage);
router.put('/:id/setDelivering', servicesController.setDelivering);
router.put('/:id/setRiderArrived', servicesController.setRiderArrived);
router.put('/:id/setClosedByRider', servicesController.setClosedByRider);
router.put('/:id/rateRider', servicesController.rateRiderByClient);
router.put('/:id/rateClient', servicesController.rateClientByRider);
router.put('/:id/app/requestRider', servicesController.requestRider);
router.put('/:id/updateLocation', servicesController.updateLocation);

router.put('/:id/setRejectedByRider', servicesController.setRejectedByRider);
router.put('/:id/setCanceledByClient', servicesController.setCanceledByClient);

/*
 * DELETE
 */
router.delete('/:id', servicesController.remove);

module.exports = router;
