var express = require('express');
var router = express.Router();
var hcpbController = require('../controllers/hcpbController.js');

/*
 * GET
 */
 router.get('/patient/:id', hcpbController.listByPatient);
 router.get('/', hcpbController.list);


/*
 * GET
 */
router.get('/:id', hcpbController.show);

/*
 * POST
 */

router.post('/', hcpbController.create);

/*
 * PUT
 */
router.put('/terminated', hcpbController.terminated);
router.put('/:id/delete', hcpbController.delete); 
router.put('/:id', hcpbController.update);


/*
 * DELETE
 */


module.exports = router;
