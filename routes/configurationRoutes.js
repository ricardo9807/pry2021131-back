var express = require('express');
var router = express.Router();
var configurationController = require('../controllers/configurationController.js');

/*
 * GET
 */

/*
 * GET
 */
router.get('/list', configurationController.list);

/*
 * POST
 */


/*
 * PUT
 */
router.put('/', configurationController.save);

/*
 * DELETE
 */



module.exports = router;
