var express = require('express');
var router = express.Router();
var userController = require('../controllers/userController.js');
var ridersController = require('../controllers/ridersController.js');
var creditsController = require('../controllers/creditsController.js');

/*
 * GET
 */
router.get('/getActiveRiders', ridersController.getActiveRiders);
router.get('/getAllRiders', ridersController.getAllRiders);
router.get('/:id/getCredits', creditsController.listCreditsByRider);

/*
 * GET
 */
router.get('/:id', ridersController.show);

/*
 * POST
 */

/*
 * PUT
 */
router.put('/search', ridersController.search);
router.put('/:id/setupRider', ridersController.setupRider);
router.put('/:id/setInactive', ridersController.setInactive);
router.put('/:id/setActive', ridersController.setActive);
router.put('/:id/updateLocation', ridersController.updateLocation);
router.put('/:id/updateDocumentation', ridersController.updateDocumentation);
router.put('/:id/setEnabled', ridersController.setEnabled);
router.put('/:id/setdisabled', ridersController.setdisabled);

/*
 * DELETE
 */

module.exports = router;
