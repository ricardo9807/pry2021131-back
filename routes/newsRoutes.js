var express = require('express');
var router = express.Router();
var newsController = require('../controllers/newsController.js');

/*
 * GET
 */
router.get('/', newsController.list);
router.get('/getPublishedNews', newsController.getPublishedNews);

/*
 * GET
 */
router.get('/:id', newsController.show);

/*
 * POST
 */
router.post('/', newsController.create);

/*
 * PUT
 */
router.put('/search', newsController.search);
router.put('/:id', newsController.update);
router.put('/:id/deleteNew', newsController.deleteNew);
router.put('/:id/publishNew', newsController.publishNew);
router.put('/:id/unpublishNew', newsController.unpublishNew);


/*
 * DELETE
 */
router.delete('/:id', newsController.remove); 

module.exports = router;
