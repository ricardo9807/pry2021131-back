var express = require('express');
var router = express.Router();
var frequentQuestionsController = require('../controllers/frequentQuestionsController.js');

/*
 * GET
 */
router.get('/', frequentQuestionsController.list);
/*
 * GET
 */
router.get('/show/:id', frequentQuestionsController.show);
/*
 * GET
 */
router.get('/search', frequentQuestionsController.search);

/*
 * POST
 */
router.post('/', frequentQuestionsController.create);

/*
 * PUT
 */
router.put('/:id', frequentQuestionsController.update);
router.put('/:id/publishQuestion', frequentQuestionsController.publishQuestion);


module.exports = router;
