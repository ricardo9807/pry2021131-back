var express = require('express');
var router = express.Router();
var patientController = require('../controllers/patientController.js');

/*
 * GET
 */
router.get('/', patientController.list);

/*
 * GET
 */
router.get('/:id/getValues', patientController.getValues);
router.get('/:id', patientController.show);

/*
 * POST
 */
router.post('/', patientController.create);

/*
 * PUT
 */
router.put('/:id/delete', patientController.delete); 
router.put('/:id', patientController.update);


/*
 * DELETE
 */


module.exports = router;
