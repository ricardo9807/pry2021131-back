var express = require('express');
var router = express.Router();
var specialityController = require('../controllers/specialityController.js');

/*
 * GET
 */
router.get('/', specialityController.list);

/*
 * GET
 */
router.get('/:id', specialityController.show);

/*
 * POST
 */
router.post('/', specialityController.create);

/*
 * PUT
 */
router.put('/:id/delete', specialityController.delete); 
router.put('/:id', specialityController.update);


/*
 * DELETE
 */


module.exports = router;
