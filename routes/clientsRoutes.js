var express = require('express');
var router = express.Router();
var clientsController = require('../controllers/clientsController.js');
var addressController = require('../controllers/addressController.js');

/*
 * GET
 */
router.get('/getClients', clientsController.getClients);
router.get('/:id/get', clientsController.getClient);
router.get('/:id/getActiveServices', clientsController.getActiveServices);
/*
 * POST
 */
router.post('/', clientsController.create);
router.post('/:id/createAddress', addressController.create);

/*
 * PUT
 */
router.put('/update', clientsController.update);
router.put('/changePassword', clientsController.changePassword);
router.put('/:id/updateProfilePhoto', clientsController.updateProfilePhoto);
router.put('/:id/updateAddress', addressController.update);
router.put('/search', clientsController.search);
/*
 * DELETE
 */


module.exports = router;
