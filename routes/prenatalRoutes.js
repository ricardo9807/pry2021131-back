var express = require('express');
var router = express.Router();
var prenatalControlController = require('../controllers/prenatalControlController.js');

/*
 * GET
 */
router.get('/:idHcpb/getAllControls', prenatalControlController.list);

/*
 * GET
 */
router.get('/:id', prenatalControlController.show);

/*
 * POST
 */
router.post('/', prenatalControlController.create);

/*
 * PUT
 */
router.put('/:id/delete', prenatalControlController.delete); 
router.put('/:id', prenatalControlController.update);




module.exports = router;
