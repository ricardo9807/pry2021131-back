var express = require('express');
var router = express.Router();
var userController = require('../controllers/userController.js');

/*
 * GET
 */
router.get('/:id', userController.show);

/*
 * POST
 */
router.post('/signUp', userController.signUp);
router.post('/signIn', userController.login);
router.post('/sendEmailResetPassword', userController.sendEmailResetPassword);
router.post('/validateUser', userController.validateUser);
router.post('/sendMail', userController.sendMailTest);


/*
 * PUT
 */
router.put('/confirmEmail', userController.confirmEmail);
router.put('/changePassword', userController.changePassword);
router.put('/:id', userController.update);
router.put('/:id/addToken', userController.addToken);
router.put('/:id/deleteToken', userController.deleteToken);

/*
 * DELETE
 */

// router.delete('/:id', userController.remove); //No se usará para producción

module.exports = router;
