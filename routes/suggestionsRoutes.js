var express = require('express');
var router = express.Router();
var sugestionsController = require('../controllers/suggestionsController.js');

/*
 * GET
 */
router.get('/', sugestionsController.list);
router.put('/search', sugestionsController.search);

/*
 * GET
 */

/*
 * POST
 */
router.post('/', sugestionsController.create);



/*
 * PUT
 */
/*
 * DELETE
 */
module.exports = router;
