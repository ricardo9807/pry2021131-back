var express = require('express');
var router = express.Router();
var creditsController = require('../controllers/creditsController.js');

/*
 * GET
 */
router.get('/', creditsController.list);
//router.get('/search', creditsController.search);
/*
 * GET
 */
router.get('/:id', creditsController.show);

/*
 * POST
 */
router.post('/', creditsController.create);
router.post('/searchOperators', creditsController.searchOp);
router.post('/createRefund', creditsController.createCreditByAdmin);
router.post('/createBonus', creditsController.createCreditByAdmin);

/*
 * PUT
 */
router.put('/:id/update', creditsController.update);
router.put('/:id/approveCredit', creditsController.approveCredit);
router.put('/:id/rejectCredit', creditsController.rejectCredit);
router.put('/:id/deleteCredit', creditsController.deleteCredit);
router.put('/search', creditsController.search);
/*
 * DELETE
 */
// router.delete('/:id', creditsController.remove); //Será eliminado para prod

module.exports = router;
