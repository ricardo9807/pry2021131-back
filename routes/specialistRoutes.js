var express = require('express');
var router = express.Router();
var specialistController = require('../controllers/specialistController.js');

/*
 * GET
 */
router.get('/', specialistController.list);

/*
 * GET
 */
 router.get('/reportAdmin', specialistController.reportAdmin);
 router.get('/:id', specialistController.show);

/*
 * POST
 */
router.post('/', specialistController.create);

/*
 * PUT
 */
router.put('/:id/delete', specialistController.delete); 
router.put('/:id', specialistController.update);


/*
 * DELETE
 */


module.exports = router;
