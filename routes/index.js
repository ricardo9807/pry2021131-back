const app = module.exports = require('express')();

app.get('/', (err, res) => res.status(200).json({msg: 'OK'}));
app.use('/users', require('./userRoutes'));
app.use('/clients', require('./clientsRoutes'));
app.use('/riders', require('./ridersRoutes'));
app.use('/services', require('./servicesRoutes'));
app.use('/credits', require('./creditsRoutes'));
app.use('/news', require('./newsRoutes'));
app.use('/suggestions', require('./suggestionsRoutes'));
app.use('/configuration', require('./configurationRoutes'));
app.use('/frequentQuestions', require('./frequentQuestionsRoutes'));



//////////
app.use('/specialties', require('./specialityRoutes'));
app.use('/specialists', require('./specialistRoutes'));
app.use('/patients', require('./patientsRoutes'));
app.use('/hcpbs', require('./hcpbRoutes'));
app.use('/prenatalControls', require('./prenatalRoutes'));
app.use('/emergencies', require('./emergencyRoutes'));